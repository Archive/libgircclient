/*
 * LibGIrcClient: libgircclient/girc-commands.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIRC_COMMANDS_H__
#define __GIRC_COMMANDS_H__

#define GIRC_COMMAND_PASS		"PASS"
#define GIRC_COMMAND_NICK		"NICK"
#define GIRC_COMMAND_USER		"USER"
#define GIRC_COMMAND_OPER		"OPER"
#define GIRC_COMMAND_MODE		"MODE"
#define GIRC_COMMAND_SERVICE	"SERVICE"
#define GIRC_COMMAND_QUIT		"QUIT"
#define GIRC_COMMAND_SQUIT		"SQUIT"
#define GIRC_COMMAND_JOIN		"JOIN"
#define GIRC_COMMAND_PART		"PART"
#define GIRC_COMMAND_TOPIC		"TOPIC"
#define GIRC_COMMAND_NAMES		"NAMES"
#define GIRC_COMMAND_LIST		"LIST"
#define GIRC_COMMAND_INVITE		"INVITE"
#define GIRC_COMMAND_KICK		"KICK"
#define GIRC_COMMAND_PRIVMSG	"PRIVMSG"
#define GIRC_COMMAND_NOTICE		"NOTICE"
#define GIRC_COMMAND_MOTD		"MOTD"
#define GIRC_COMMAND_LUSERS		"LUSERS"
#define GIRC_COMMAND_VERSION	"VERSION"
#define GIRC_COMMAND_STATS		"STATS"
#define GIRC_COMMAND_LINKS		"LINKS"
#define GIRC_COMMAND_TIME		"TIME"
#define GIRC_COMMAND_CONNECT	"CONNECT"
#define GIRC_COMMAND_TRACE		"TRACE"
#define GIRC_COMMAND_ADMIN		"ADMIN"
#define GIRC_COMMAND_INFO		"INFO"
#define GIRC_COMMAND_SERVLIST	"SERVLIST"
#define GIRC_COMMAND_WHO		"WHO"
#define GIRC_COMMAND_WHOIS		"WHOIS"
#define GIRC_COMMAND_WHOWAS		"WHOWAS"
#define GIRC_COMMAND_KILL		"KILL"
#define GIRC_COMMAND_PING		"PING"
#define GIRC_COMMAND_PONG		"PONG"
#define GIRC_COMMAND_ERROR		"ERROR"
#define GIRC_COMMAND_AWAY		"AWAY"
#define GIRC_COMMAND_REHASH		"REHASH"
#define GIRC_COMMAND_DIE		"DIE"
#define GIRC_COMMAND_RESTART	"RESTART"
#define GIRC_COMMAND_SUMMON		"SUMMON"
#define GIRC_COMMAND_USERS		"USERS"
#define GIRC_COMMAND_WALLOPS	"WALLOPS"
#define GIRC_COMMAND_USERHOST	"USERHOST"
#define GIRC_COMMAND_ISON		"ISON"

#endif /* __GIRC_COMMANDS_H__ */
