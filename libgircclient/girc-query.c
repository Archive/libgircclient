/*
 * LibGIrcClient: libgircclient/girc-query.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "girc-query.h"
#include "girc-client.h"


/* Create girc-query-private.h if query-related non-API functions are necessary? */


GType
girc_query_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcQuery",
												   (GBoxedCopyFunc) girc_query_ref,
												   (GBoxedFreeFunc) girc_query_unref);
	}

	return boxed_type;
}


/**
 * girc_query_new: 
 * 
 * Creates a new #GIrcQuery-struct with a reference count of one.
 * 
 * Returns: a new #GIrcQuery-struct.
 * 
 * Since: 1.0
 **/
GIrcQuery *
girc_query_new (void)
{
	GIrcQuery *retval = NULL;

	retval = g_new0 (GIrcQuery, 1);

	retval->type = GIRC_TYPE_QUERY;
	retval->ref = 1;

	retval->ctime = time (NULL);

	return retval;
}


/**
 * girc_query_ref: 
 * @query: the data to reference.
 * 
 * Increases the reference count on @query by one. When the reference count on @query
 * drops to zero, the data will be freed.
 * 
 * Returns: a new reference to @query.
 * 
 * Since: 1.0
 **/
GIrcQuery *
girc_query_ref (GIrcQuery *query)
{
	g_return_val_if_fail (GIRC_IS_QUERY (query), NULL);

	query->ref++;

	return query;
}

/**
 * girc_query_unref: 
 * @query: the data to be unreferenced.
 *
 * Decreases the reference count on @query by one. When the reference count reaches
 * zero, the data will be freed.
 *
 * Since: 1.0
 **/
void
girc_query_unref (GIrcQuery *query)
{
	g_return_if_fail (GIRC_IS_QUERY (query));

	query->ref--;

	if (query->ref == 0)
	{
		if (query->userhost != NULL)
			g_free (query->userhost);

		if (query->user != NULL)
			girc_user_unref (query->user);
	}
}
