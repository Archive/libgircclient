/*
 * LibGIrcClient: libgircclient/girc-channel.h
 *
 * Copyright (c) 2002 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIRC_CHANNEL_H__
#define __GIRC_CHANNEL_H__


#include <glib-object.h>

#include <time.h>

#include "girc-type-utils.h"
#include "girc-user.h"


#define GIRC_TYPE_CHANNEL					(girc_channel_get_type ())
#define GIRC_CHANNEL(boxed)					(GIRC_TYPE_CHECK_BOXED_CAST ((boxed), GIRC_TYPE_CHANNEL, GIrcChannel))
#define GIRC_IS_CHANNEL(boxed)				(GIRC_TYPE_CHECK_BOXED_TYPE ((boxed), GIRC_TYPE_CHANNEL))

#define GIRC_TYPE_CHANNEL_USER				(girc_channel_user_get_type ())
#define GIRC_CHANNEL_USER(boxed)			(GIRC_TYPE_CHECK_BOXED_CAST ((boxed), GIRC_TYPE_CHANNEL_USER, GIrcChannelUser))
#define GIRC_IS_CHANNEL_USER(boxed)			(GIRC_TYPE_CHECK_BOXED_TYPE ((boxed), GIRC_TYPE_CHANNEL_USER))

#define GIRC_TYPE_CHANNEL_MASK				(girc_channel_mask_get_type ())
#define GIRC_CHANNEL_MASK(boxed)			(GIRC_TYPE_CHECK_BOXED_CAST ((boxed), GIRC_TYPE_CHANNEL_MASK, GIrcChannelMask))
#define GIRC_IS_CHANNEL_MASK(boxed)			(GIRC_TYPE_CHECK_BOXED_TYPE ((boxed), GIRC_TYPE_CHANNEL_MASK))

#define GIRC_TYPE_USER_CHANNEL_ON			(girc_user_channel_on_get_type ())
#define GIRC_USER_CHANNEL_ON(boxed)			(GIRC_TYPE_CHECK_BOXED_CAST ((boxed), GIRC_TYPE_USER_CHANNEL_ON, GIrcUserChannelOn))
#define GIRC_IS_USER_CHANNEL_ON(boxed)		(GIRC_TYPE_CHECK_BOXED_TYPE ((boxed), GIRC_TYPE_USER_CHANNEL_ON))

#define GIRC_TYPE_CHANNEL_MODE				(girc_channel_mode_get_type ())
#define GIRC_CHANNEL_MODE(boxed)			((GIrcChannelMode *)(boxed))

#define GIRC_TYPE_CHANNEL_PREFIX			(girc_channel_prefix_get_type ())
#define GIRC_CHANNEL_PREFIX(boxed)			((GIrcChannelPrefix *)(boxed))

#define GIRC_TYPE_CHANNEL_USER_MODE			(girc_channel_user_mode_get_type ())
#define GIRC_CHANNEL_USER_MODE(boxed)		(GIRC_TYPE_CHECK_BOXED_CAST ((boxed), GIRC_TYPE_CHANNEL_USER_MODE, GIrcChannelUserMode))
#define GIRC_IS_CHANNEL_USER_MODE(boxed)	(GIRC_TYPE_CHECK_BOXED_TYPE ((boxed), GIRC_TYPE_CHANNEL_USER_MODE))


typedef struct _GIrcChannel GIrcChannel;
typedef struct _GIrcChannelMask GIrcChannelMask;
typedef struct _GIrcChannelPrefix GIrcChannelPrefix;
typedef struct _GIrcChannelMode GIrcChannelMode;
typedef struct _GIrcChannelUser GIrcChannelUser;
typedef struct _GIrcChannelUserMode GIrcChannelUserMode;
typedef struct _GIrcUserChannelOn GIrcUserChannelOn;


/* *INDENT-OFF* */
typedef enum /*< flags,prefix=GIRC_CHANNEL_MODE > */
{
	GIRC_CHANNEL_MODE_NONE				= 0,
	GIRC_CHANNEL_MODE_MODERATED			= 1 << 0,
	GIRC_CHANNEL_MODE_INVITE_ONLY		= 1 << 1,
	GIRC_CHANNEL_MODE_NOMSG				= 1 << 2,
	GIRC_CHANNEL_MODE_TOPIC_PROTECT		= 1 << 3,
	GIRC_CHANNEL_MODE_LIMIT				= 1 << 4,
	GIRC_CHANNEL_MODE_KEY				= 1 << 5,
	GIRC_CHANNEL_MODE_PRIVATE			= 1 << 6,
	GIRC_CHANNEL_MODE_SECRET			= 1 << 7,
	GIRC_CHANNEL_MODE_QUIET				= 1 << 8,
	GIRC_CHANNEL_MODE_ANONYMOUS			= 1 << 9,
	GIRC_CHANNEL_MODE_REOP				= 1 << 10
}
GIrcChannelModeFlags;


typedef enum /* < prefix=GIRC_CHANNEL_MODE_CHAR > */
{
	GIRC_CHANNEL_MODE_CHAR_MODERATED			= 'm',
	GIRC_CHANNEL_MODE_CHAR_INVITE_ONLY			= 'i',
	GIRC_CHANNEL_MODE_CHAR_NOMSG				= 'n',
	GIRC_CHANNEL_MODE_CHAR_TOPIC_PROTECT		= 't',
	GIRC_CHANNEL_MODE_CHAR_LIMIT				= 'l',
	GIRC_CHANNEL_MODE_CHAR_KEY					= 'k',
	GIRC_CHANNEL_MODE_CHAR_PRIVATE				= 'p',
	GIRC_CHANNEL_MODE_CHAR_SECRET				= 's',
	GIRC_CHANNEL_MODE_CHAR_QUIET				= 'q',
	GIRC_CHANNEL_MODE_CHAR_ANONYMOUS			= 'a',
	GIRC_CHANNEL_MODE_CHAR_REOP					= 'r',

	/* These modes aren't "toggles" per-se, but they do exist. */
	GIRC_CHANNEL_MODE_CHAR_BAN					= 'b',
	GIRC_CHANNEL_MODE_CHAR_BAN_EXCEPTIONS		= 'e',
	GIRC_CHANNEL_MODE_CHAR_INVITE               = 'I',
	GIRC_CHANNEL_MODE_CHAR_INVITE_EXCEPTIONS	= 'E'
}
GIrcChannelModeChar;


typedef enum /*< flags,prefix=GIRC_CHANNEL_USER_MODE >*/
{
	GIRC_CHANNEL_USER_MODE_NONE		= 0,
	GIRC_CHANNEL_USER_MODE_VOICE	= 1 << 0,
	GIRC_CHANNEL_USER_MODE_HALFOPS	= 1 << 1,
	GIRC_CHANNEL_USER_MODE_OPS		= 1 << 2,
	GIRC_CHANNEL_USER_MODE_CREATOR	= 1 << 3
}
GIrcChannelUserModeFlags;


typedef enum /* < prefix=GIRC_CHANNEL_USER_MODE_CHAR > */
{
	GIRC_CHANNEL_USER_MODE_CHAR_NONE	= '\0',

	/* Used to set modes */
	GIRC_CHANNEL_USER_MODE_CHAR_OPS		= 'o',
	GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS	= 'h',
	GIRC_CHANNEL_USER_MODE_CHAR_VOICE	= 'v',

	/* This mode is never settable by a client */
	GIRC_CHANNEL_USER_MODE_CHAR_CREATOR	= 'O'
}
GIrcChannelUserModeChar;


typedef enum /* < prefix=GIRC_CHANNEL_PREFIX > */
{
	GIRC_CHANNEL_PREFIX_NORMAL		= '#',
	GIRC_CHANNEL_PREFIX_LOCAL		= '&',
	GIRC_CHANNEL_PREFIX_SAFE		= '!',
	GIRC_CHANNEL_PREFIX_NO_MODES	= '+'
}
GIrcChannelPrefixChar;


typedef enum /* < prefix=GIRC_CHANNEL_MODE_ARGS > */
{
	GIRC_CHANNEL_MODE_ARGS_UNKNOWN,
	GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM,
	GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_PARAM,
	GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_OR_NO_PARAM,
	GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM
}
GIrcChannelModeArgumentType;


typedef enum /* <prefix=GIRC_CHANNEL_INFO> */
{
	GIRC_CHANNEL_INFO_NONE,
	GIRC_CHANNEL_INFO_MODES,
	GIRC_CHANNEL_INFO_USERS,
	GIRC_CHANNEL_INFO_TOPIC,
	GIRC_CHANNEL_INFO_LIMIT,
	GIRC_CHANNEL_INFO_KEY,
	GIRC_CHANNEL_INFO_BAN,
	GIRC_CHANNEL_INFO_BAN_EXCEPTION,
	GIRC_CHANNEL_INFO_INVITE,
	GIRC_CHANNEL_INFO_INVITE_EXCEPTION,
	GIRC_CHANNEL_INFO_LAST
}
GIrcChannelInfoType;


typedef enum /* <prefix=GIRC_CHANNEL_USER_CHANGED> */
{
	GIRC_CHANNEL_USER_CHANGED_NONE,
	GIRC_CHANNEL_USER_CHANGED_JOIN,
	GIRC_CHANNEL_USER_CHANGED_PART,
	GIRC_CHANNEL_USER_CHANGED_KICKED,
	GIRC_CHANNEL_USER_CHANGED_QUIT,
	GIRC_CHANNEL_USER_CHANGED_MODES,
	GIRC_CHANNEL_USER_CHANGED_NICK
}
GIrcChannelUserChangedType;
/* *INDENT-ON* */


struct _GIrcChannelPrefix
{
	gchar prefix;
	gchar *description;
};


struct _GIrcChannelMode
{
	gchar mode_char;
	GIrcChannelModeFlags mode;
	GIrcChannelModeArgumentType arg_type;
	gchar *description;
};

struct _GIrcChannelUser
{
	/* < private > */
	GType type;
	guint ref;

	/* < public > */
	GIrcUser *user;
	GIrcChannelUserModeFlags modes:4;
};


struct _GIrcChannelUserMode
{
	/* < private > */
	GType type;

	/* < public > */
	GIrcChannelUserModeFlags mode;
	gchar mode_char;

	/* Used to designate status in lists of channels a user is on */
	gchar prefix_char;

	gchar *description;
};


struct _GIrcChannelMask
{
	/* < private > */
	GType type;

	/* < public > */
	gchar *mask;
	gchar *server;
	time_t set_on_time;
};


struct _GIrcChannel
{
	/* < private > */
	GType type;
	guint ref;

	/* < public > */
	gchar *name;
	gchar *creator;
	time_t ctime;
	gchar *topic;

	GIrcChannelUser *my_user;

	guint limit;
	gchar *key;

	GSList *bans;
	GSList *ban_excepts;

	GSList *invites;
	GSList *invite_excepts;

	GHashTable *users;

	GIrcChannelModeFlags modes:11;
};


struct _GIrcUserChannelOn
{
	/* < private > */
	GType type;

	/* < public > */
	gchar *name;
	GIrcChannelUserModeFlags modes:4;
};


GType girc_channel_get_type (void);
GType girc_channel_user_get_type (void);
GType girc_channel_mask_get_type (void);
GType girc_channel_mode_get_type (void);
GType girc_channel_prefix_get_type (void);
GType girc_channel_user_mode_get_type (void);
GType girc_user_channel_on_get_type (void);


GIrcChannel *girc_channel_new (void);
GIrcChannel *girc_channel_ref (GIrcChannel * channel);
void girc_channel_unref (GIrcChannel * channel);
GSList *girc_channel_get_users (GIrcChannel * channel);


GIrcChannelUser *girc_channel_user_new (void);
GIrcChannelUser *girc_channel_user_ref (GIrcChannelUser * user);
void girc_channel_user_unref (GIrcChannelUser * user);
gint girc_channel_user_collate (const GIrcChannelUser * user1,
								const GIrcChannelUser * user2,
								const gchar *encoding);

GIrcUserChannelOn *girc_user_channel_on_new (void);
GIrcUserChannelOn *girc_user_channel_on_dup (const GIrcUserChannelOn * channel_on);
void girc_user_channel_on_free (GIrcUserChannelOn * channel_on);
gint girc_user_channel_on_collate (const GIrcUserChannelOn * chan1,
									const GIrcUserChannelOn * chan2,
									const gchar * encoding);

GIrcChannelMask *girc_channel_mask_new (void);
GIrcChannelMask *girc_channel_mask_dup (const GIrcChannelMask * src);
void girc_channel_mask_free (GIrcChannelMask * mask);


G_CONST_RETURN GIrcChannelPrefix *girc_channel_prefix_get_prefix (gchar prefix_char);
GIrcChannelPrefix *girc_channel_prefix_dup (const GIrcChannelPrefix * src);
void girc_channel_prefix_free (GIrcChannelPrefix * prefix);

G_CONST_RETURN GIrcChannelMode *girc_channel_mode_new_from_char (gchar mode_char);
GIrcChannelMode *girc_channel_mode_dup (const GIrcChannelMode * src);
void girc_channel_mode_free (GIrcChannelMode * mode);

GIrcChannelUserMode *girc_channel_user_mode_new (gchar mode_char,
												 gchar prefix_char);
gchar *girc_channel_user_mode_get_description (gchar mode_char);
GIrcChannelUserMode *girc_channel_user_mode_dup (const GIrcChannelUserMode * src);
void girc_channel_user_mode_free (GIrcChannelUserMode * mode);


gint girc_channel_user_mode_flags_collate (const GIrcChannelUserModeFlags modes1,
										   const GIrcChannelUserModeFlags modes2);


#endif /* __GIRC_CHANNEL_H__ */
