
#include "girc-client.h"
#include <libgtcpsocket/gtcp-errors.h>

#include <stdio.h>
#include <string.h>
#include <time.h>

#define SERVER "localhost"
#define PORT 6667
#define NICK "testgirc"
#define USERNAME "testuser"
#define REALNAME "Test GIrcClient"
#define PASS "testpass"

#define JOIN_CMD "JOIN #test-lib"
#define QUERY_CMD "PRIVMSG testgirc :Hi!"

#define BOOLEAN_TO_STRING(val) (val ? "TRUE" : "FALSE")

GMainLoop *loop = NULL;


/* ******************* *
 *  UTILITY FUNCTIONS  *
 * ******************* */

/* static gboolean
quit_me (gpointer sock)
{
	g_message ("Closing socket.");
	girc_client_quit (GIRC_CLIENT (sock));

	return FALSE;
} */


/* ******************* *
 *  TESTING FUNCTIONS  *
 * ******************* */

static gboolean
test_server_environment (gpointer client)
{
	GValueArray *user_modes,
	 *channel_prefixes,
	 *channel_modes,
	 *languages;
	gchar *network_name,
	 *charset;
	GIrcClientCharacterMappingType charmapping;
	guint i,
	  max_channels,
	  max_bans,
	  nicklen,
	  topiclen,
	  kicklen,
	  chanidlen,
	  channelnamelen,
	  maxtargets,
	  maxwatches,
	  cmdsize,
	  silencesize;
	gboolean userip,
	  knock,
	  cjoin,
	  cprivmsg,
	  cnotice,
	  wallchops,
	  who_is_whox;

	g_print ("(*** TESTING 005 NUMERIC PARSING ***)\n");

	g_print ("Retrieving environment... ");
	g_object_get (client,
				  "network-name", &network_name,
				  "character-mapping", &charmapping,
				  "server-character-set", &charset,
				  "max-channels", &max_channels,
				  "max-bans", &max_bans,
				  "nick-length", &nicklen,
				  "topic-length", &topiclen,
				  "kick-message-length", &kicklen,
				  "channel-id-length", &chanidlen,
				  "channel-name-length", &channelnamelen,
				  "max-targets", &maxtargets,
				  "max-watches", &maxwatches,
				  "mode-command-size", &cmdsize,
				  "silence-list-size", &silencesize,
				  "userip-command", &userip,
				  "knock-command", &knock,
				  "cjoin-command", &cjoin,
				  "cprivmsg-command", &cprivmsg,
				  "cnotice-command", &cnotice,
				  "wallchops-command", &wallchops,
				  "who-command-is-whox", &who_is_whox,
				  "channel-user-modes", &user_modes,
				  "channel-types", &channel_prefixes,
				  "channel-modes", &channel_modes, "languages", &languages, NULL);
	g_print ("Done.\n\n");


	g_print ("MISCELANEOUS\n");

	g_print ("Network Name: %s\n", network_name);
	if (network_name != NULL)
		g_free (network_name);

	g_print ("Character mapping: %d\n", charmapping);

	g_print ("Character set: %s\n\n", charset);
	if (charset != NULL)
		g_free (charset);


	g_print ("LIMITS:\n");

	g_print ("Max Channels: %u\n", max_channels);
	g_print ("Max Bans: %u\n", max_bans);
	g_print ("Topic Length: %u\n", topiclen);
	g_print ("Kick Message Length: %u\n", kicklen);
	g_print ("Channel ID Length: %u\n", chanidlen);
	g_print ("Channel Name Length: %u\n", channelnamelen);
	g_print ("Max Targets: %u\n", maxtargets);
	g_print ("Max Watches: %u\n", maxwatches);
	g_print ("MODE Command Size: %u\n", cmdsize);
	g_print ("Silence List Size: %u\n\n", silencesize);

	g_print ("TOGGLES:\n");

	g_print ("USERIP Command: %s\n", BOOLEAN_TO_STRING (userip));
	g_print ("KNOCK Command: %s\n", BOOLEAN_TO_STRING (knock));
	g_print ("CJOIN Command: %s\n", BOOLEAN_TO_STRING (cjoin));
	g_print ("CPRIVMSG Command: %s\n", BOOLEAN_TO_STRING (cprivmsg));
	g_print ("CNOTICE Command: %s\n", BOOLEAN_TO_STRING (cnotice));
	g_print ("WALLCHOPS Command: %s\n", BOOLEAN_TO_STRING (wallchops));
	g_print ("WHOX Command: %s\n\n", BOOLEAN_TO_STRING (who_is_whox));


	g_print ("USER MODES:\n");
	for (i = 0; i < user_modes->n_values; i++)
	{
		GIrcChannelUserMode *mode;

		mode = g_value_get_boxed (g_value_array_get_nth (user_modes, i));

		g_print ("User Mode Char: %c\n", mode->mode_char);
		g_print ("Prefix Char: %c\n", mode->prefix_char);
		g_print ("Description: %s\n\n", mode->description);
	}
	g_value_array_free (user_modes);

	g_print ("CHANNEL PREFIXES:\n");
	for (i = 0; i < channel_prefixes->n_values; i++)
	{
		GIrcChannelPrefix *prefix;

		prefix = g_value_get_boxed (g_value_array_get_nth (channel_prefixes, i));

		g_print ("Prefix Char: %c\n", prefix->prefix);
		g_print ("Description: %s\n\n", prefix->description);
	}
	g_value_array_free (channel_prefixes);

	g_print ("CHANNEL MODES:\n");
	for (i = 0; i < channel_modes->n_values; i++)
	{
		GIrcChannelMode *mode;

		mode = g_value_get_boxed (g_value_array_get_nth (channel_modes, i));

		g_print ("Mode Char: %c\n", mode->mode_char);
		g_print ("Description: %s\n\n", mode->description);
	}
	g_value_array_free (channel_modes);

	g_print ("LANGUAGES:\n");
	if (languages->n_values > 0)
	{
		for (i = 0; i < languages->n_values; i++)
		{
			gchar *lang;

			lang = (gchar *) g_value_get_string (g_value_array_get_nth (languages, i));

			g_print ("Language: %s\n", lang);
		}
	}
	else
	{
		g_print ("None.\n");
	}
	g_value_array_free (languages);

	g_print ("(*** DONE TESTING 005 NUMERIC PARSING ***)\n\n");

	return FALSE;
}


static gboolean
test_join (gpointer data)
{
	girc_client_send_raw (GIRC_CLIENT (data), JOIN_CMD);

	return FALSE;
}


static gboolean
test_query (gpointer data)
{
	girc_client_send_raw (GIRC_CLIENT (data), QUERY_CMD);

	return FALSE;
}


/* ************************** *
 *  GTcpConnection CALLBACKS  *
 * ************************** */

static void
lookup_done_cb (GTcpConnection * sock,
				GTcpLookupStatus status,
				gpointer blah)
{
	if (status != GTCP_LOOKUP_OK)
	{
		g_warning ("DNS LOOKUP FAILED: %s\n",
				   gtcp_error_get_lookup_status_message (status, SERVER));
		g_main_loop_quit (loop);
	}
	else
	{
		g_print ("(*** Lookup Successful, connecting... ***)\n\n");
	}
}


static void
close_cb (GTcpConnection * sock,
		  gpointer blah)
{
	g_object_unref (sock);

	g_message ("Socket closed. Exiting.");
	g_main_loop_quit (loop);
}


static void
recv_cb (GTcpConnection * sock,
		 gconstpointer data,
		 gsize length,
		 gpointer blah)
{
	gchar *str = g_strndup (data, length - 2);

	printf ("(*** RAW INCOMING ***)\n%s\n(*** RAW INCOMING END ***)\n\n", str);

	g_free (str);
}


static void
send_cb (GTcpConnection * sock,
		 gconstpointer data,
		 gsize length,
		 gpointer blah)
{
	gchar *str = g_strndup (data, length - 2);

	printf ("(*** RAW OUTGOING ***)\n%s\n(*** RAW OUTGOING END ***)\n\n", str);
	g_free (str);
}


/* ********************** *
 *  GIrcClient CALLBACKS  *
 * ********************** */

static void
connect_complete_cb (GIrcClient * client,
					 GIrcClientStatus status,
					 gchar * server,
					 gchar * nick,
					 gpointer blah)
{
	if (status != GIRC_CLIENT_CONNECTED)
	{
		g_warning ("CONNECTION FAILED");
		g_main_loop_quit (loop);
		return;
	}

	g_print ("(*** Connection successful, connected to '%s' as '%s' ***)\n\n",
			 server, nick);

	/* Tests */
	g_idle_add (test_join, client);
	g_idle_add (test_query, client);
	g_idle_add (test_server_environment, client);
}


static void
motd_recv_cb (GIrcClient * irc,
			  const gchar * motd)
{
	g_print ("(*** MOTD RECIEVED ***)\n");

	printf ("%s\n", motd);

	g_print ("(*** END MOTD RECIEVED ***)\n\n");
}


static void
msg_recv_cb (GIrcClient * irc,
			 GIrcReplyType reply,
			 const gchar * msg)
{
	g_print ("(*** MSG RECIEVED ***)\n");
	g_print ("Reply: %d\nMessage: %s\n", reply, msg);
	g_print ("(*** END MSG RECIEVED***)\n\n");
}


static void
new_channel_cb (GIrcClient * irc,
				const gchar * name)
{
	g_print ("(*** NEW CHANNEL ***)\n");
	g_print ("Channel name = '%s'\n", name);
	g_print ("(*** END NEW CHANNEL ***)\n\n");
}


static void
new_query_cb (GIrcClient * irc,
			  const gchar * userhost)
{
	GIrcQuery *query = GIRC_QUERY (girc_client_get_target (irc, userhost));
	struct tm tm_time;
	gchar time_buf[20];

	localtime_r ((time_t *) & (query->ctime), &tm_time);
	strftime (time_buf, G_N_ELEMENTS (time_buf), "%r", &tm_time);

	g_print ("(*** NEW QUERY ***)\n");
	g_print ("Query Target = '%s'\n", userhost);
	g_print ("Start Time = '%s'\n", time_buf);
	g_print ("(*** END NEW QUERY ***)\n\n");
}


static void
channel_mode_changed_cb (GIrcClient * irc,
						 const gchar * channel,
						 GIrcChannelModeFlags modes,
						 guint limit,
						 const gchar * key)
{
	g_print ("(*** CHANNEL MODE CHANGED ***)\n");
	g_print ("Channel: %s\nModes:   %d\nLimit:   %u\nKey:     %s\n",
			 channel, modes, limit, key);
	g_print ("(*** END CHANNEL MODE CHANGED ***)\n\n");
}


/* **************** *
 *  MAIN FUNCTIONS  *
 * **************** */

static gboolean
do_testgircclient (gpointer data)
{
	GObject *client;

	g_type_init ();

	g_print ("(*** Creating object. ***)\n\n");

	client = girc_client_new (NICK, REALNAME, USERNAME, SERVER, PORT, PASS, NULL);

	g_signal_connect (client, "lookup-done", G_CALLBACK (lookup_done_cb), NULL);
//  g_signal_connect (client, "connect-done", G_CALLBACK (connect_done_cb), NULL);
	g_signal_connect (client, "connect-complete", G_CALLBACK (connect_complete_cb), NULL);
	g_signal_connect (client, "recv", G_CALLBACK (recv_cb), NULL);
	g_signal_connect (client, "send", G_CALLBACK (send_cb), NULL);
	g_signal_connect (client, "closed", G_CALLBACK (close_cb), NULL);
	g_signal_connect (client, "motd-recv", G_CALLBACK (motd_recv_cb), NULL);
	g_signal_connect (client, "new-channel", G_CALLBACK (new_channel_cb), NULL);
	g_signal_connect (client, "new-query", G_CALLBACK (new_query_cb), NULL);
	g_signal_connect (client, "channel-mode-changed",
					  G_CALLBACK (channel_mode_changed_cb), NULL);
	g_signal_connect (client, "msg-recv", G_CALLBACK (msg_recv_cb), NULL);

	g_print ("(*** Opening connection asynchronously. ***)\n\n");
	girc_client_open (GIRC_CLIENT (client));

	g_print ("(*** Open function has returned. ***)\n\n");

	return FALSE;
}


int
main (int argc,
	  char *argv[])
{
	loop = g_main_loop_new (NULL, FALSE);

	g_idle_add (do_testgircclient, NULL);

	g_main_loop_run (loop);

	return 0;
}
