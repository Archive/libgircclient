/*
 * LibGIrcClient: libgircclient/girc-client-private.h
 *
 * Copyright (c) 2002 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIRC_CLIENT_PRIVATE_H__
#define __GIRC_CLIENT_PRIVATE_H__


#include "girc-client.h"


typedef struct _GIrcClientVariables GIrcClientVariables;


struct _GIrcClientPrivate
{
	/* Incomplete lines waiting */
	gchar *buffer;

	/* My Information */
	gchar *preferred_nick;
	GIrcUser *my_user;
	guint nick_attempts;

	gchar *pass;

	/* Server Information */
	GIrcClientVariables *vars;
	gchar *encoding;
	GIConv converter;

	/* Cached Data */
	GHashTable *users;
	GHashTable *targets;

	/* Pings waiting on a return */
	GSList *waiting_pings;

	/* These lists are user-set, but managed by the client. */
	GSList *ignores;
	GSList *notifies;

	/* Notification ID */
	gint notify_id;

	/* GSource for anti-timeout ping & lag detector timeval */
	gint anti_timeout_id;
	GTimeVal lag;

	/* Accumulation for multiline command returns */
	GIrcUser *waiting_whois;
	GHashTable *names_reply;
	GSList *chan_list;
	GIrcMotd *waiting_motd;
	GSList *waiting_banlist;

	GIrcClientStatus status:5;

	/* Server modes */
	GIrcServerModeFlags modes:7;
	GIrcServerNoticeFlags server_notices:14;
	gboolean away:1;
};


/* See http://www.irc.org/tech_docs/005.html */
struct _GIrcClientVariables
{
	/* Not technically in the 005 line, but it belongs here better than
	   GIrcClientPrivate */
	gchar *servername;
	gchar *motd;

	/* Strings */
	gchar *network_name;
	/* Charset is currently unused, and if it's set to rfc1459, it actually
	   is just an alias to charmapping. However, it may be used for charset
	   conversion to UTF-8 in the future. Actually, just supporting the
	   LANGUAGE command is the best solution from a client-author perspective. */
	gchar *charset;

	/* Available channel mode flags (Type: GIrcChannelMode *) */
	GSList *channel_modes;
	/* Available channel user modes (Type: GIrcChannelUserMode *) */
	GSList *user_modes;
	/* Channel types (#,!,&,+) (Type: GIrcChannelPrefix *) */
	GSList *channel_prefixes;
	/* Supported languages via the LANGUAGE command (Type: gchar *) */
	GSList *languages;

	/* Limits */
	guint8 max_nick_length;
	guint8 max_channels;
	guint8 max_num_of_bans;
	guint8 max_topic_length;
	guint8 max_kickmsg_length;
	guint8 max_channel_length;
	guint8 max_per_mode_cmds;
	guint8 max_watches;
	guint8 max_targets;
	guint8 silence_list_size;
	guint8 chanid_length;

	/* Character mapping (rfc1459 || ascii <-- defaults to ascii) */
	GIrcClientCharacterMappingType char_mapping:2;

	/* Supported commands */
	gboolean userip_cmd:1;
	gboolean knock_cmd:1;
	gboolean cjoin_cmd:1;
	gboolean cprivmsg_cmd:1;
	gboolean cnotice_cmd:1;
	gboolean wallchops_cmd:1;
	gboolean language_cmd:1;

	/* Supported features & feature types */
	gboolean penalty:1;
	gboolean forced_nickchange:1;
	gboolean ban_exceptions:1;
	gboolean invite_exceptions:1;
	gboolean server_side_ignore:1;	// http://www.ircd-hybrid.org/docs/modeg.txt
	gboolean who_is_whox:1;
	gboolean safe_list:1;
};


/* Lib-Public Functions */
GSList *_girc_client_get_users_targets (GIrcClient * irc,
										const gchar * userhost);


/* Signal Emitters */
void _girc_client_emit_connect_complete (GIrcClient * irc,
										 GIrcClientStatus status);
void _girc_client_emit_quit (GIrcClient * irc,
							 gboolean requested);

void _girc_client_emit_nick_changed (GIrcClient * irc,
									 const gchar * new_nick);
void _girc_client_emit_ctcp_recv (GIrcClient * irc,
								  GIrcUser * user,
								  GIrcCtcpMessageType type,
								  const gchar * data);

void _girc_client_emit_target_closed (GIrcClient * irc,
									  const GIrcTarget * target,
									  const gchar * reason);
void _girc_client_emit_target_recv (GIrcClient * irc,
									const GIrcTarget * target,
									const GValue * sender,
									const GIrcUserMessageType type,
									const gchar * msg);

GIrcTarget *_girc_client_setup_new_query (GIrcClient * irc,
										  GIrcUser * user);
void _girc_client_emit_query_changed (GIrcClient * irc,
									  const GIrcQuery * query,
									  const GIrcQueryChangedType changed,
									  const gchar * msg);

GIrcChannel *_girc_client_setup_new_channel (GIrcClient * irc,
											 const gchar * channel);
void _girc_client_emit_channel_changed (GIrcClient * irc,
										const GIrcChannel * channel,
										const GValue * changer,
										const GIrcChannelInfoType changed,
										const GValue * value,
										const gboolean is_on);
void _girc_client_emit_channel_modes_changed (GIrcClient * irc,
											  const GIrcChannel * channel,
											  const GValue * changer,
											  const GIrcChannelModeFlags modes);
void _girc_client_emit_channel_user_changed (GIrcClient * irc,
											 const GIrcChannel * channel,
											 const GIrcChannelUser * user,
											 const GIrcChannelUserChangedType changed,
											 const GValue * changer,
											 const GValue * value);

void _girc_client_emit_notice_recv (GIrcClient * irc,
									const gchar * sender,
									const gchar * msg,
									const gchar * data);
void _girc_client_emit_mode_recv (GIrcClient * irc,
								  GIrcServerModeFlags modes);
void _girc_client_emit_whois_recv (GIrcClient * irc);
void _girc_client_emit_motd_recv (GIrcClient * irc);
void _girc_client_emit_list_recv (GIrcClient * irc);
void _girc_client_emit_names_recv (GIrcClient * irc);

void _girc_client_emit_msg_recv (GIrcClient * irc,
								 GIrcReplyType reply,
								 const gchar * msg);
void _girc_client_emit_error_recv (GIrcClient * irc,
								   GIrcServerErrorType error,
								   const gchar * msg);

#endif /* __GIRC_CLIENT_PRIVATE_H__ */
