/*
 * LibGIrcClient: libgircclient/girc-channel.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "girc-channel.h"
#include "girc-utils.h"

#include <libgtcpsocket/gtcp-i18n.h>


typedef struct
{
	gchar ch;
	gint val;
	gchar *str;
}
CharEnumString;


static GIrcChannelPrefix channel_prefixes[] = {
	{GIRC_CHANNEL_PREFIX_NORMAL,
	 N_("A channel who's name begins with a \"#\" character is a standard channel.")},

	{GIRC_CHANNEL_PREFIX_LOCAL,
	 N_("A channel who's name begins with a \"&\" character is one that only appears on "
		"the current server. Users must be on the same server as the channel was created "
		"to join it.")},

	{GIRC_CHANNEL_PREFIX_SAFE,
	 N_("A channel who's name begins with a \"!\" character is one that has a channel ID "
		"automatically appended to it, so it will be created if there is another "
		"channel with the same basic name.")},

	{GIRC_CHANNEL_PREFIX_NO_MODES,
	 N_("A channel who's name begins with a \"+\" character is one that only allows the "
		"topic protection mode to be set, all other modes are set by the server.")},

	{0, NULL}
};


static GIrcChannelMode channel_modes[] = {
	{GIRC_CHANNEL_MODE_CHAR_ANONYMOUS,
	 GIRC_CHANNEL_MODE_ANONYMOUS,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("All messages on the channel are \"anonymous\" to other users on the channel. "
		"This mode can only be set by Channel Creators on safe (\"!\") channels, and "
		"Channel Operators on local (\"&\") channels.")},

	{GIRC_CHANNEL_MODE_CHAR_INVITE_ONLY,
	 GIRC_CHANNEL_MODE_INVITE_ONLY,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("Only users who have been invited by a user already on the channel, or are "
		"listed in the invite exceptions list can join the channel.")},

	{GIRC_CHANNEL_MODE_CHAR_MODERATED,
	 GIRC_CHANNEL_MODE_MODERATED,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("Only Channel Operators, Half-Operators, and Voiced users can speak on moderated "
		"channels.")},

	{GIRC_CHANNEL_MODE_CHAR_NOMSG,
	 GIRC_CHANNEL_MODE_NOMSG,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("Only users who are in the channel can speak.")},

	{GIRC_CHANNEL_MODE_CHAR_TOPIC_PROTECT,
	 GIRC_CHANNEL_MODE_TOPIC_PROTECT,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("Only Channel Operators and Half-Operators can change the channel's topic.")},

	{GIRC_CHANNEL_MODE_CHAR_REOP,
	 GIRC_CHANNEL_MODE_REOP,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("The channel will automatically give Channel Operator abilities to a random "
		"user on the channel if it has been without Operators for a period of time.")},

	{GIRC_CHANNEL_MODE_CHAR_PRIVATE,
	 GIRC_CHANNEL_MODE_PRIVATE,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("The channel does not appear in a user's information.")},
	{GIRC_CHANNEL_MODE_CHAR_SECRET,
	 GIRC_CHANNEL_MODE_SECRET,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("The channel does not appear in a user's information or any listing of available "
		"channels.")},
	{GIRC_CHANNEL_MODE_CHAR_QUIET,
	 GIRC_CHANNEL_MODE_QUIET,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM,
	 N_("The channel does not appear in a user's information or any listing of available "
		"channels, and other users on the channel are hidden.")},

	{GIRC_CHANNEL_MODE_CHAR_KEY,
	 GIRC_CHANNEL_MODE_KEY,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_PARAM,
	 N_("The channel requires a password to join.")},

	{GIRC_CHANNEL_MODE_CHAR_LIMIT,
	 GIRC_CHANNEL_MODE_LIMIT,
	 GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_OR_NO_PARAM,
	 N_("The channel has a limited number of users who can join.")},

	{GIRC_CHANNEL_MODE_CHAR_BAN,
	 GIRC_CHANNEL_MODE_NONE,
	 GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM,
	 N_("The channel has banned certain users from entering.")},

	{GIRC_CHANNEL_MODE_CHAR_BAN_EXCEPTIONS,
	 GIRC_CHANNEL_MODE_NONE,
	 GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM,
	 N_("The channel has allowed some users to join in spite of a ban.")},

	{GIRC_CHANNEL_MODE_CHAR_INVITE,
	 GIRC_CHANNEL_MODE_NONE,
	 GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM,
	 N_("The channel has invited certain users to join.")},

	{GIRC_CHANNEL_MODE_CHAR_INVITE_EXCEPTIONS,
	 GIRC_CHANNEL_MODE_NONE,
	 GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM,
	 N_("The channel has allowed some users to join without an invitation.")},

	{0, 0, 0, NULL}
};


static CharEnumString user_mode_descriptions[] = {
	{0,
	 GIRC_CHANNEL_USER_MODE_NONE,
	 N_("A normal user has no special abilities.")},

	{GIRC_CHANNEL_USER_MODE_CHAR_OPS,
	 GIRC_CHANNEL_USER_MODE_OPS,
	 N_("A Channel Operator has the ability to set channel modes, kick and ban other "
		"users from the channel, talk freely when the channel is moderated, and give "
		"other users these abilities.")},

	{GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS,
	 GIRC_CHANNEL_USER_MODE_HALFOPS,
	 N_("A Half-Operator has the ability to set channel modes, kick and ban other "
		"users (but not Channel Operators), talk freely when the channel is moderated, "
		"and give other users these limited abilities.")},

	{GIRC_CHANNEL_USER_MODE_CHAR_VOICE,
	 GIRC_CHANNEL_USER_MODE_VOICE,
	 N_("A Voiced user has the ability talk freely when the channel is moderated.")},

	{GIRC_CHANNEL_USER_MODE_CHAR_CREATOR,
	 GIRC_CHANNEL_USER_MODE_CREATOR,
	 N_("A Channel Creator is automatically selected by the server when a safe channel "
		"(\"!\") is created. A Channel Creator has the ability to set channel modes "
		"(including the anonymous and re-op modes), kick and ban other users from the "
		"channel, talk freely when the channel is moderated, and give other users "
		"Channel Operator abilities.")},

	{0, 0, NULL}
};


/* *********************** *
 *  GIrcChannel Functions  *
 * *********************** */


GType
girc_channel_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcChannel",
												   (GBoxedCopyFunc) girc_channel_ref,
												   (GBoxedFreeFunc) girc_channel_unref);
	}

	return boxed_type;
}


/**
 * girc_channel_new: 
 *
 * Creates a new #GIrcChannel-struct.
 *
 * Returns: a new #GIrcChannel-struct.
 *
 * Since: 1.0
 **/
GIrcChannel *
girc_channel_new (void)
{
	GIrcChannel *retval = NULL;

	retval = g_new0 (GIrcChannel, 1);

	retval->type = GIRC_TYPE_CHANNEL;
	retval->ref = 1;

	retval->users = g_hash_table_new_full (girc_userhost_hash, girc_userhost_equal,
										   g_free,
										   (GDestroyNotify) girc_channel_user_unref);

	return retval;
}


/**
 * girc_channel_ref: 
 * @channel: the channel data to reference.
 *
 * Increases the reference count on @channel by one. When the reference count drops to
 * zero, the channel data will be freed.
 * 
 * Returns: a new reference to @channel.
 *
 * Since: 1.0
 **/
GIrcChannel *
girc_channel_ref (GIrcChannel * channel)
{
	g_return_val_if_fail (GIRC_IS_CHANNEL (channel), NULL);

	channel->ref++;

	return channel;
}


/**
 * girc_channel_unref: 
 * @channel: the channel data to unreference.
 *
 * Decreases the reference count on @channel by one. When the reference count drops to
 * zero, the channel data will be freed.
 *
 * Since: 1.0
 **/
void
girc_channel_unref (GIrcChannel * channel)
{
	g_return_if_fail (GIRC_IS_CHANNEL (channel));

	channel->ref--;

	if (channel->ref == 0)
	{
		if (channel->name != NULL)
			g_free (channel->name);

		if (channel->topic != NULL)
			g_free (channel->topic);

		for (; channel->bans != NULL; channel->bans = g_slist_remove_link (channel->bans,
																		   channel->bans))
		{
			if (channel->bans->data != NULL)
				g_free (channel->bans->data);
		}

		for (; channel->ban_excepts != NULL;
			 channel->ban_excepts = g_slist_remove_link (channel->ban_excepts,
														 channel->ban_excepts))
		{
			if (channel->ban_excepts->data != NULL)
				g_free (channel->ban_excepts->data);
		}

		for (; channel->invites != NULL;
			 channel->invites = g_slist_remove_link (channel->invites, channel->invites))
		{
			if (channel->invites->data != NULL)
				g_free (channel->invites->data);
		}

		for (; channel->invite_excepts != NULL;
			 channel->invite_excepts = g_slist_remove_link (channel->invite_excepts,
															channel->invite_excepts))
		{
			if (channel->invite_excepts->data != NULL)
				g_free (channel->invite_excepts->data);
		}

		g_hash_table_destroy (channel->users);

		g_free (channel);
	}
}

/**
 * girc_channel_get_users:
 * @channel: the channel to examine.
 * 
 * Retrieves the channel's users in #GSList format. The data items should be
 * unreferenced using girc_channel_user_unref() when no longer needed.
 * 
 * Returns: a new #GSList containing references to the users on @channel.
 * 
 * Since: 1.0
 **/
GSList *
girc_channel_get_users (GIrcChannel * channel)
{
	return girc_g_hash_table_copy_to_slist (channel->users,
											(GCacheDupFunc) girc_channel_user_ref);
}


/* ****************************** *
 *  GIrcUserChannelOn Functions  *
 * ****************************** */

GType
girc_user_channel_on_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcUserChannelOn",
										  (GBoxedCopyFunc) girc_user_channel_on_dup,
										  (GBoxedFreeFunc) girc_user_channel_on_free);
	}

	return boxed_type;
}


/**
 * girc_user_channel_on_new: 
 *
 * Creates a new #GIrcUserChannelOn-struct. The returned data should be
 * freed with girc_user_channel_on_free() when no longer needed.
 *
 * Returns: a new #GIrcUserChannelOn-struct.
 *
 * Since: 1.0
 **/
GIrcUserChannelOn *
girc_user_channel_on_new (void)
{
	GIrcUserChannelOn *retval = NULL;

	retval = g_new0 (GIrcUserChannelOn, 1);

	retval->type = GIRC_TYPE_USER_CHANNEL_ON;

	return retval;
}


/**
 * girc_user_channel_on_dup:
 * @channel_on: the data to copy.
 * 
 * Copies the data in @channel_on. When the returned value is no longer needed,
 * it should be freed with girc_user_channel_on_free ().
 *
 * Returns: a reference to @channel_on.
 *
 * Since: 1.0
 **/
GIrcUserChannelOn *
girc_user_channel_on_dup (const GIrcUserChannelOn * channel_on)
{
	GIrcUserChannelOn *retval;

	g_return_val_if_fail (GIRC_IS_USER_CHANNEL_ON (channel_on), NULL);

	retval = girc_user_channel_on_new ();

	retval->name = g_strdup (channel_on->name);
	retval->modes = channel_on->modes;

	return retval;
}


/**
 * girc_user_channel_on_free:
 * @channel_on: the data to free.
 *
 * Frees the data used by @channel_on.
 *
 * Since: 1.0
 **/
void
girc_user_channel_on_free (GIrcUserChannelOn * channel_on)
{
	g_return_if_fail (GIRC_IS_USER_CHANNEL_ON (channel_on));

	if (channel_on->name != NULL)
		g_free (channel_on->name);

	g_free (channel_on);
}


/**
 * girc_user_channel_on_collate:
 * @chan1: the first channel to test.
 * @chan2: the second channel to test.
 * @encoding: the character encoding of the channel names.
 *
 * Compare two #GIrcUserChannelOn-struct structures, first by mode flags, then
 * by the channel name.
 *
 * Returns: an integer less than, equal to, or greater than zero if %chan1 is
 * found, respectively, to be less than, to match, or to be greater than @chan2.
 *
 * Since: 1.0
 **/
gint
girc_user_channel_on_collate (const GIrcUserChannelOn * chan1,
							   const GIrcUserChannelOn * chan2,
							   const gchar * encoding)
{
	g_return_val_if_fail (GIRC_IS_USER_CHANNEL_ON (chan1), 0);
	g_return_val_if_fail (GIRC_IS_USER_CHANNEL_ON (chan2), 0);

	if (chan1->modes > chan2->modes)
	{
		return -1;
	}
	else if (chan1->modes < chan2->modes)
	{
		return 1;
	}
	else
	{
		gchar *name1,
		 *name2;
		gint retval;

		name1 = girc_convert_to_utf8 (chan1->name, encoding);
		name2 = girc_convert_to_utf8 (chan2->name, encoding);

		if (name1 == NULL && name2 == NULL)
			retval = 0;
		else if (name1 == NULL && name2 != NULL)
			retval = 1;
		else if (name1 != NULL && name2 == NULL)
			retval = -1;
		else
			retval = g_utf8_collate (name1, name2);

		if (name1 != NULL)
			g_free (name1);

		if (name2 != NULL)
			g_free (name2);

		return retval;
	}
}


/* *************************** *
 *  GIrcChannelUser Functions  *
 * *************************** */


GType
girc_channel_user_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcChannelUser",
										  (GBoxedCopyFunc) girc_channel_user_ref,
										  (GBoxedFreeFunc) girc_channel_user_unref);
	}

	return boxed_type;
}


/**
 * girc_channel_user_new: 
 *
 * Creates a new #GIrcChannelUser-struct. The returned data should be freed
 * with girc_channel_user_free() when no longer needed.
 *
 * Returns: A new #GIrcChannelUser-struct.
 *
 * Since: 1.0
 **/
GIrcChannelUser *
girc_channel_user_new (void)
{
	GIrcChannelUser *user = NULL;

	user = g_new0 (GIrcChannelUser, 1);

	user->type = GIRC_TYPE_CHANNEL_USER;
	user->ref = 1;

	return user;
}


/**
 * girc_channel_user_ref:
 * @user: the channel user to reference.
 *
 * Increases the reference count of the data in @user by one. When the reference count
 * drops to zero, the structure will be freed.
 *
 * Returns: a new reference to @user.
 * 
 * Since: 1.0.
 **/
GIrcChannelUser *
girc_channel_user_ref (GIrcChannelUser * user)
{
	g_return_val_if_fail (GIRC_IS_CHANNEL_USER (user), NULL);

	user->ref++;

	return user;
}


/**
 * girc_channel_user_unref: 
 * @user: the channel user data to unreference.
 *
 * Decreases the reference count of the data in @user by one. When the reference count
 * drops to zero, the data will be freed.
 *
 * Since: 1.0
 **/
void
girc_channel_user_unref (GIrcChannelUser * user)
{
	g_return_if_fail (GIRC_IS_CHANNEL_USER (user));

	user->ref--;

	if (user->ref == 0)
	{
		if (user->user != NULL)
			girc_user_unref (user->user);

		g_free (user);
	}
}


/**
 * girc_channel_user_collate:
 * @user1: the first user.
 * @user2: the second user.
 * @encoding: the character encoding of the nicknames.
 * 
 * Test the equality of two #GIrcChannelUser-struct structures, useful for sorting.
 *
 * Returns: negative if user1 should be sorted first, positive if user2 should
 * be sorted first, and zero if they are equal.
 * 
 * Since: 1.0
 **/
gint
girc_channel_user_collate (const GIrcChannelUser * user1,
						   const GIrcChannelUser * user2,
						   const gchar * encoding)
{
	gint retval;

	if (user1 == NULL && user2 != NULL)
	{
		retval = 1;
	}
	else if (user1 != NULL && user2 == NULL)
	{
		retval = -1;
	}
	else if (user1 == user2)
	{
		retval = 0;
	}
	else
	{
		retval = girc_channel_user_mode_flags_collate (user1->modes, user2->modes);

		if (retval == 0)
		{
			gchar *nick1,
			 *nick2;

			nick1 = girc_convert_to_utf8 (user1->user->nick, encoding);
			nick2 = girc_convert_to_utf8 (user2->user->nick, encoding);

			if (nick1 != NULL && nick2 == NULL)
			{
				retval = -1;
			}
			else if (nick1 == NULL && nick2 != NULL)
			{
				retval = 1;
			}
			else
			{
				retval = g_utf8_collate (nick1, nick2);
			}

			if (nick1 != NULL)
				g_free (nick1);
			if (nick2 != NULL)
				g_free (nick2);
		}
	}

	return retval;
}


/* *************************** *
 *  GIrcChannelMask Functions  *
 * *************************** */


GType
girc_channel_mask_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcChannelMask",
										  (GBoxedCopyFunc) girc_channel_mask_dup,
										  (GBoxedFreeFunc) girc_channel_mask_free);
	}

	return boxed_type;
}


/**
 * girc_channel_mask_new: 
 *
 * Creates a new #GIrcChannelMask-struct. The returned data should be freed
 * with girc_channel_mask_free() when no longer needed.
 *
 * Returns: A new #GIrcChannelMask-struct.
 *
 * Since: 1.0
 **/
GIrcChannelMask *
girc_channel_mask_new (void)
{
	GIrcChannelMask *mask = NULL;

	mask = g_new0 (GIrcChannelMask, 1);

	mask->type = GIRC_TYPE_CHANNEL_MASK;

	return mask;
}


/**
 * girc_channel_mask_dup: 
 * @src: the channel mask data to copy.
 *
 * Copies an existing #GIrcChannelMask-struct. The returned data should be freed
 * with girc_channel_mask_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GIrcChannelMask *
girc_channel_mask_dup (const GIrcChannelMask * src)
{
	GIrcChannelMask *dest;

	g_return_val_if_fail (GIRC_IS_CHANNEL_MASK (src), NULL);

	dest = girc_channel_mask_new ();

	dest->mask = g_strdup (src->mask);
	dest->server = g_strdup (src->server);
	dest->set_on_time = src->set_on_time;

	return dest;
}


/**
 * girc_channel_mask_free: 
 * @mask: the channel mask data to free.
 *
 * Frees the memory associated with @mask.
 *
 * Since: 1.0
 **/
void
girc_channel_mask_free (GIrcChannelMask * mask)
{
	g_return_if_fail (GIRC_IS_CHANNEL_MASK (mask));

	if (mask->mask != NULL)
		g_free (mask->mask);

	if (mask->server != NULL)
		g_free (mask->server);

	g_free (mask);
}


/* ***************************** *
 *  GIrcChannelPrefix Functions  *
 * ***************************** */


GType
girc_channel_prefix_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcChannelPrefix",
										  (GBoxedCopyFunc) girc_channel_prefix_dup,
										  (GBoxedFreeFunc) girc_channel_prefix_free);
	}

	return boxed_type;
}


/**
 * girc_channel_prefix_get_prefix: 
 * @prefix_char: the channel prefix character
 *
 * Retrieves an appropriate #GIrcChannelPrefix-struct structure based on
 * @prefix_char. The returned data should not be modified or freed.
 *
 * Returns: the appropriate prefix data.
 *
 * Since: 1.0
 **/
G_CONST_RETURN GIrcChannelPrefix *
girc_channel_prefix_get_prefix (gchar prefix_char)
{
	GIrcChannelPrefix *prefix = NULL;
	guint i;

	for (i = 0; i < G_N_ELEMENTS (channel_prefixes) && prefix == NULL; i++)
	{
		if (channel_prefixes[i].prefix == prefix_char)
			prefix = (GIrcChannelPrefix *) & (channel_prefixes[i]);
	}

	return prefix;
}


/**
 * girc_channel_prefix_dup: 
 * @src: the channel prefix data to copy.
 *
 * Copies an existing #GIrcChannelPrefix-struct. The returned data should be freed
 * with girc_channel_prefix_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GIrcChannelPrefix *
girc_channel_prefix_dup (const GIrcChannelPrefix * src)
{
	GIrcChannelPrefix *dest = NULL;

	g_return_val_if_fail (src != NULL, NULL);

	dest = g_new0 (GIrcChannelPrefix, 1);

	dest->prefix = src->prefix;
	dest->description = g_strdup (src->description);

	return dest;
}


/**
 * girc_channel_prefix_free: 
 * @prefix: the channel prefix data to free.
 *
 * Frees the memory associated with @prefix.
 *
 * Since: 1.0
 **/
void
girc_channel_prefix_free (GIrcChannelPrefix * prefix)
{
	g_return_if_fail (prefix != NULL);

	if (prefix->description != NULL)
		g_free (prefix->description);

	g_free (prefix);
}


/* ******************************* *
 *  GIrcChannelUserMode Functions  *
 * ******************************* */


GType
girc_channel_user_mode_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcChannelUserMode",
										  (GBoxedCopyFunc) girc_channel_user_mode_dup,
										  (GBoxedFreeFunc) girc_channel_user_mode_free);
	}

	return boxed_type;
}


/**
 * girc_channel_user_mode_dup: 
 * @src: the channel user mode data to copy.
 *
 * Copies an existing #GIrcChannelUserMode-struct. The returned data should be
 * freed with girc_channel_user_mode_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GIrcChannelUserMode *
girc_channel_user_mode_dup (const GIrcChannelUserMode * src)
{
	GIrcChannelUserMode *dest = NULL;

	g_return_val_if_fail (src != NULL, NULL);

	/* We just do a raw memory copy rather than a series of assignments */
	dest = g_memdup (src, sizeof (GIrcChannelUserMode));

	/* Unfortunately, we need to deep-copy the string, rather than just the address of
	   the src's string */
	dest->description = g_strdup (src->description);

	return dest;
}


/**
 * girc_channel_user_mode_free: 
 * @mode: the channel user mode data to free.
 *
 * Frees the memory associated with @mode.
 *
 * Since: 1.0
 **/
void
girc_channel_user_mode_free (GIrcChannelUserMode * mode)
{
	g_return_if_fail (mode != NULL);

	if (mode->description != NULL)
		g_free (mode->description);

	g_free (mode);
}


/**
 * girc_channel_user_mode_get_description: 
 * @mode_char: the channel mode character.
 *
 * Retrieves a copy of the description for the mode corresponding to
 * @mode_char. The returned string should be freed when no longer needed.
 *
 * Returns: a copy of @mode_char's description.
 *
 * Since: 1.0
 **/
gchar *
girc_channel_user_mode_get_description (gchar mode_char)
{
	gint i;

	g_return_val_if_fail (mode_char == GIRC_CHANNEL_USER_MODE_CHAR_NONE
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_OPS
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_VOICE
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_CREATOR, NULL);

	for (i = 0; user_mode_descriptions[i].str != NULL; i++)
	{
		if (user_mode_descriptions[i].ch == mode_char)
			return g_strdup (user_mode_descriptions[i].str);
	}

	return NULL;
}


/**
 * girc_channel_user_mode_new: 
 * @mode_char: the channel mode character.
 * @prefix_char: the mode character used in channel lists.
 *
 * Creates a new #GIrcChannelUserMode-struct based on the data passed. The
 * returned data should be freed with girc_channel_user_mode_free() when no
 * longer needed.
 *
 * Returns: a new channel user mode data structure.
 *
 * Since: 1.0
 **/
GIrcChannelUserMode *
girc_channel_user_mode_new (gchar mode_char,
							gchar prefix_char)
{
	GIrcChannelUserMode *user_mode = NULL;
	guint i;

	g_return_val_if_fail (mode_char == GIRC_CHANNEL_USER_MODE_CHAR_NONE
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_OPS
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_VOICE
						  || mode_char == GIRC_CHANNEL_USER_MODE_CHAR_CREATOR, NULL);

	user_mode = g_new0 (GIrcChannelUserMode, 1);
	user_mode->type = GIRC_TYPE_CHANNEL_USER_MODE;

	user_mode->mode_char = mode_char;
	user_mode->prefix_char = prefix_char;

	for (i = 0;
		 i < G_N_ELEMENTS (user_mode_descriptions) && user_mode->description == NULL; i++)
	{
		if (user_mode_descriptions[i].ch == mode_char)
		{
			user_mode->mode = user_mode_descriptions[i].val;
			user_mode->description = g_strdup (user_mode_descriptions[i].str);
		}
	}

	return user_mode;
}


/* *************************** *
 *  GIrcChannelMode Functions  *
 * *************************** */


GType
girc_channel_mode_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type =
			g_boxed_type_register_static ("GIrcChannelMode",
										  (GBoxedCopyFunc) girc_channel_mode_dup,
										  (GBoxedFreeFunc) girc_channel_mode_free);
	}

	return boxed_type;
}


/**
 * girc_channel_mode_dup: 
 * @src: the channel mode data to copy.
 *
 * Copies an existing #GIrcChannelMode-struct. The returned data should be
 * freed with girc_channel_mode_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GIrcChannelMode *
girc_channel_mode_dup (const GIrcChannelMode * src)
{
	GIrcChannelMode *dest;

	g_return_val_if_fail (src != NULL, NULL);

	dest = g_memdup (src, sizeof (GIrcChannelMode));

	dest->description = g_strdup (src->description);

	return dest;
}


/**
 * girc_channel_mode_free: 
 * @mode: the channel mode data to free.
 *
 * Frees the memory associated with @mode.
 *
 * Since: 1.0
 **/
void
girc_channel_mode_free (GIrcChannelMode * mode)
{
	g_return_if_fail (mode != NULL);

	if (mode->description != NULL)
		g_free (mode->description);

	g_free (mode);
}


/**
 * girc_channel_mode_new_from_char: 
 * @mode_char: the channel mode character.
 *
 * Retrieves information on the channel mode set by @mode_char. The returned
 * data should not be modified or freed.
 *
 * Returns: a #GIrcChannelMode-struct for the @mode_char channel mode.
 *
 * Since: 1.0
 **/
G_CONST_RETURN GIrcChannelMode *
girc_channel_mode_new_from_char (gchar mode_char)
{
	GIrcChannelMode *mode = NULL;
	guint i;

	for (i = 0; i < G_N_ELEMENTS (channel_modes) && mode == NULL; i++)
	{
		if (channel_modes[i].mode_char == mode_char)
			mode = (GIrcChannelMode *) & (channel_modes[i]);
	}

	return mode;
}


/* ************************* *
 *  Miscellaneous Functions  *
 * ************************* */


/**
 * girc_channel_user_mode_flags_collate:
 * @modes1: the first modes.
 * @modes2: the second modes.
 * 
 * Test the equality of two #GIrcChannelUserModeFlags items, useful for sorting.
 *
 * Returns: negative if modes1 should be sorted first, positive if modes2 should
 * be sorted first, and zero if they are equal.
 * 
 * Since: 1.0
 **/
gint
girc_channel_user_mode_flags_collate (const GIrcChannelUserModeFlags modes1,
									  const GIrcChannelUserModeFlags modes2)
{
	gint retval = 0;

	if (modes1 == modes2)
	{
		retval = 0;
	}
	else if (modes1 > modes2)
	{
		if (modes1 & GIRC_CHANNEL_USER_MODE_OPS
			|| modes1 & GIRC_CHANNEL_USER_MODE_HALFOPS
			|| modes1 & GIRC_CHANNEL_USER_MODE_CREATOR)
		{
			if (modes1 & GIRC_CHANNEL_USER_MODE_VOICE)
			{
				/* modes1 == ops+voice, modes2 == ops */
				if (modes2 & GIRC_CHANNEL_USER_MODE_OPS
					|| modes2 & GIRC_CHANNEL_USER_MODE_HALFOPS
					|| modes2 & GIRC_CHANNEL_USER_MODE_CREATOR)
				{
					retval = 1;
				}
				/* modes1 == ops+voice, modes2 <= voice */
				else
				{
					retval = -1;
				}
			}
			/* modes1 == ops, modes2 < ops */
			else
			{
				retval = -1;
			}
		}
		/* modes1 == voice, modes2 == normal */
		else
		{
			retval = -1;
		}
	}
	/* modes1 < modes2 */
	else if (modes2 & GIRC_CHANNEL_USER_MODE_OPS
			 || modes2 & GIRC_CHANNEL_USER_MODE_HALFOPS
			 || modes2 & GIRC_CHANNEL_USER_MODE_CREATOR)
	{
		if (modes2 & GIRC_CHANNEL_USER_MODE_VOICE)
		{
			/* modes2 == ops+voice, modes1 == ops */
			if (modes1 & GIRC_CHANNEL_USER_MODE_OPS
				|| modes1 & GIRC_CHANNEL_USER_MODE_HALFOPS
				|| modes1 & GIRC_CHANNEL_USER_MODE_CREATOR)
			{
				retval = -1;
			}
			/* modes2 == ops+voice, modes1 <= voice */
			else
			{
				retval = 1;
			}
		}
		/* modes2 == ops, modes1 <= voice */
		else
		{
			retval = 1;
		}
	}
	/* modes2 == voice, modes1 == normal */
	else
	{
		retval = 1;
	}

	return retval;
}
