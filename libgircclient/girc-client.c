/*
 * LibGIrcClient: libgircclient/girc-client.c
 *
 * Copyright (c) 2002 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "girc-client.h"
#include "girc-type-utils.h"
#include "girc-type-builtins.h"

#include "girc-client-private.h"
#include "girc-commands.h"
#include "marshal.h"
#include "parser.h"


#include <libgtcpsocket/gtcp-i18n.h>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Various code-cleanup stuff */
#define SEND_MSG_SIZE 509
#define EOL_STR "\r\n"


enum
{
	PROP_0,

	/* What we want to use */
	PROP_PREF_NICK,

	/* What we're actually using */
	PROP_NICK,
	PROP_PASS,
	PROP_HOSTNAME,
	PROP_USERNAME,
	PROP_REALNAME,

	PROP_AWAY,

	/* Who we're connected to */
	PROP_SERVER_NAME,
	/* What we think/know the server expects/sends */
	PROP_ENCODING,

	/* GIrcChannel, GIrcQuery */
	PROP_TARGETS,
	PROP_CHANNELS,
	PROP_QUERIES,

	/* The message of the day */
	PROP_MOTD,

	/* Server Environment */
	/* Environment lists */
	PROP_VAR_USER_PREFIXES,
	PROP_VAR_CHANNEL_TYPES,
	PROP_VAR_CHANNEL_MODES,
	PROP_VAR_LANGUAGES,

	/* Supported commands */
	PROP_VAR_KNOCK_CMD,
	PROP_VAR_USERIP_CMD,
	PROP_VAR_CJOIN_CMD,
	PROP_VAR_CPRIVMSG_CMD,
	PROP_VAR_CNOTICE_CMD,
	PROP_VAR_WALLCHOPS_CMD,
	PROP_VAR_LANGUAGE_CMD,

	/* Supported features & feature types */
	PROP_VAR_PENALTY,
	PROP_VAR_FORCED_NICKCHANGE,
	PROP_VAR_BAN_EXCEPTIONS,
	PROP_VAR_INVITE_EXCEPTIONS,
	PROP_VAR_WHO_IS_WHOX,
	PROP_VAR_SAFE_LIST,
	PROP_VAR_SERVER_SIDE_IGNORE,

	/* Environment */
	PROP_VAR_CHARSET,
	PROP_VAR_CHARMAPPING,
	PROP_VAR_NETWORK_NAME,

	/* Limits */
	PROP_VAR_MAX_CHANNELS,
	PROP_VAR_MAX_BANS,
	PROP_VAR_NICK_LENGTH,
	PROP_VAR_TOPIC_LENGTH,
	PROP_VAR_KICK_LENGTH,
	PROP_VAR_CHANNEL_LENGTH,
	PROP_VAR_BANG_CHANID_LENGTH,
	PROP_VAR_MAX_TARGETS,
	PROP_VAR_MAX_WATCHES,
	PROP_VAR_PER_MODE_CMDS,
	PROP_VAR_SILENCE_LIST_SIZE
};

enum
{
	CONNECT_COMPLETE,
	QUIT,

	NICK_CHANGED,
	CTCP_RECV,

	TARGET_OPENED,
	TARGET_CLOSED,
	TARGET_RECV,

	QUERY_CHANGED,

	CHANNEL_CHANGED,
	CHANNEL_USER_CHANGED,

	MODE_RECV,
	WHOIS_RECV,
	LIST_RECV,
	NAMES_RECV,
	MOTD_RECV,
	NOTICE_RECV,

	MSG_RECV,
	ERROR_RECV,

	LAST_SIGNAL
};


typedef struct _GIrcPing GIrcPing;

struct _GIrcPing
{
	gchar *target;
	GTime start_time;
};


static gint signals[LAST_SIGNAL] = { 0 };
static gpointer parent_class = NULL;


/* ******************* *
 *  UTILITY FUNCTIONS  *
 * ******************* */

/* Just a funky function to convert a string to it's e|33+ variant */
/*
static void
leetify_string (gchar * str)
{
	if (str == NULL)
		return;

	for (; *str != '\0'; str = g_utf8_next_char (str))
	{
		gunichar ch = g_utf8_get_char (str);

		switch (ch)
		{
		case 'a':
		case 'A':
			*str = '4';
			break;
		case 'e':
		case 'E':
			*str = '3';
			break;
		case 'i':
		case 'I':
			*str = '1';
			break;
		case 'l':
		case 'L':
			*str = '|';
			break;
		case 't':
			*str = '+';
			break;
		case 'T':
			*str = '7';
			break;
		case 'o':
		case 'O':
			*str = '0';
			break;
		case 'u':
		case 'U':
			*str = 'v';
			break;
		default:
		}
	}
}
*/


static void
listify_channels (gpointer key,
				  gpointer value,
				  GSList ** data)
{
	if (GIRC_IS_CHANNEL (value))
		*data = g_slist_prepend (*data, value);
}


static void
listify_queries (gpointer key,
				 gpointer value,
				 GSList ** data)
{
	if (GIRC_IS_QUERY (value))
		*data = g_slist_prepend (*data, value);
}


/* *************************** *
 *  GENERIC PRIVATE FUNCTIONS  *
 * *************************** */

/* Runs an "ISON" command on all the nicknames in the notify list */
static gboolean
user_notify_check (gpointer data)
{
	GIrcClient *irc = GIRC_CLIENT (data);
	GSList *list = irc->_priv->notifies;
	GString *str;

	if (list == NULL || irc->_priv->notify_id == -1)
	{
		irc->_priv->notify_id = -1;
		return FALSE;
	}

	while (list != NULL)
	{
		str = g_string_sized_new (SEND_MSG_SIZE);
		g_string_append (str, GIRC_COMMAND_ISON);

		/* Assumption that nobody will have a nickname > 47 chars long */
		while (list != NULL && str->len < SEND_MSG_SIZE - 48)
		{
			g_string_append_c (str, ' ');
			g_string_append (str, (gchar *) list->data);
			list = list->next;
		}

		girc_client_send_raw (irc, str->str);

		g_string_erase (str, 0, str->len);
	}

	g_string_free (str, TRUE);

	return TRUE;
}


/* ********************** *
 *  GIrcClient Callbacks  *
 * ********************** */

static void
girc_client_target_closed (GIrcClient * irc,
						   GIrcTarget * target,
						   const gchar * reason)
{
	if (GIRC_IS_CHANNEL (target))
	{
		g_hash_table_remove (irc->_priv->targets, target->channel.name);
	}
	else if (GIRC_IS_QUERY (target))
	{
		g_hash_table_remove (irc->_priv->targets, target->query.userhost);
	}
	else
	{
		g_assert_not_reached ();
	}
}


static void
girc_client_names_recv (GIrcClient * irc,
						GSList * names_list)
{
	GSList *list;

	for (list = names_list; list != NULL; list = list->next)
	{
		if (GIRC_IS_NAMES_ITEM (list->data))
		{
			GIrcNamesItem *item = GIRC_NAMES_ITEM (names_list->data);
			GIrcChannel *channel = GIRC_CHANNEL (g_hash_table_lookup (irc->_priv->targets,
																	  item->channel));

			if (channel != NULL)
			{
				GSList *users;

				girc_g_hash_table_clear (channel->users);

				for (users = item->users; users != NULL; users = users->next)
				{
					GIrcChannelUser *user = NULL;

					if (users->data != NULL && *((gchar *) (users->data)) != '\0')
					{
						user = _girc_parser_parse_channel_user (irc, users->data);

						if (user != NULL)
						{
							if (user->user == irc->_priv->my_user)
							{
								channel->my_user = user;
							}

							g_hash_table_insert (channel->users,
												 girc_user_to_string (user->user), user);
						}
						else
						{
							g_warning ("Unable to parse user: \"%s\"",
									   (gchar *) users->data);
						}
					}
				}

				g_signal_emit (irc, signals[CHANNEL_CHANGED], 0,
							   channel, NULL, GIRC_CHANNEL_INFO_USERS, NULL, FALSE);
			}
		}
	}
}


/* ************************** *
 *  GTcpConnection Callbacks  *
 * ************************** */

static void
girc_client_recv (GTcpConnection * conn,
				  gconstpointer raw_data,
				  gsize length)
{
	gchar *data;
	GMainContext *context = g_main_context_default ();

	if (GTCP_CONNECTION_CLASS (parent_class)->recv)
		GTCP_CONNECTION_CLASS (parent_class)->recv (conn, raw_data, length);

	if (raw_data == NULL || length == 0)
		return;

	/* Make sure the data is NUL-terminated by allocating a new buffer length + 1
	   bytes long */
	data = g_malloc (length + 1);
	memcpy (data, raw_data, length);
	data[length] = '\0';

	while (g_main_context_pending (context))
		g_main_context_iteration (context, FALSE);

	_girc_parser_parse_incoming (GIRC_CLIENT (conn), data);
}


static void
girc_client_closed (GTcpConnection * conn,
					gboolean requested)
{
	GIrcClient *irc = GIRC_CLIENT (conn);
	GIrcClientStatus status;

	if (GTCP_CONNECTION_CLASS (parent_class)->closed)
		GTCP_CONNECTION_CLASS (parent_class)->closed (conn, requested);

	status = irc->_priv->status;
	irc->_priv->status = GIRC_CLIENT_CLOSED;

	irc->_priv->nick_attempts = 0;

	irc->_priv->vars->max_nick_length = -1;
	g_object_notify (G_OBJECT (conn), "nick-length");

	if (status == GIRC_CLIENT_LOGGING_IN)
	{
		g_signal_emit (irc, signals[CONNECT_COMPLETE], 0, GIRC_CLIENT_CLOSED,
					   NULL, NULL, NULL);
	}
	else if (status == GIRC_CLIENT_CLOSING)
	{
		g_signal_emit (irc, signals[QUIT], 0, TRUE);
	}
	else
	{
		g_signal_emit (irc, signals[QUIT], 0, FALSE);
	}
}


static void
girc_client_connect_done (GTcpConnection * conn,
						  GTcpConnectionStatus status)
{
	GIrcClient *irc = GIRC_CLIENT (conn);

	if (GTCP_CONNECTION_CLASS (parent_class)->connect_done)
		GTCP_CONNECTION_CLASS (parent_class)->connect_done (conn, status);

	if (status != GTCP_CONNECTION_CONNECTED)
	{
		irc->_priv->status = status;
		g_signal_emit (irc, signals[CONNECT_COMPLETE], 0, status, NULL, NULL);
		irc->_priv->status = GIRC_CLIENT_CLOSED;
		return;
	}

	irc->_priv->status = GIRC_CLIENT_LOGGING_IN;
	girc_client_set_nick (irc, irc->_priv->preferred_nick);
}


/* ******************* *
 *  GObject Callbacks  *
 * ******************* */

static void
girc_client_set_property (GObject * object,
						  guint property,
						  const GValue * value,
						  GParamSpec * param_spec)
{
	GIrcClient *irc = GIRC_CLIENT (object);

	switch (property)
	{
	case PROP_PREF_NICK:
		if (irc->_priv->preferred_nick != NULL)
			g_free (irc->_priv->preferred_nick);

		irc->_priv->preferred_nick = g_value_dup_string (value);
		g_object_notify (object, "preferred-nick");
	case PROP_NICK:
		if (irc->_priv->status > GIRC_CLIENT_CLOSED)
		{
			girc_client_set_nick (irc, g_value_get_string (value));
		}
		break;

	case PROP_USERNAME:
		if (irc->_priv->my_user->username != NULL)
			g_free (irc->_priv->my_user->username);

		irc->_priv->my_user->username = g_value_dup_string (value);
		g_object_notify (object, "username");
		break;
	case PROP_REALNAME:
		if (irc->_priv->my_user->realname != NULL)
			g_free (irc->_priv->my_user->realname);

		irc->_priv->my_user->realname = g_value_dup_string (value);
		g_object_notify (object, "realname");
		break;
	case PROP_HOSTNAME:
		if (irc->_priv->my_user->hostname != NULL)
			g_free (irc->_priv->my_user->hostname);

		irc->_priv->my_user->hostname = g_value_dup_string (value);
		g_object_notify (object, "hostname");
		break;
	case PROP_PASS:
		if (irc->_priv->pass != NULL)
			g_free (irc->_priv->pass);

		irc->_priv->pass = g_value_dup_string (value);
		g_object_notify (object, "pass");
		break;
	case PROP_ENCODING:
		girc_client_set_encoding (irc, g_value_get_string (value));
		break;
	case PROP_AWAY:
		girc_client_set_away (irc, g_value_get_boolean (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
girc_client_get_property (GObject * object,
						  guint property,
						  GValue * value,
						  GParamSpec * param_spec)
{
	GIrcClient *irc = GIRC_CLIENT (object);

	switch (property)
	{
	case PROP_PREF_NICK:
		g_value_set_string (value, irc->_priv->preferred_nick);
		break;
	case PROP_NICK:
		g_value_set_string (value, irc->_priv->my_user->nick);
		break;
	case PROP_USERNAME:
		g_value_set_string (value, irc->_priv->my_user->username);
		break;
	case PROP_HOSTNAME:
		g_value_set_string (value, irc->_priv->my_user->hostname);
		break;
	case PROP_REALNAME:
		g_value_set_string (value, irc->_priv->my_user->realname);
		break;
	case PROP_PASS:
		g_value_set_string (value, irc->_priv->pass);
		break;
	case PROP_ENCODING:
		g_value_set_string (value, irc->_priv->encoding);
		break;
	case PROP_AWAY:
		g_value_set_boolean (value, irc->_priv->away);
		break;

	case PROP_SERVER_NAME:
		g_value_set_string (value, irc->_priv->vars->servername);
		break;
	case PROP_MOTD:
		g_value_set_string (value, irc->_priv->vars->motd);
		break;

		/* Targets */
	case PROP_CHANNELS:
		{
			GSList *channels = NULL;

			g_hash_table_foreach (irc->_priv->targets, (GHFunc) listify_channels,
								  &channels);
			girc_g_value_set_value_array_from_slist (value, channels, GIRC_TYPE_CHANNEL);
		}
		break;
	case PROP_QUERIES:
		{
			GSList *queries = NULL;

			g_hash_table_foreach (irc->_priv->targets, (GHFunc) listify_queries,
								  &queries);
			girc_g_value_set_value_array_from_slist (value, queries, GIRC_TYPE_QUERY);
		}
		break;

		/* Server variables */
		/* Lists */
	case PROP_VAR_USER_PREFIXES:
		girc_g_value_set_value_array_from_slist (value, irc->_priv->vars->user_modes,
												 GIRC_TYPE_CHANNEL_USER_MODE);
		break;
	case PROP_VAR_CHANNEL_TYPES:
		girc_g_value_set_value_array_from_slist (value, irc->_priv->vars->channel_prefixes,
												 GIRC_TYPE_CHANNEL_PREFIX);
		break;
	case PROP_VAR_CHANNEL_MODES:
		girc_g_value_set_value_array_from_slist (value, irc->_priv->vars->channel_modes,
												 GIRC_TYPE_CHANNEL_MODE);
		break;
	case PROP_VAR_LANGUAGES:
		girc_g_value_set_value_array_from_slist (value, irc->_priv->vars->languages,
												 G_TYPE_STRING);
		break;

		/* Supported commands */
	case PROP_VAR_KNOCK_CMD:
		g_value_set_boolean (value, irc->_priv->vars->knock_cmd);
		break;
	case PROP_VAR_USERIP_CMD:
		g_value_set_boolean (value, irc->_priv->vars->userip_cmd);
		break;
	case PROP_VAR_CJOIN_CMD:
		g_value_set_boolean (value, irc->_priv->vars->cjoin_cmd);
		break;
	case PROP_VAR_CPRIVMSG_CMD:
		g_value_set_boolean (value, irc->_priv->vars->cprivmsg_cmd);
		break;
	case PROP_VAR_CNOTICE_CMD:
		g_value_set_boolean (value, irc->_priv->vars->cnotice_cmd);
		break;
	case PROP_VAR_WALLCHOPS_CMD:
		g_value_set_boolean (value, irc->_priv->vars->wallchops_cmd);
		break;
	case PROP_VAR_LANGUAGE_CMD:
		g_value_set_boolean (value, irc->_priv->vars->language_cmd);
		break;

		/* Supported features */
	case PROP_VAR_PENALTY:
		g_value_set_boolean (value, irc->_priv->vars->penalty);
		break;
	case PROP_VAR_FORCED_NICKCHANGE:
		g_value_set_boolean (value, irc->_priv->vars->forced_nickchange);
		break;
	case PROP_VAR_BAN_EXCEPTIONS:
		g_value_set_boolean (value, irc->_priv->vars->ban_exceptions);
		break;
	case PROP_VAR_INVITE_EXCEPTIONS:
		g_value_set_boolean (value, irc->_priv->vars->invite_exceptions);
		break;
	case PROP_VAR_WHO_IS_WHOX:
		g_value_set_boolean (value, irc->_priv->vars->who_is_whox);
		break;
	case PROP_VAR_SAFE_LIST:
		g_value_set_boolean (value, irc->_priv->vars->safe_list);
		break;
	case PROP_VAR_SERVER_SIDE_IGNORE:
		g_value_set_boolean (value, irc->_priv->vars->server_side_ignore);
		break;

		/* Environment */
	case PROP_VAR_CHARMAPPING:
		g_value_set_enum (value, irc->_priv->vars->char_mapping);
		break;
	case PROP_VAR_NETWORK_NAME:
		g_value_set_string (value, irc->_priv->vars->network_name);
		break;

		/* Unused */
	case PROP_VAR_CHARSET:
		g_value_set_string (value, irc->_priv->vars->charset);
		break;

		/* Limits */
	case PROP_VAR_MAX_CHANNELS:
		g_value_set_uint (value, irc->_priv->vars->max_channels);
		break;
	case PROP_VAR_MAX_BANS:
		g_value_set_uint (value, irc->_priv->vars->max_num_of_bans);
		break;
	case PROP_VAR_NICK_LENGTH:
		g_value_set_int (value, irc->_priv->vars->max_nick_length);
		break;
	case PROP_VAR_TOPIC_LENGTH:
		g_value_set_uint (value, irc->_priv->vars->max_topic_length);
		break;
	case PROP_VAR_KICK_LENGTH:
		g_value_set_uint (value, irc->_priv->vars->max_kickmsg_length);
		break;
	case PROP_VAR_CHANNEL_LENGTH:
		g_value_set_uint (value, irc->_priv->vars->max_channel_length);
		break;
	case PROP_VAR_BANG_CHANID_LENGTH:
		g_value_set_uint (value, irc->_priv->vars->chanid_length);
		break;
	case PROP_VAR_MAX_TARGETS:
		g_value_set_uint (value, irc->_priv->vars->max_targets);
		break;
	case PROP_VAR_MAX_WATCHES:
		g_value_set_uint (value, irc->_priv->vars->max_watches);
		break;
	case PROP_VAR_PER_MODE_CMDS:
		g_value_set_uint (value, irc->_priv->vars->max_per_mode_cmds);
		break;
	case PROP_VAR_SILENCE_LIST_SIZE:
		g_value_set_uint (value, irc->_priv->vars->silence_list_size);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
girc_client_finalize (GObject * object)
{
	GIrcClient *irc = GIRC_CLIENT (object);

	if (irc->_priv->status >= GIRC_CLIENT_CONNECTED)
		girc_client_quit (irc, NULL);

	g_free (irc->_priv->preferred_nick);

	if (irc->_priv->my_user != NULL)
		girc_user_unref (irc->_priv->my_user);

	g_free (irc->_priv->pass);
	g_free (irc->_priv->encoding);

	if (irc->_priv->converter != (GIConv) - 1)
		g_iconv_close (irc->_priv->converter);

	g_free (irc->_priv->vars->servername);
	g_free (irc->_priv->vars->motd);
	g_free (irc->_priv->vars->network_name);
	g_free (irc->_priv->vars->charset);

	g_slist_free (irc->_priv->vars->channel_modes);
	g_slist_free (irc->_priv->vars->channel_prefixes);
	girc_g_slist_deep_free (irc->_priv->vars->languages, g_free);
	girc_g_slist_deep_free (irc->_priv->vars->user_modes,
							(GFreeFunc) girc_channel_user_mode_free);

	g_free (irc->_priv->vars);

	g_hash_table_destroy (irc->_priv->targets);

	g_free (irc->_priv->buffer);

	for (; irc->_priv->waiting_pings != NULL;
		 irc->_priv->waiting_pings = g_slist_remove (irc->_priv->waiting_pings,
													 irc->_priv->waiting_pings->data))
	{
		if (irc->_priv->waiting_pings->data != NULL)
		{
			if (((GIrcPing *) (irc->_priv->waiting_pings->data))->target != NULL)
				g_free (((GIrcPing *) (irc->_priv->waiting_pings->data))->target);

			g_free (irc->_priv->waiting_pings->data);
		}
	}

	g_hash_table_destroy (irc->_priv->names_reply);


	if (irc->_priv->waiting_whois != NULL)
		girc_user_unref (irc->_priv->waiting_whois);

	g_free (irc->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
girc_client_class_init (GIrcClientClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GTcpConnectionClass *conn_class = GTCP_CONNECTION_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = girc_client_set_property;
	object_class->get_property = girc_client_get_property;
	object_class->finalize = girc_client_finalize;

	conn_class->connect_done = girc_client_connect_done;
	conn_class->recv = girc_client_recv;
	conn_class->closed = girc_client_closed;

	class->target_closed = girc_client_target_closed;
	class->names_recv = girc_client_names_recv;

	g_object_class_install_property (object_class, PROP_NICK,
									 g_param_spec_string
									 ("nick", _("Nickname"),
									  _("The nickname currently being used."),
									  NULL, G_PARAM_READWRITE));
	g_object_class_install_property (object_class, PROP_PREF_NICK,
									 g_param_spec_string
									 ("preferred-nick",
									  _("Preferred Nickname"),
									  _("The user's preferred nickname."),
									  NULL, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_USERNAME,
									 g_param_spec_string ("username",
														  _("User Name"),
														  _("The local user name of the "
															"user."),
														  NULL,
														  (G_PARAM_READWRITE |
														   G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_REALNAME,
									 g_param_spec_string ("realname", _("Real Name"),
														  _("The real name of the user."),
														  NULL,
														  (G_PARAM_READWRITE |
														   G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_HOSTNAME,
									 g_param_spec_string ("hostname", _("Hostname"),
														  _("The local host name."), NULL,
														  G_PARAM_READWRITE));
	g_object_class_install_property (object_class, PROP_PASS,
									 g_param_spec_string ("pass", _("Password"),
														  _("The password for this "
															"session."),
														  NULL,
														  (G_PARAM_READWRITE |
														   G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_SERVER_NAME,
									 g_param_spec_string ("server-name", _("Server Name"),
														  _("The name of the server."),
														  NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Server's Encoding"),
														  _("The character set we "
															"believe the server "
															"expects."), NULL,
														  G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
									 PROP_AWAY,
									 g_param_spec_boolean
									 ("away",
									  _("User Is Marked Away"),
									  _("Whether the user is marked away or not."),
									  FALSE, G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_CHANNELS,
									 g_param_spec_value_array ("channels",
															   _("Channels Array"),
															   _("An GValueArray of "
																 "GIrcChannel structures "
																 "for the channels this "
																 "GIrcClient is on."),
															   g_param_spec_boxed
															   ("channel-entry", NULL,
																NULL, GIRC_TYPE_CHANNEL,
																G_PARAM_READWRITE),
															   G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_QUERIES,
									 g_param_spec_value_array ("queries",
															   _("Queries Array"),
															   _("A GValueArray of "
																 "GIrcQuery structures "
																 "for the user queries "
																 "this GIrcClient has "
																 "open."),
															   g_param_spec_boxed
															   ("query-entry", NULL, NULL,
																GIRC_TYPE_QUERY,
																G_PARAM_READWRITE),
															   G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_MOTD,
									 g_param_spec_string ("motd",
														  _("Message Of The Day"),
														  _("The server's introductory "
															"message."),
														  NULL, G_PARAM_READABLE));

	/* IRC Server environment variables */
	/* Miscellaneous */
	g_object_class_install_property (object_class,
									 PROP_VAR_NETWORK_NAME,
									 g_param_spec_string
									 ("network-name",
									  _("Network Name"),
									  _("The name of the network this server is on."),
									  NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_CHARMAPPING,
									 g_param_spec_enum
									 ("character-mapping",
									  _("Character Mapping Style"),
									  _("The character mapping the server uses (rfc1459 "
										"or ascii)."),
									  GIRC_TYPE_CLIENT_CHARACTER_MAPPING_TYPE,
									  GIRC_CLIENT_CHARACTER_MAPPING_UNKNOWN,
									  G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_CHARSET,
									 g_param_spec_string
									 ("server-character-set",
									  _("Expected Character Set"),
									  _("The character set the server uses."),
									  NULL, G_PARAM_READABLE));
	/* Limits */
	g_object_class_install_property (object_class,
									 PROP_VAR_MAX_CHANNELS,
									 g_param_spec_uint
									 ("max-channels",
									  _("Channels Limit"),
									  _("The maximum number of channels a user can "
										"join at any one time."), 0,
									  G_MAXUSHORT, 0, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_SILENCE_LIST_SIZE,
									 g_param_spec_uint ("max-bans",
														_("Maximum Number Of Bans"),
														_("The maximum number of items "
														  "allowed in a channel's ban "
														  "list."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_NICK_LENGTH,
									 g_param_spec_uint ("nick-length",
														_("Maximum Nickname Size"),
														_("The maximum number of "
														  "characters allowed in a "
														  "nickname."),
														0, G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_TOPIC_LENGTH,
									 g_param_spec_uint ("topic-length",
														_("Maximum Topic Size"),
														_("The maximum number of bytes "
														  "allowed in a channel's topic."),
														0, G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_KICK_LENGTH,
									 g_param_spec_uint ("kick-message-length",
														_("Maximum Kick Message Size"),
														_("The maximum number of bytes "
														  "allowed in a KICK command's "
														  "message."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_BANG_CHANID_LENGTH,
									 g_param_spec_uint ("channel-id-length",
														_("Channel ID Length"),
														_("The number of characters used "
														  "in a \"!\" "
														  "channel's ID."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_CHANNEL_LENGTH,
									 g_param_spec_uint ("channel-name-length",
														_("Channel Name Length"),
														_("The maximum number of "
														  "characters which can be "
														  "used in a channel name."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_MAX_TARGETS,
									 g_param_spec_uint ("max-targets",
														_("Maximum Number of Targets"),
														_("The maximum number of targets "
														  "for a CPRIVMSG, WALLCHOPS, or "
														  "CNOTICE command."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_MAX_WATCHES,
									 g_param_spec_uint ("max-watches",
														_("Maximum Number of Watches"),
														_("The maximum number of watches "
														  "the user can have."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_PER_MODE_CMDS,
									 g_param_spec_uint ("mode-command-size",
														_("Maximum Number of Modes"),
														_("The maximum number of modes "
														  "that can be set "
														  "for each use of the MODE "
														  "command +bbb, for "
														  "example, is 3."), 0,
														G_MAXUSHORT, 0,
														G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_SILENCE_LIST_SIZE,
									 g_param_spec_uint ("silence-list-size",
														_("Silence Command Size"),
														_("The maximum number of items "
														  "allowed in the SILENCE list. "
														  "If zero, the command is not "
														  "supported."), 0, G_MAXUSHORT,
														0, G_PARAM_READABLE));
	/* Lists */
	g_object_class_install_property (object_class,
									 PROP_VAR_USER_PREFIXES,
									 g_param_spec_value_array
									 ("channel-user-modes",
									  _("Channel User Modes"),
									  _("An array of GIrcChannelUserMode structures that "
										"the server supports."),
									  g_param_spec_boxed
									  ("user-prefix-entry", NULL,
									   NULL,
									   GIRC_TYPE_CHANNEL_USER_MODE,
									   G_PARAM_READWRITE), G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_CHANNEL_TYPES,
									 g_param_spec_value_array
									 ("channel-types",
									  _("Channel Types"),
									  _("A GValueArray of GIrcChannelPrefix structures "
										"that the server supports."),
									  g_param_spec_boxed ("channel-type-entry", NULL,
														  NULL, GIRC_TYPE_CHANNEL_PREFIX,
														  G_PARAM_READWRITE),
									  G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_CHANNEL_MODES,
									 g_param_spec_value_array ("channel-modes",
															   _("Channel Modes"),
															   _("A GValueArray of "
																 "GIrcChannelModes the "
																 "server supports, and "
																 "their argument style."),
															   g_param_spec_boxed
															   ("channel-mode-entry",
																NULL, NULL,
																GIRC_TYPE_CHANNEL_MODE,
																G_PARAM_READWRITE),
															   G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_LANGUAGES,
									 g_param_spec_value_array ("languages",
															   _("Supported Languages"),
															   _("A GValueArray of "
																 "strings corresponding "
																 "to the languages the "
																 "server supports via "
																 "the LANGUAGE command."),
															   g_param_spec_string
															   ("language-entry", NULL,
																NULL, NULL,
																G_PARAM_READWRITE),
															   G_PARAM_READABLE));
	/* Supported commands */
	g_object_class_install_property (object_class,
									 PROP_VAR_USERIP_CMD,
									 g_param_spec_boolean
									 ("userip-command",
									  _("User IP Command Support"),
									  _("Whether the IRC server supports the USERIP "
										"command to get the IP address of a user."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_KNOCK_CMD,
									 g_param_spec_boolean
									 ("knock-command",
									  _("Knock Command Support"),
									  _("Whether the IRC server supports the KNOCK "
										"command for selecting character set."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_VAR_CJOIN_CMD,
									 g_param_spec_boolean
									 ("cjoin-command",
									  _("CJoin Command Support"),
									  _("Whether the IRC server supports the CJOIN "
										"command for joining multiple channels."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_CPRIVMSG_CMD,
									 g_param_spec_boolean
									 ("cprivmsg-command",
									  _("CPrivMsg Command Support"),
									  _("Whether the IRC server supports the CPRIVMSG "
										"command for sending PRIVMSGs to multiple "
										"users."), FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_CNOTICE_CMD,
									 g_param_spec_boolean
									 ("cnotice-command",
									  _("CNotice Command Support"),
									  _("Whether the IRC server supports the CNOTICE "
										"command for sending NOTICEs to multiple users."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_WALLCHOPS_CMD,
									 g_param_spec_boolean
									 ("wallchops-command",
									  _("Warn All Channel Ops Command Support"),
									  _("Whether the IRC server supports the WALLCHOPS "
										"command for sending NOTICEs to all the channel "
										"operators in a particular channel."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_LANGUAGE_CMD,
									 g_param_spec_boolean
									 ("language-command",
									  _("Language Command Support"),
									  _("Whether the IRC server supports the LANGUAGE "
										"command."), FALSE, G_PARAM_READABLE));

	/* Features */
	g_object_class_install_property (object_class,
									 PROP_VAR_WHO_IS_WHOX,
									 g_param_spec_boolean
									 ("who-command-is-whox",
									  _("WHO Command is in WHOX Format"),
									  _("Whether the IRC server uses a WHOX-style "
										"command for WHO."), FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_BAN_EXCEPTIONS,
									 g_param_spec_boolean
									 ("ban-exceptions",
									  _("Ban Exceptions Support"),
									  _("Whether the IRC server supports channel ban "
										"exceptions via the 'e' channel mode."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_INVITE_EXCEPTIONS,
									 g_param_spec_boolean
									 ("invite-exceptions",
									  _("Invite Exceptions Support"),
									  _("Whether the IRC server supports channel invite "
										"exceptions via the 'I' channel mode."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_PENALTY,
									 g_param_spec_boolean
									 ("penalty",
									  _("Command Time Penalties"),
									  _("Whether the IRC server enforces a time penalty "
										"on certain commands."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_FORCED_NICKCHANGE,
									 g_param_spec_boolean
									 ("forced-nickchange",
									  _("Forced Nickname Change"),
									  _("Whether the IRC server can change the user's "
										"nickname without the user requesting it."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_SAFE_LIST,
									 g_param_spec_boolean
									 ("safe-list",
									  _("Save LIST Command"),
									  _("Whether the IRC server sends the return "
										"from a LIST command in several stages."),
									  FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class,
									 PROP_VAR_SAFE_LIST,
									 g_param_spec_boolean
									 ("server-side-ignore",
									  _("Server-Side Ignores"),
									  _("Whether the IRC server supports the "
										"(+g) user mode and ACCEPT command to "
										"ignore users from the server."),
									  FALSE, G_PARAM_READABLE));

	/* Signals */
	/* Client */
	signals[CONNECT_COMPLETE] =
		g_signal_new ("connect-complete",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (GIrcClientClass, connect_complete),
					  NULL, NULL, _girc_marshal_VOID__ENUM_STRING_STRING,
					  G_TYPE_NONE, 3, GIRC_TYPE_CLIENT_STATUS,
					  G_TYPE_STRING, G_TYPE_STRING);
	signals[QUIT] =
		g_signal_new ("quit", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, quit),
					  NULL, NULL, g_cclosure_marshal_VOID__BOOLEAN,
					  G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

	signals[CTCP_RECV] =
		g_signal_new ("ctcp-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, ctcp_recv),
					  NULL, NULL, _girc_marshal_VOID__BOXED_ENUM_STRING,
					  G_TYPE_NONE, 3, GIRC_TYPE_USER, GIRC_TYPE_CTCP_MESSAGE_TYPE,
					  G_TYPE_STRING);

	/* Shared Query/Channel */
	signals[TARGET_OPENED] =
		g_signal_new ("target-opened",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, target_opened),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_TARGET);
	signals[TARGET_CLOSED] =
		g_signal_new ("target-closed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_CLEANUP,
					  G_STRUCT_OFFSET (GIrcClientClass, target_closed),
					  NULL, NULL, _girc_marshal_VOID__BOXED_STRING,
					  G_TYPE_NONE, 2, GIRC_TYPE_TARGET, G_TYPE_STRING);
	signals[TARGET_RECV] =
		g_signal_new ("target-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, target_recv),
					  NULL, NULL, _girc_marshal_VOID__BOXED_BOXED_ENUM_STRING,
					  G_TYPE_NONE, 4, GIRC_TYPE_TARGET, G_TYPE_VALUE,
					  GIRC_TYPE_USER_MESSAGE_TYPE, G_TYPE_STRING);

	/* Queries */
	signals[QUERY_CHANGED] =
		g_signal_new ("query-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, query_changed),
					  NULL, NULL, _girc_marshal_VOID__BOXED_ENUM_STRING,
					  G_TYPE_NONE, 3, GIRC_TYPE_QUERY, GIRC_TYPE_QUERY_CHANGED_TYPE,
					  G_TYPE_STRING);

	/* Channels */
	signals[CHANNEL_CHANGED] =
		g_signal_new ("channel-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, channel_changed),
					  NULL, NULL,
					  _girc_marshal_VOID__BOXED_BOXED_ENUM_BOXED_BOOLEAN,
					  G_TYPE_NONE, 5, GIRC_TYPE_CHANNEL, G_TYPE_VALUE,
					  GIRC_TYPE_CHANNEL_INFO_TYPE, G_TYPE_VALUE, G_TYPE_BOOLEAN);
	signals[CHANNEL_USER_CHANGED] =
		g_signal_new ("channel-user-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, channel_changed),
					  NULL, NULL, _girc_marshal_VOID__BOXED_BOXED_ENUM_BOXED_BOXED,
					  G_TYPE_NONE, 5, GIRC_TYPE_CHANNEL, GIRC_TYPE_CHANNEL_USER,
					  GIRC_TYPE_CHANNEL_USER_CHANGED_TYPE, G_TYPE_VALUE, G_TYPE_VALUE);

	/* Server Messages */
	signals[NOTICE_RECV] =
		g_signal_new ("notice-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, notice_recv),
					  NULL, NULL, _girc_marshal_VOID__STRING_STRING_STRING,
					  G_TYPE_NONE, 3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	signals[MODE_RECV] =
		g_signal_new ("mode-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, mode_recv),
					  NULL, NULL, g_cclosure_marshal_VOID__FLAGS,
					  G_TYPE_NONE, 1, GIRC_TYPE_SERVER_MODE_FLAGS);
	signals[WHOIS_RECV] =
		g_signal_new ("whois-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, whois_recv),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_USER);
	signals[LIST_RECV] =
		g_signal_new ("list-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, list_recv),
					  NULL, NULL, g_cclosure_marshal_VOID__POINTER,
					  G_TYPE_NONE, 1, G_TYPE_POINTER);
	signals[NAMES_RECV] =
		g_signal_new ("names-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, names_recv),
					  NULL, NULL, g_cclosure_marshal_VOID__POINTER,
					  G_TYPE_NONE, 1, G_TYPE_POINTER);
	signals[MOTD_RECV] =
		g_signal_new ("motd-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, motd_recv),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_MOTD);
	signals[NICK_CHANGED] =
		g_signal_new ("nick-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, nick_changed),
					  NULL, NULL, g_cclosure_marshal_VOID__STRING,
					  G_TYPE_NONE, 1, G_TYPE_STRING);

	/* Basic Messages */
	signals[MSG_RECV] =
		g_signal_new ("msg-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, msg_recv),
					  NULL, NULL, _girc_marshal_VOID__ENUM_STRING,
					  G_TYPE_NONE, 2, GIRC_TYPE_REPLY_TYPE, G_TYPE_STRING);
	signals[ERROR_RECV] =
		g_signal_new ("error-recv",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GIrcClientClass, error_recv),
					  NULL, NULL, _girc_marshal_VOID__ENUM_STRING,
					  G_TYPE_NONE, 2, GIRC_TYPE_SERVER_ERROR_TYPE, G_TYPE_STRING);
}


static void
girc_client_instance_init (GIrcClient * irc)
{
	irc->_priv = g_new0 (GIrcClientPrivate, 1);

	irc->_priv->vars = NULL;
	irc->_priv->vars = g_new0 (GIrcClientVariables, 1);

	irc->_priv->targets = g_hash_table_new_full (girc_userhost_hash,
												 girc_userhost_equal,
												 NULL,
												 (GDestroyNotify) girc_target_unref);

	irc->_priv->users = g_hash_table_new_full (girc_userhost_hash,
											   girc_userhost_equal,
											   g_free, (GDestroyNotify) girc_user_unref);

	irc->_priv->my_user = girc_user_new ();

	irc->_priv->status = GIRC_CLIENT_CLOSED;
	irc->_priv->anti_timeout_id = -1;
	irc->_priv->notify_id = -1;
	irc->_priv->modes = GIRC_SERVER_MODE_INVISIBLE;
	irc->_priv->converter = (GIConv) - 1;

	irc->_priv->names_reply = g_hash_table_new_full (girc_ascii_strcasehash,
													 girc_ascii_strcaseequal,
													 NULL,
													 (GDestroyNotify)
													 girc_names_item_free);
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

GType
girc_client_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (GIrcClientClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) girc_client_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,				/* class data */
			sizeof (GIrcClient),
			0,					/* number of pre-allocs */
			(GInstanceInitFunc) girc_client_instance_init,
			NULL				/* value table */
		};

		type = g_type_register_static (GTCP_TYPE_CONNECTION, "GIrcClient", &info, 0);
	}

	return type;
}


/**
 * girc_client_new: 
 * @nick: the nickname to use.
 * @realname: the user's real name.
 * @username: the user's username.
 * @server: the address to connect to.
 * @port: the remote port to connect to.
 * @pass: the password, if any, to use.
 * @outgoing_encoding: the character set the server uses.
 *
 * Creates a new #GIrcClient ready to connect to server:port. The
 * @outgoing_encoding argument is used to translate between outgoing UTF-8
 * strings and the charset the server is known to expect. If the
 * server's expected encoding is unknown, pass NULL and the user's
 * environment's settings will be used.
 *
 * Returns: a new #GIrcClient-struct
 *
 * Since: 1.0
 **/
GObject *
girc_client_new (const gchar * nick,
				 const gchar * realname,
				 const gchar * username,
				 const gchar * server,
				 guint port,
				 const gchar * passwd,
				 const gchar * outgoing_encoding)
{
	gchar *pass;
	GObject *irc;

	g_return_val_if_fail (nick != NULL, NULL);
	g_return_val_if_fail (nick[0] != '\0', NULL);
	g_return_val_if_fail (realname != NULL, NULL);
	g_return_val_if_fail (realname[0] != '\0', NULL);
	g_return_val_if_fail (username != NULL, NULL);
	g_return_val_if_fail (username[0] != '\0', NULL);
	g_return_val_if_fail (server != NULL, NULL);
	g_return_val_if_fail (server[0] != '\0', NULL);
	g_return_val_if_fail (port < 65535, NULL);

	/* We need a unique ID if we haven't been given a password */
	if (passwd != NULL && passwd[0] != '\0')
		pass = g_strdup (passwd);
	else
		pass = g_strdup_printf ("GC_%0ld", lrand48 ());

	irc = g_object_new (GIRC_TYPE_CLIENT,
						"address", server,
						"port", port,
						"pass", pass,
						"encoding", outgoing_encoding,
						"preferred-nick", nick,
						"realname", realname, "username", username, NULL);

	g_free (pass);

	return irc;
}


/**
 * girc_client_open: 
 * @irc: the irc client to open.
 *
 * Starts the connection process for an previously created #GIrcClient object.
 *
 * Since: 1.0
 **/
void
girc_client_open (GIrcClient * irc)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (irc->_priv->status < GIRC_CLIENT_CONNECTED);

	irc->_priv->status = GIRC_CLIENT_CONNECTING;
	gtcp_connection_open (GTCP_CONNECTION (irc));
}


/**
 * girc_client_is_open: 
 * @irc: the irc client to check.
 *
 * Checks if a #GIrcClient-struct is open.
 *
 * Returns: %TRUE if the IRC client is connected and logged in, %FALSE if it is
 * not.
 *
 * Since: 1.0
 **/
gboolean
girc_client_is_open (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, FALSE);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), FALSE);

	return (irc->_priv->status == GIRC_CLIENT_CONNECTED);
}


/**
 * girc_client_quit: 
 * @irc: the irc client to close.
 * @reason: the quit message to display to other users.
 *
 * Properly closes an open IRC client connection. @reason may be %NULL to
 * omit any quit message.
 *
 * Since: 1.0
 **/
void
girc_client_quit (GIrcClient * irc,
				  const gchar * reason)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));

	if (irc->_priv->status == GTCP_CONNECTION_CONNECTED)
	{
		gchar *msg;

		if (reason != NULL && reason[0] != '\0')
		{
			msg = g_strdup_printf ("%s %s", GIRC_COMMAND_QUIT, reason);
		}
		else
		{
			msg = g_strdup (GIRC_COMMAND_QUIT);
		}

		girc_client_send_raw (irc, msg);
		g_free (msg);
	}
	else
	{
		gtcp_connection_close (GTCP_CONNECTION (irc));
	}
}


/**
 * girc_client_send_raw: 
 * @irc: the irc client to use.
 * @str: the raw IRC message to send.
 *
 * Sends a raw IRC command or message to the server. IRC line terminators
 * will be added, and @str will be converted to the proper encoding.
 *
 * See also: girc_client_set_encoding().
 *
 * Since: 1.0
 **/
void
girc_client_send_raw (GIrcClient * irc,
					  const gchar * str)
{
	gchar *text,
	 *data;

	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (str != NULL && str[0] != '\0');

	if (g_ascii_strncasecmp (str, GIRC_COMMAND_QUIT, 4) == 0)
	{
		irc->_priv->status = GIRC_CLIENT_CLOSING;
	}

	text = g_convert_with_fallback (str, -1, irc->_priv->encoding,
									"UTF-8", _("?"), NULL, NULL, NULL);
	if (text != NULL)
	{
		data = g_strconcat (text, EOL_STR, NULL);
		g_free (text);
	}
	else
	{
		data = g_strconcat (str, EOL_STR, NULL);
	}

	gtcp_connection_send (GTCP_CONNECTION (irc), data, -1);

	g_free (data);
}


/**
 * girc_client_set_nick: 
 * @irc: the irc client to use.
 * @nick: the new nickname.
 *
 * Attempts to set the current user's nickname to @nick. Whether or not the 
 * nickname was actually changed may be determined depending on whether an
 * "#error-recv" or "#nick-changed" signal is emitted later.
 *
 * Returns: %TRUE if @irc is connected and @nick is valid, %FALSE if it @irc
 * is not connected or @nick is invalid.
 *
 * Since: 1.0
 **/
gboolean
girc_client_set_nick (GIrcClient * irc,
					  const gchar * nick)
{
	gchar *text;

	g_return_val_if_fail (irc != NULL, FALSE);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), FALSE);
	g_return_val_if_fail (nick != NULL, FALSE);
	g_return_val_if_fail (nick[0] != '\0', FALSE);

	if (irc->_priv->status < GIRC_CLIENT_CONNECTED)
		return FALSE;

	if (!girc_client_is_valid_nick (irc, nick))
		return FALSE;

	text = g_strconcat (GIRC_COMMAND_PASS, " ", irc->_priv->pass, EOL_STR,
						GIRC_COMMAND_NICK, " ", nick, EOL_STR, GIRC_COMMAND_NICK, NULL);
	girc_client_send_raw (irc, text);
	g_free (text);

	return TRUE;
}


/**
 * girc_client_get_nick: 
 * @irc: the irc client to use.
 *
 * Retrieves the currently used nickname. The returned string should not be freed.
 *
 * Returns: the currently used nickname.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
girc_client_get_nick (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);

	return irc->_priv->my_user->nick;
}


/**
 * girc_client_set_encoding: 
 * @irc: the irc client to use.
 * @encoding: the encoding to use.
 *
 * Sets the outgoing encoding. Messages sent with girc_client_send and
 * girc_client_send_raw() will be converted to this encoding from UTF-8. If
 * @encoding is %NULL, UTF-8 will be used.
 *
 * Since: 1.0
 **/
void
girc_client_set_encoding (GIrcClient * irc,
						  const gchar * encoding)
{
	GIConv converter;
	const gchar *real_encoding;

	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));

	if (encoding != NULL && encoding[0] != '\0')
		real_encoding = encoding;
	else
		real_encoding = "UTF-8";

	converter = g_iconv_open (real_encoding, "UTF-8");

	if (converter != (GIConv) - 1)
	{
		if (irc->_priv->converter != (GIConv) - 1)
			g_iconv_close (irc->_priv->converter);

		if (irc->_priv->encoding != NULL)
			g_free (irc->_priv->encoding);

		irc->_priv->converter = converter;
		irc->_priv->encoding = g_strdup (real_encoding);

		g_object_notify (G_OBJECT (irc), "encoding");
	}
}


/**
 * girc_client_get_encoding: 
 * @irc: the irc client to use.
 *
 * Retrieves the outgoing encoding. The returned string should not be freed.
 *
 * Returns: the currently used outgoing encoding.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
girc_client_get_encoding (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);

	return irc->_priv->encoding;
}


/**
 * girc_client_get_motd: 
 * @irc: the irc client to use.
 *
 * Retrieves the server's message of the day, if it is connected. The returned
 * string should not be modified or freed.
 *
 * Returns: the server's message of the day.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
girc_client_get_motd (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);
	g_return_val_if_fail (girc_client_is_open (irc), NULL);

	return irc->_priv->vars->motd;
}


/**
 * girc_client_get_network_name: 
 * @irc: The irc client to use
 *
 * Retrieves the name of the network, if @irc is connected and the server
 * has provided it. The returned string should not be modified or freed.
 *
 * Returns: the server's network.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
girc_client_get_network_name (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);
	g_return_val_if_fail (girc_client_is_open (irc), NULL);

	return irc->_priv->vars->network_name;
}


/**
 * girc_client_get_away: 
 * @irc: The irc client to use
 *
 * Retrieves the user's away status.
 *
 * Returns: the user's away status.
 *
 * Since: 1.0
 **/
gboolean
girc_client_get_away (GIrcClient * irc)
{
	g_return_val_if_fail (irc != NULL, FALSE);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), FALSE);
	g_return_val_if_fail (girc_client_is_open (irc), FALSE);

	return irc->_priv->away;
}


/**
 * girc_client_set_away: 
 * @irc: the irc client to use.
 * @away: the away status.
 *
 * Sets the user's away status.
 *
 * Since: 1.0
 **/
void
girc_client_set_away (GIrcClient * irc,
					  gboolean away)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (girc_client_is_open (irc));

	if (away == irc->_priv->away)
		return;

	girc_client_send_raw (irc, (const gchar *) GIRC_COMMAND_AWAY);

	irc->_priv->away = away;
}


/**
 * girc_client_open_query: 
 * @irc: the irc client to use.
 * @nick: the nickname to open a query with.
 *
 * Opens a query connection with @nick. If the request is successful, the
 * #GIrcClient--new-query signal will be emitted.
 * 
 * Returns: the new query.
 *
 * Since: 1.0
 **/
G_CONST_RETURN GIrcTarget *
girc_client_open_query (GIrcClient * irc,
						const gchar * nick)
{
	GIrcTarget *retval;
	gchar *key;

	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);
	g_return_val_if_fail (girc_client_is_open (irc), NULL);
	g_return_val_if_fail (nick != NULL && nick[0] != '\0', NULL);

	key = g_strdup_printf ("%s!*@*", nick);

	retval = g_hash_table_lookup (irc->_priv->targets, key);

	if (retval == NULL)
	{
		GIrcUser *user = g_hash_table_lookup (irc->_priv->users, key);

		if (user == NULL)
		{
			user = girc_user_new ();

			user->nick = g_strdup (nick);
			g_hash_table_insert (irc->_priv->users, g_strdup (key), user);
		}

		retval = _girc_client_setup_new_query (irc, user);
	}

	g_free (key);

	return retval;
}


/**
 * girc_client_close_target: 
 * @irc: the irc client to use.
 * @target: the target to close.
 *
 * Properly shuts down the channel or query represented by @target.
 *
 * Since: 1.0
 **/
void
girc_client_close_target (GIrcClient * irc,
						  GIrcTarget * target)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (girc_client_is_open (irc));
	g_return_if_fail (GIRC_IS_CHANNEL (target) || GIRC_IS_QUERY (target));

	if (GIRC_IS_CHANNEL (target))
	{
		gchar *msg;

		msg = g_strdup_printf ("%s %s", GIRC_COMMAND_PART, GIRC_CHANNEL (target)->name);
		girc_client_send_raw (irc, msg);
		g_free (msg);
	}
	else
	{
		g_signal_emit (irc, signals[TARGET_CLOSED], 0, target);
	}
}


/**
 * girc_client_get_target: 
 * @irc: the irc client to use.
 * @handle: the target handle to retrieve.
 *
 * Retrieves the #GIrcTarget which corresponds to @handle, or %NULL if it
 * could not be found.
 *
 * Returns: the #GIrcTarget corresponding to @handle, or %NULL.
 *
 * Since: 1.0
 **/
G_CONST_RETURN GIrcTarget *
girc_client_get_target (GIrcClient * irc,
						const gchar * handle)
{
	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);
	g_return_val_if_fail (girc_client_is_open (irc), NULL);
	g_return_val_if_fail (handle != NULL && handle[0] != '\0', NULL);

	return GIRC_TARGET (g_hash_table_lookup (irc->_priv->targets, handle));
}


/**
 * girc_client_is_valid_nick: 
 * @irc: the irc client to use.
 * @nick: the nickname to test.
 *
 * Tests if a nickname does not contain invalid characters and
 * is not longer than the server's maximum nickname length.
 *
 * Returns: the currently used nickname.
 *
 * Since: 1.0
 **/
gboolean
girc_client_is_valid_nick (GIrcClient * irc,
						   const gchar * nick)
{
	gchar *ptr;
	guint nick_len;

	g_return_val_if_fail (g_utf8_validate (nick, -1, NULL), FALSE);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), FALSE);

	if (nick == NULL || nick[0] == '\0')
		return FALSE;

	for (nick_len = 0, ptr = (gchar *) nick; *ptr != '\0';
		 ptr = g_utf8_next_char (ptr), nick_len++)
	{
		gunichar ch;

		ch = g_utf8_get_char (ptr);

		switch (ch)
		{
		case ' ':
		case '\t':
		case '\n':
		case '\r':
		case '#':
		case '!':
		case '+':
		case '@':
			return FALSE;
		default:
			break;
		}
	}

	if (irc->_priv->vars->max_nick_length != 0
		&& nick_len > irc->_priv->vars->max_nick_length)
		return FALSE;

	return TRUE;
}


/**
 * girc_client_add_user_notify: 
 * @irc: the irc client to use.
 * @nick: the nickname to monitor.
 *
 * Adds a nickname to the list of nicknames to periodically check the
 * status of.
 *
 * Since: 1.0
 **/
void
girc_client_add_user_notify (GIrcClient * irc,
							 const gchar * nick)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (nick != NULL && nick[0] != '\0');

	irc->_priv->notifies = g_slist_append (irc->_priv->notifies, g_strdup (nick));

	if (irc->_priv->notify_id < 0)
		irc->_priv->notify_id = g_timeout_add (3000, user_notify_check, irc);
}


/**
 * girc_client_remove_user_notify: 
 * @irc: the irc client to use.
 * @nick: the nickname to stop monitoring.
 *
 * Removes a nickname from the list of nicknames to periodically check the
 * status of.
 *
 * Since: 1.0
 **/
void
girc_client_remove_user_notify (GIrcClient * irc,
								const gchar * nick)
{
	g_return_if_fail (irc != NULL);
	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (nick != NULL && nick[0] != '\0');

	irc->_priv->notifies = g_slist_remove (irc->_priv->notifies, nick);

	if (g_slist_length (irc->_priv->notifies) == 0 && irc->_priv->notify_id >= 0)
	{
		g_source_remove (irc->_priv->notify_id);
		irc->_priv->notify_id = -1;
	}
}


/**
 * girc_client_get_user_by_nick:
 * @irc: the irc client to use.
 * @nick: the nickname to search for.
 *
 * Retrieves information on a particular nickname. If @nick is %NULL, the
 * user's own #GIrcUser-struct will be returned. The returned #GIrcUser-struct
 * should be unreferenced with girc_user_unref() when it is no longer needed.
 *
 * Returns: a #GIrcUser-struct corresponding to @nick.
 *
 * Since: 1.0
 **/
GIrcUser *
girc_client_get_user_by_nick (GIrcClient * irc,
							  const gchar * nick)
{
	GIrcUser *retval;
	gchar *userhost;

	g_return_val_if_fail (irc != NULL, NULL);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), NULL);

	if (nick == NULL)
	{
		retval = girc_user_ref (irc->_priv->my_user);
	}
	else
	{
		userhost = g_strdup_printf ("%s!*@*", nick);
		retval = g_hash_table_lookup (irc->_priv->users, userhost);
		g_free (userhost);

		if (retval != NULL)
			girc_user_ref (retval);
	}

	return retval;
}


/**
 * girc_client_has_channel_mode:
 * @irc: the irc client to use.
 * @mode_char: the mode to search for.
 *
 * Retrieves whether or not a server supports a particular channel mode.
 *
 * Returns: %TRUE if @mode_char is supported, %FALSE if it is not.
 *
 * Since: 1.0
 **/
gboolean
girc_client_has_channel_mode (GIrcClient * irc,
							  gchar mode_char)
{
	GSList *list;

	g_return_val_if_fail (irc != NULL, FALSE);
	g_return_val_if_fail (GIRC_IS_CLIENT (irc), FALSE);

	for (list = irc->_priv->vars->channel_modes; list != NULL; list = list->next)
	{
		GIrcChannelMode *mode;

		if (list->data != NULL)
		{
			mode = list->data;

			if (mode->mode_char == mode_char)
				return TRUE;
		}
	}

	return FALSE;
}


/* **************** *
 *  LIB-PUBLIC API  *
 * **************** */

GSList *
_girc_client_get_users_targets (GIrcClient * irc,
								const gchar * userhost)
{
	GSList *all_targets = NULL,
	 *targets = NULL;

	all_targets = girc_g_hash_table_copy_to_slist (irc->_priv->targets, NULL);

	for (; all_targets != NULL;
		 all_targets = g_slist_remove_link (all_targets, all_targets))
	{
		if (GIRC_IS_CHANNEL (all_targets->data))
		{
			GIrcChannel *channel = GIRC_CHANNEL (all_targets->data);

			if (g_hash_table_lookup (channel->users, userhost) != NULL)
			{
				targets = g_slist_prepend (targets, all_targets->data);
			}
		}
		else if (GIRC_IS_QUERY (all_targets->data)
				 && girc_userhost_equal (GIRC_QUERY (all_targets->data)->userhost,
										 userhost))
		{
			targets = g_slist_prepend (targets, all_targets->data);
		}
	}

	return targets;
}


/* SIGNAL EMITTERS */

/* Client */
void
_girc_client_emit_connect_complete (GIrcClient * irc,
									GIrcClientStatus status)
{
	g_signal_emit (irc, signals[CONNECT_COMPLETE], 0,
				   status, irc->_priv->vars->servername, irc->_priv->my_user->nick);
}

void
_girc_client_emit_quit (GIrcClient * irc,
						gboolean requested)
{
	g_signal_emit (irc, signals[QUIT], 0, requested);
}


void
_girc_client_emit_nick_changed (GIrcClient * irc,
								const gchar * new_nick)
{
	g_signal_emit (irc, signals[NICK_CHANGED], 0, new_nick);
}


void
_girc_client_emit_ctcp_recv (GIrcClient * irc,
							 GIrcUser * user,
							 GIrcCtcpMessageType type,
							 const gchar * data)
{
	g_signal_emit (irc, signals[CTCP_RECV], 0, user, type, data);
}


void
_girc_client_emit_target_closed (GIrcClient * irc,
								 const GIrcTarget * target,
								 const gchar * reason)
{
	g_signal_emit (irc, signals[TARGET_CLOSED], 0, target, reason);
}


void
_girc_client_emit_target_recv (GIrcClient * irc,
							   const GIrcTarget * target,
							   const GValue * sender,
							   const GIrcUserMessageType type,
							   const gchar * msg)
{
	g_signal_emit (irc, signals[TARGET_RECV], 0, target, sender, type, msg);
}


/* Channels */
GIrcChannel *
_girc_client_setup_new_channel (GIrcClient * irc,
								const gchar * name)
{
	GIrcChannel *channel;
	gchar *msg,
	 *userhost;

	channel = girc_channel_new ();
	channel->name = g_strdup (name);

	channel->my_user = girc_channel_user_new ();
	channel->my_user->user = girc_user_ref (irc->_priv->my_user);

	userhost = girc_user_to_string (irc->_priv->my_user);
	g_hash_table_insert (channel->users, userhost, channel->my_user);

	g_hash_table_insert (irc->_priv->targets, channel->name, channel);

	msg = g_strdup_printf ("%s %s", GIRC_COMMAND_WHO, name);
	girc_client_send_raw (irc, msg);
	g_free (msg);

	msg = g_strdup_printf ("%s %s", GIRC_COMMAND_MODE, name);
	girc_client_send_raw (irc, msg);
	g_free (msg);

	g_signal_emit (irc, signals[TARGET_OPENED], 0, channel);

	return channel;
}


void
_girc_client_emit_channel_changed (GIrcClient * irc,
								   const GIrcChannel * channel,
								   const GValue * sender,
								   const GIrcChannelInfoType changed,
								   const GValue * value,
								   const gboolean is_on)
{
	g_signal_emit (irc, signals[CHANNEL_CHANGED], 0,
				   channel, sender, changed, value, is_on);
}


void
_girc_client_emit_channel_user_changed (GIrcClient * irc,
										const GIrcChannel * channel,
										const GIrcChannelUser * user,
										const GIrcChannelUserChangedType changed,
										const GValue * changer,
										const GValue * value)
{
	g_signal_emit (irc, signals[CHANNEL_USER_CHANGED], 0,
				   channel, user, changed, changer, value);
}


/* Queries */
GIrcTarget *
_girc_client_setup_new_query (GIrcClient * irc,
							  GIrcUser * user)
{
	GIrcQuery *query = girc_query_new ();

	query->user = girc_user_ref (user);
	query->userhost = girc_user_to_string (user);

	g_hash_table_insert (irc->_priv->targets, query->userhost, query);

	g_signal_emit (irc, signals[TARGET_OPENED], 0, query);

	return GIRC_TARGET (query);
}



void
_girc_client_emit_query_changed (GIrcClient * irc,
								 const GIrcQuery * query,
								 GIrcQueryChangedType changed,
								 const gchar * msg)
{
	g_signal_emit (irc, signals[QUERY_CHANGED], 0, query, changed, msg);
}


/* Specific Messages */
void
_girc_client_emit_mode_recv (GIrcClient * irc,
							 GIrcServerModeFlags modes)
{
	g_signal_emit (irc, signals[MODE_RECV], 0, modes);
}

void
_girc_client_emit_motd_recv (GIrcClient * irc)
{
	g_signal_emit (irc, signals[MOTD_RECV], 0, irc->_priv->waiting_motd);
}

void
_girc_client_emit_whois_recv (GIrcClient * irc)
{
	g_signal_emit (irc, signals[WHOIS_RECV], 0, irc->_priv->waiting_whois);

	girc_user_unref (irc->_priv->waiting_whois);
	irc->_priv->waiting_whois = NULL;
}

void
_girc_client_emit_list_recv (GIrcClient * irc)
{
	g_signal_emit (irc, signals[LIST_RECV], 0, irc->_priv->chan_list);
	girc_g_slist_deep_free (irc->_priv->chan_list, (GFreeFunc) girc_list_item_free);
}

void
_girc_client_emit_names_recv (GIrcClient * irc)
{
	GSList *list = girc_g_hash_table_copy_to_slist (irc->_priv->names_reply, NULL);

	g_signal_emit (irc, signals[NAMES_RECV], 0, list);

	g_slist_free (list);
	girc_g_hash_table_clear (irc->_priv->names_reply);
}


/* Generic Messages */
void
_girc_client_emit_msg_recv (GIrcClient * irc,
							GIrcReplyType reply,
							const gchar * msg)
{
	g_signal_emit (irc, signals[MSG_RECV], 0, reply, msg);
}

void
_girc_client_emit_error_recv (GIrcClient * irc,
							  GIrcServerErrorType error,
							  const gchar * msg)
{
	g_signal_emit (irc, signals[ERROR_RECV], 0, error, msg);
}

void
_girc_client_emit_notice_recv (GIrcClient * irc,
							   const gchar * sender,
							   const gchar * msg,
							   const gchar * data)
{
	if (irc == NULL || msg == NULL || data == NULL)
		return;

	g_signal_emit (irc, signals[NOTICE_RECV], 0, sender, msg, data);
}
