/*
 * LibGIrcClient: libgircclient/girc-utils.h
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIRC_UTILS_H__
#define __GIRC_UTILS_H__

#include <glib-object.h>


GSList *girc_g_hash_table_copy_to_slist (GHashTable * table,
										 GCacheDupFunc copy_func);
void girc_g_hash_table_clear (GHashTable * table);

void girc_g_slist_deep_free (GSList * list,
							 GFreeFunc free_func);


guint girc_ascii_strcasehash (gconstpointer key);
gboolean girc_ascii_strcaseequal (gconstpointer key1,
								  gconstpointer key2);

gchar *girc_convert_to_utf8 (const gchar * text,
							 const gchar * encoding);

gboolean girc_g_utf8_strcaseequal (const gchar * str1,
								   const gchar * str2);

gint girc_g_utf8_strncasecmp (const gchar * str1,
							  const gchar * str2,
							  gssize len);
gint girc_g_utf8_strcasecmp (const gchar * str1,
							 const gchar * str2);
G_CONST_RETURN gchar *girc_g_utf8_strcasestr (const gchar * haystack,
											  const gchar * needle);

void girc_g_value_set_value_array_from_slist (GValue * value,
											  GSList * list,
											  GType g_type);

#endif /* __GIRC_UTILS_H__ */
