/*
 * LibGIrcClient: libgircclient/girc-types.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/* GBoxed type handling stuff */


#include "girc-types.h"
#include "girc-utils.h"


/* ********************** *
 *  GIrcTarget Functions  *
 * ********************** */

GType
girc_target_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcTarget",
												   (GBoxedCopyFunc) girc_target_ref,
												   (GBoxedFreeFunc) girc_target_unref);
	}

	return boxed_type;
}


/**
 * girc_target_ref:
 * @src: The target to reference.
 *
 * Increases the reference count on @src by one. When the reference count
 * reaches zero, the data will be freed.
 *
 * Returns: a new reference to @src.
 *
 * Since: 1.0
 **/
GIrcTarget *
girc_target_ref (const GIrcTarget * src)
{
	g_return_val_if_fail (GIRC_IS_TARGET (src), NULL);

	if (GIRC_IS_CHANNEL (src))
	{
		return GIRC_TARGET (girc_channel_ref (GIRC_CHANNEL (src)));
	}
	else if (GIRC_IS_QUERY (src))
	{
		return GIRC_TARGET (girc_query_ref (GIRC_QUERY (src)));
	}

	return NULL;
}


/**
 * girc_target_unref:
 * @target: The target to unreference
 *
 * Decreases the reference count on @target by one. When the reference count reaches
 * zero, the data will be freed.
 *
 * Since: 1.0
 **/
void
girc_target_unref (GIrcTarget * target)
{
	g_return_if_fail (GIRC_IS_TARGET (target));

	if (GIRC_IS_CHANNEL (target))
	{
		girc_channel_unref (GIRC_CHANNEL (target));
	}
	else if (GIRC_IS_QUERY (target))
	{
		girc_query_unref (GIRC_QUERY (target));
	}
}


/* ******************** *
 *  GIrcMotd Functions  *
 * ******************** */

GType
girc_motd_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcMotd",
												   (GBoxedCopyFunc) girc_motd_dup,
												   (GBoxedFreeFunc) girc_motd_free);
	}

	return boxed_type;
}


/**
 * girc_motd_new: 
 *
 * Creates a new #GIrcMotd-struct. The returned data should be freed with
 * girc_motd_free() when no longer needed.
 *
 * Returns: a new #GIrcMotd-struct.
 *
 * Since: 1.0
 **/
GIrcMotd *
girc_motd_new (void)
{
	GIrcMotd *motd = NULL;

	motd = g_new0 (GIrcMotd, 1);

	motd->type = GIRC_TYPE_MOTD;

	return motd;
}


/**
 * girc_motd_dup:
 * @src: The #GIrcMotd-struct to copy.
 *
 * Copies an existing #GIrcMotd-struct. The returned data should be freed
 * with girc_motd_free() when no longer needed.
 *
 * Returns: A copy of @src.
 *
 * Since: 1.0
 **/
GIrcMotd *
girc_motd_dup (const GIrcMotd * src)
{
	GIrcMotd *dest;

	g_return_val_if_fail (GIRC_IS_MOTD (src), NULL);

	dest = girc_motd_new ();

	dest->server = g_strdup (src->server);
	dest->motd = g_strdup (src->motd);

	return dest;
}


/**
 * girc_motd_free:
 * @motd: The #GIrcMotd-struct to free.
 *
 * Frees the memory used by an existing #GIrcMotd-struct.
 *
 * Since: 1.0
 **/
void
girc_motd_free (GIrcMotd * motd)
{
	g_return_if_fail (GIRC_IS_MOTD (motd));

	g_free (motd->motd);
	g_free (motd->server);
	g_free (motd);
}


/* ************************* *
 *  GIrcNamesItem Functions  *
 * ************************* */

GType
girc_names_item_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcNamesItem",
												   (GBoxedCopyFunc) girc_names_item_dup,
												   (GBoxedFreeFunc) girc_names_item_free);
	}

	return boxed_type;
}


/**
 * girc_names_item_new: 
 *
 * Creates a new empty #GIrcNamesItem-struct. The returned data should be freed
 * with girc_names_item_free() when no longer needed.
 *
 * Returns: A new #GIrcNamesItem-struct.
 *
 * Since: 1.0
 **/
GIrcNamesItem *
girc_names_item_new (void) 
{
	GIrcNamesItem *retval;

	retval = g_new0 (GIrcNamesItem, 1);

	retval->type = GIRC_TYPE_NAMES_ITEM;
	retval->channel = NULL;
	retval->users = NULL;

	return retval;
}


/**
 * girc_names_item_dup:
 * @src: The #GIrcNamesItem-struct to copy
 *
 * Copies an existing #GIrcNamesItem-struct. The returned data should be freed
 * with girc_names_item_free() when no longer needed.
 *
 * Returns: A copy of @src.
 *
 * Since: 1.0
 **/
GIrcNamesItem *
girc_names_item_dup (const GIrcNamesItem * src)
{
	GIrcNamesItem *dest;
	GSList *list;

	g_return_val_if_fail (GIRC_IS_NAMES_ITEM (src), NULL);

	dest = girc_names_item_new ();

	dest->channel = g_strdup (src->channel);

	for (list = src->users; list != NULL; list = list->next)
		dest->users = g_slist_append (dest->users, g_strdup (list->data));

	return dest;
}


/**
 * girc_names_item_free:
 * @item: The #GIrcNamesItem-struct to free
 *
 * Frees the memory used by an existing #GIrcNamesItem-struct.
 *
 * Since: 1.0
 **/
void
girc_names_item_free (GIrcNamesItem * item)
{
	g_return_if_fail (GIRC_IS_NAMES_ITEM (item));

	girc_g_slist_deep_free (item->users, g_free);
	g_free (item->channel);
	g_free (item);
}


/* ************************* *
 *  GIrcListItem Functions  *
 * ************************* */

GType
girc_list_item_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcListItem",
												   (GBoxedCopyFunc) girc_list_item_dup,
												   (GBoxedFreeFunc) girc_list_item_free);
	}

	return boxed_type;
}


/**
 * girc_list_item_new: 
 *
 * Creates a new #GIrcListItem-struct. The returned data should be freed with
 * girc_list_item_free() when no longer needed.
 *
 * Returns: a new #GIrcListItem-struct.
 *
 * Since: 1.0
 **/
GIrcListItem *
girc_list_item_new (void) 
{
	GIrcListItem *retval = NULL;

	retval = g_new0 (GIrcListItem, 1);

	retval->type = GIRC_TYPE_LIST_ITEM;
	retval->channel = NULL;
	retval->topic = NULL;

	return retval;
}


/**
 * girc_list_item_dup:
 * @src: The #GIrcListItem-struct to copy
 *
 * Copies an existing #GIrcListItem-struct. The returned data should be freed
 * with girc_list_item_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GIrcListItem *
girc_list_item_dup (const GIrcListItem * src)
{
	GIrcListItem *dest;

	g_return_val_if_fail (GIRC_IS_LIST_ITEM (src), NULL);

	dest = girc_list_item_new ();

	dest->channel = g_strdup (src->channel);
	dest->topic = g_strdup (src->topic);
	dest->users = src->users;

	return dest;
}


/**
 * girc_list_item_free:
 * @item: The LIST command item to free
 *
 * Frees the memory used by an existing #GIrcListItem-struct.
 *
 * Since: 1.0
 **/
void
girc_list_item_free (GIrcListItem * item)
{
	g_return_if_fail (GIRC_IS_LIST_ITEM (item));

	g_free (item->channel);
	g_free (item->topic);
	g_free (item);
}


GQuark
girc_error_quark (void)
{
	static GQuark error_quark = 0;

	if (error_quark == 0)
		error_quark = g_quark_from_static_string ("girc-error-quark");

	return error_quark;
}
