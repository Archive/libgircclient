/*
 * LibGIrcClient: libgircclient/girc-types.h
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIRC_TYPES_H__
#define __GIRC_TYPES_H__


#include <glib.h>
#include <glib-object.h>
#include <libgtcpsocket/gtcp-socket-types.h>

#include "girc-type-utils.h"
#include "girc-channel.h"
#include "girc-query.h"


/* ********************** *
 *  GOBJECT-STYLE MACROS  *
 * ********************** */

#define GIRC_TYPE_NAMES_ITEM			(girc_names_item_get_type ())
#define GIRC_NAMES_ITEM(boxed)			(GIRC_TYPE_CHECK_BOXED_CAST (boxed, GIRC_TYPE_NAMES_ITEM, GIrcNamesItem))
#define GIRC_IS_NAMES_ITEM(boxed)		(GIRC_TYPE_CHECK_BOXED_TYPE (boxed, GIRC_TYPE_NAMES_ITEM))

#define GIRC_TYPE_LIST_ITEM				(girc_list_item_get_type ())
#define GIRC_LIST_ITEM(boxed)			(GIRC_TYPE_CHECK_BOXED_CAST (boxed, GIRC_TYPE_LIST_ITEM, GIrcListItem))
#define GIRC_IS_LIST_ITEM(boxed)		(GIRC_TYPE_CHECK_BOXED_TYPE (boxed, GIRC_TYPE_LIST_ITEM))

#define GIRC_TYPE_MOTD					(girc_motd_get_type ())
#define GIRC_MOTD(boxed)				(GIRC_TYPE_CHECK_BOXED_CAST (boxed, GIRC_TYPE_MOTD, GIrcMotd))
#define GIRC_IS_MOTD(boxed)				(GIRC_TYPE_CHECK_BOXED_TYPE (boxed, GIRC_TYPE_MOTD))

#define GIRC_TYPE_TARGET				(girc_target_get_type ())
#define GIRC_IS_TARGET(boxed)			(GIRC_TYPE_CHECK_BOXED_2UNION_TYPE (boxed, GIRC_TYPE_CHANNEL, GIRC_TYPE_QUERY))
#define GIRC_TARGET(boxed)				(GIRC_TYPE_CHECK_BOXED_2UNION_CAST (boxed, GIRC_TYPE_TARGET, GIRC_TYPE_CHANNEL, GIRC_TYPE_QUERY, GIrcTarget))

#define GIRC_ERROR (girc_error_quark ())


/* ****************** *
 *  ENUMERATED TYPES  *
 * ****************** */


/* *INDENT-OFF* */
typedef enum /* < prefix=GIRC_CLIENT > */
{
	GIRC_CLIENT_ERROR_INVALID_NICK              = -1,
	GIRC_CLIENT_ERROR_INVALID_USER              = -2,
	GIRC_CLIENT_ERROR_CONNECTION_REFUSED        = GTCP_CONNECTION_ERROR_CONNECTION_REFUSED,
	GIRC_CLIENT_ERROR_TIMEOUT                   = GTCP_CONNECTION_ERROR_TIMEOUT,
	GIRC_CLIENT_ERROR_NETWORK_UNREACHABLE       = GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE,
	GIRC_CLIENT_ERROR_BAD_BROADCAST_OR_FIREWALL = GTCP_CONNECTION_ERROR_BAD_BROADCAST_OR_FIREWALL,
	GIRC_CLIENT_ERROR_INTERNAL                  = GTCP_CONNECTION_ERROR_INTERNAL,
	GIRC_CLIENT_ERROR_THREAD_ERROR              = GTCP_CONNECTION_ERROR_THREAD_ERROR,
	GIRC_CLIENT_ERROR_PROXY_ERROR               = GTCP_CONNECTION_ERROR_PROXY_ERROR,
	GIRC_CLIENT_CLOSING                         = GTCP_CONNECTION_CLOSING,
	GIRC_CLIENT_CLOSED                          = GTCP_CONNECTION_CLOSED,

	GIRC_CLIENT_CONNECTED                       = GTCP_CONNECTION_CONNECTED,
	GIRC_CLIENT_CONNECTING                      = GTCP_CONNECTION_CONNECTING,
	GIRC_CLIENT_LOGGING_IN
}
GIrcClientStatus;


typedef enum /* < flags,prefix=GIRC_SERVER_NOTICE > */
{
	GIRC_SERVER_NOTICE_NONE            = 0,
	GIRC_SERVER_NOTICE_OLD_MSGS        = 1 << 0,
	GIRC_SERVER_NOTICE_NICK_COLLISION  = 1 << 1,
	GIRC_SERVER_NOTICE_KLINE           = 1 << 2,
	GIRC_SERVER_NOTICE_DESYNC          = 1 << 3,
	GIRC_SERVER_NOTICE_TEMP_DESYNC     = 1 << 4,
	GIRC_SERVER_NOTICE_UNAUTH          = 1 << 5,
	GIRC_SERVER_NOTICE_TCP_ERRORS      = 1 << 5,
	GIRC_SERVER_NOTICE_TOO_MANY_CONNS  = 1 << 6,
	GIRC_SERVER_NOTICE_UWORLD_ACTIONS  = 1 << 7,
	GIRC_SERVER_NOTICE_NETWORK_KLINE   = 1 << 8,
	GIRC_SERVER_NOTICE_NETSPLIT_REJOIN = 1 << 9,
	GIRC_SERVER_NOTICE_IP_MISMATCH     = 1 << 10,
	GIRC_SERVER_NOTICE_THROTTLES       = 1 << 11,
	GIRC_SERVER_NOTICE_OLD_OPERONLY    = 1 << 12,
	GIRC_SERVER_NOTICE_CLIENT_CONNEXIT = 1 << 13
}
GIrcServerNoticeFlags;


typedef enum /* < flags,prefix=GIRC_SERVER_MODE > */
{
	GIRC_SERVER_MODE_NONE             = 0,
	GIRC_SERVER_MODE_AWAY             = 1 << 0,
	GIRC_SERVER_MODE_INVISIBLE        = 1 << 1,
	GIRC_SERVER_MODE_GET_WALLOPS      = 1 << 2,
	GIRC_SERVER_MODE_RESTRICTED       = 1 << 3,
	GIRC_SERVER_MODE_OPER             = 1 << 4,
	GIRC_SERVER_MODE_LOCAL_OPER       = 1 << 5,
	GIRC_SERVER_MODE_GET_SERV_NOTICES = 1 << 6
}
GIrcServerModeFlags;


typedef enum /*< prefix=GIRC_SERVER_ERROR > */
{
	/* Custom Errors */
	GIRC_SERVER_ERROR_BAD_STARTING_NICKS        = -1,
	GIRC_SERVER_ERROR_BAD_USER_CMD              = -2,

	/* Error Replies from the server */
	GIRC_SERVER_ERROR_LOAD_TOO_HEAVY            = 263,

	GIRC_SERVER_ERROR_NO_SUCH_NICK              = 401,
	GIRC_SERVER_ERROR_NO_SUCH_SERVER            = 402,
	GIRC_SERVER_ERROR_NO_SUCH_CHANNEL           = 403,
	GIRC_SERVER_ERROR_CANNOT_SEND_TO_CHAN       = 404,
	GIRC_SERVER_ERROR_JOINED_TOO_MANY_CHANNELS  = 405,
	GIRC_SERVER_ERROR_WAS_NO_SUCH_NICK          = 406,
	GIRC_SERVER_ERROR_TOO_MANY_TARGETS          = 407,
	
	GIRC_SERVER_ERROR_NO_ORIGIN                 = 409,
	
	GIRC_SERVER_ERROR_NO_RECIPIENT              = 411,
	GIRC_SERVER_ERROR_NO_TEXT_TO_SEND           = 412,
	GIRC_SERVER_ERROR_NO_TOP_LEVEL              = 413,
	GIRC_SERVER_ERROR_WILD_TOP_LEVEL            = 414,

	GIRC_SERVER_ERROR_UNKNOWN_COMMAND           = 421,
	GIRC_SERVER_ERROR_NO_MOTD                   = 422,
	GIRC_SERVER_ERROR_NO_ADMIN_INFO             = 423,
	GIRC_SERVER_ERROR_FILE_ERROR                = 424,
	
	GIRC_SERVER_ERROR_NO_NICKNAME_GIVEN         = 431,
	GIRC_SERVER_ERROR_NICK_HAS_INVALID_CHARS    = 432,
	GIRC_SERVER_ERROR_NICK_IN_USE               = 433,
	
	GIRC_SERVER_ERROR_NICK_COLLISION            = 436,
	
	GIRC_SERVER_ERROR_USER_NOT_IN_CHANNEL       = 441,
	GIRC_SERVER_ERROR_YOURE_NOT_IN_CHANNEL      = 442,
	GIRC_SERVER_ERROR_USER_ALREADY_IN_CHANNEL   = 443,
	GIRC_SERVER_ERROR_NOT_LOGGED_IN             = 444,
	GIRC_SERVER_ERROR_SUMMON_CMD_DISABLED       = 445,
	GIRC_SERVER_ERROR_USERS_CMD_DISABLED        = 446,
	
	GIRC_SERVER_ERROR_NOT_REGISTERED            = 451,
	
	GIRC_SERVER_ERROR_NOT_ENOUGH_PARAMS         = 461,
	GIRC_SERVER_ERROR_ALREADY_REGISTERED        = 462,
	GIRC_SERVER_ERROR_COULD_NOT_REGISTER        = 463,
	GIRC_SERVER_ERROR_PASSWD_MISMATCH           = 464,
	GIRC_SERVER_ERROR_BANNED_FROM_SERVER        = 465,
	
	GIRC_SERVER_ERROR_CHAN_KEY_ALREADY_SET      = 467,
	
	GIRC_SERVER_ERROR_CHAN_IS_FULL              = 471,
	GIRC_SERVER_ERROR_UNKNOWN_MODE_CHAR         = 472,
	GIRC_SERVER_ERROR_CHAN_IS_INVITE_ONLY       = 473,
	GIRC_SERVER_ERROR_BANNED_FROM_CHAN          = 474,
	GIRC_SERVER_ERROR_BAD_CHAN_KEY              = 475,
	
	GIRC_SERVER_ERROR_NOT_OPER                  = 481,
	GIRC_SERVER_ERROR_NOT_CHAN_OP               = 482,
	GIRC_SERVER_ERROR_CANNOT_KILL_SERVER        = 483,
	
	GIRC_SERVER_ERROR_NO_OPER_FROM_HOST         = 491,
	
	GIRC_SERVER_ERROR_UNKNOWN_MODE_FLAG         = 501,
	GIRC_SERVER_ERROR_CANNOT_CHANGE_OTHERS_MODE = 502
}
GIrcServerErrorType;


typedef enum /*< prefix=GIRC_REPLY > */
{
	/* Initial welcoming replies */
	GIRC_REPLY_WELCOME            = 1,
	GIRC_REPLY_YOURHOST           = 2,
	GIRC_REPLY_CREATED            = 3,
	GIRC_REPLY_VERSION            = 4,
	GIRC_REPLY_MYINFO             = 5,

	/* Command Replies -- these prefix server replies to valid commands the
	   user sends. */
	GIRC_REPLY_OTHER_USER_IS_AWAY = 301,
	GIRC_REPLY_USERHOST           = 302,
	GIRC_REPLY_IS_ON              = 303,
	GIRC_REPLY_BACK               = 305,
	GIRC_REPLY_AWAY               = 306,

	GIRC_REPLY_WHOIS_USER         = 311,
	GIRC_REPLY_WHOIS_SERVER       = 312,
	GIRC_REPLY_WHOIS_IS_OPER      = 313,
	GIRC_REPLY_WHOIS_IS_IDLE      = 317,
	GIRC_REPLY_WHOIS_END          = 318,
	GIRC_REPLY_WHOIS_CHANNELS_ON  = 319,

	GIRC_REPLY_WHOWAS_USER        = 314,
	GIRC_REPLY_WHOWAS_END         = 369,

	GIRC_REPLY_LIST_START         = 321,
	GIRC_REPLY_LIST_BODY          = 322,
	GIRC_REPLY_LIST_END           = 323,

	GIRC_REPLY_CHAN_MODES         = 324,
	GIRC_REPLY_CHAN_INFO          = 329,
	GIRC_REPLY_CHAN_INFO2         = 333,

	GIRC_REPLY_NO_TOPIC_IS_SET    = 331,
	GIRC_REPLY_TOPIC              = 332,

	GIRC_REPLY_INVITE_SENT        = 341,

	GIRC_REPLY_SUMMON_SENT        = 342,

	GIRC_REPLY_SERVER_VERSION     = 351,

	GIRC_REPLY_WHO                = 352,
	GIRC_REPLY_WHO_END            = 315,

	GIRC_REPLY_NAME               = 353,
	GIRC_REPLY_NAMES_END          = 366,

	GIRC_REPLY_LINKS              = 364,
	GIRC_REPLY_LINKS_END          = 365,

	GIRC_REPLY_BANLIST            = 367,
	GIRC_REPLY_BANLIST_END        = 368,

	GIRC_REPLY_INVITELIST         = 346,
	GIRC_REPLY_INVITELIST_END     = 347,

	GIRC_REPLY_EXCEPTIONLIST      = 348,
	GIRC_REPLY_EXCEPTIONLIST_END  = 349,

	GIRC_REPLY_INFO               = 371,
	GIRC_REPLY_INFO_END           = 374,

	GIRC_REPLY_MOTD_START         = 375,
	GIRC_REPLY_MOTD_BODY          = 372,
	GIRC_REPLY_MOTD_END           = 376,

	GIRC_REPLY_YOU_ARE_NOW_OPER   = 381,
	GIRC_REPLY_REHASHING          = 382,
	GIRC_REPLY_TIME               = 391,

	GIRC_REPLY_USERS_START        = 392,
	GIRC_REPLY_USERS_BODY         = 393,
	GIRC_REPLY_USERS_END          = 394,
	GIRC_REPLY_USERS_NONE         = 395,

	GIRC_REPLY_TRACE_LINK         = 200,
	GIRC_REPLY_TRACE_CONNECTING   = 201,
	GIRC_REPLY_TRACE_HANDSHAKE    = 202,
	GIRC_REPLY_TRACE_UNKNOWN      = 203,
	GIRC_REPLY_TRACE_OPER         = 204,
	GIRC_REPLY_TRACE_USER         = 205,
	GIRC_REPLY_TRACE_SERVER       = 206,
	GIRC_REPLY_TRACE_NEW_TYPE     = 208,
	GIRC_REPLY_TRACE_LOG          = 261,

	GIRC_REPLY_STATS_LINKINFO     = 211,
	GIRC_REPLY_STATS_COMMANDS     = 212,
	GIRC_REPLY_STATS_C_LINE       = 213,
	GIRC_REPLY_STATS_N_LINE       = 214,
	GIRC_REPLY_STATS_I_LINE       = 215,
	GIRC_REPLY_STATS_K_LINE       = 216,
	GIRC_REPLY_STATS_Y_LINE       = 218,
	GIRC_REPLY_STATS_L_LINE       = 241,
	GIRC_REPLY_STATS_UPTIME       = 242,
	GIRC_REPLY_STATS_O_LINE       = 243,
	GIRC_REPLY_STATS_H_LINE       = 244,
	GIRC_REPLY_STATS_END          = 219,

	GIRC_REPLY_UMODEIS            = 221,

	GIRC_REPLY_LUSERS_CLIENT      = 251,
	GIRC_REPLY_LUSERS_OPER        = 252,
	GIRC_REPLY_LUSERS_UNKNOWN     = 253,
	GIRC_REPLY_LUSERS_CHANNELS    = 254,
	GIRC_REPLY_LUSERS_SERVER_INFO = 255,

	GIRC_REPLY_SERVER_ADMIN_INFO  = 256,
	GIRC_REPLY_SERVER_ADMIN_LOC1  = 257,
	GIRC_REPLY_SERVER_ADMIN_LOC2  = 258,
	GIRC_REPLY_SERVER_ADMIN_EMAIL = 259
}
GIrcReplyType;


typedef enum /* < prefix=GIRC_CLIENT_CHARACTER_MAPPING > */
{
	GIRC_CLIENT_CHARACTER_MAPPING_UNKNOWN,
	GIRC_CLIENT_CHARACTER_MAPPING_ASCII,
	GIRC_CLIENT_CHARACTER_MAPPING_RFC1459
}
GIrcClientCharacterMappingType;


typedef enum /* < prefix=GIRC_USER_MESSAGE > */
{
	GIRC_USER_MESSAGE_PRIVMSG,
	GIRC_USER_MESSAGE_ACTION,
	GIRC_USER_MESSAGE_NOTICE,

	GIRC_USER_MESSAGE_LAST
}
GIrcUserMessageType;


typedef enum /* < prefix=GIRC_CTCP_MESSAGE > */
{
	GIRC_CTCP_MESSAGE_NONE,

	GIRC_CTCP_MESSAGE_DCC_CHAT,
	GIRC_CTCP_MESSAGE_DCC_RECV,
	GIRC_CTCP_MESSAGE_DCC_SEND,

	GIRC_CTCP_MESSAGE_CLIENTINFO,
	GIRC_CTCP_MESSAGE_USERINFO,
	GIRC_CTCP_MESSAGE_FINGER,
	GIRC_CTCP_MESSAGE_PING,
	GIRC_CTCP_MESSAGE_SOURCE,
	GIRC_CTCP_MESSAGE_TIME,
	GIRC_CTCP_MESSAGE_VERSION,

	GIRC_CTCP_MESSAGE_ACTION,

	GIRC_CTCP_MESSAGE_UNKNOWN
}
GIrcCtcpMessageType;
/* *INDENT-ON* */


/* ***************** *
 *  DATA STRUCTURES  *
 * ***************** */

typedef union _GIrcTarget GIrcTarget;

typedef struct _GIrcMotd GIrcMotd;
typedef struct _GIrcNamesItem GIrcNamesItem;
typedef struct _GIrcListItem GIrcListItem;


union _GIrcTarget
{
	GType type;

	GIrcChannel channel;
	GIrcQuery query;
};


struct _GIrcMotd
{
	/* < private > */
	GType type;

	/* < public > */
	gchar *server;
	gchar *motd;
};


struct _GIrcNamesItem
{
	/* < private > */
	GType type;

	/* < public > */
	gchar *channel;
	GSList *users;
};


struct _GIrcListItem
{
	/* < private > */
	GType type;

	/* < public > */
	gchar *channel;
	guint users;
	gchar *topic;
};


/* ************** *
 *  GBoxed TYPES  *
 * ************** */

GType girc_target_get_type (void);
GType girc_motd_get_type (void);
GType girc_names_item_get_type (void);
GType girc_list_item_get_type (void);


/* ******************* *
 *  UTILITY FUNCTIONS  *
 * ******************* */

GIrcMotd *girc_motd_new (void);
GIrcMotd *girc_motd_dup (const GIrcMotd * src);
void girc_motd_free (GIrcMotd * motd);

GIrcNamesItem *girc_names_item_new (void);
GIrcNamesItem *girc_names_item_dup (const GIrcNamesItem * src);
void girc_names_item_free (GIrcNamesItem * item);

GIrcListItem *girc_list_item_new (void);
GIrcListItem *girc_list_item_dup (const GIrcListItem * src);
void girc_list_item_free (GIrcListItem * item);

GIrcTarget *girc_target_ref (const GIrcTarget * src);
void girc_target_unref (GIrcTarget * target);

GQuark girc_error_quark (void);


#endif /* __GIRC_TYPES_H__ */
