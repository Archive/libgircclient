/*
 * LibGIrcClient: libgircclient/parser.h
 *
 * Copyright (c) 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __PARSER_H__
#define __PARSER_H__

#include "girc-client.h"

void _girc_parser_parse_incoming (GIrcClient * irc,
								  gchar * data);

GIrcChannelUser *_girc_parser_parse_channel_user (GIrcClient * irc,
												  const gchar * data);

#endif /* __PARSER_H__ */
