/*
 * LibGIrcClient: libgircclient/girc-utils.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "girc-utils.h"

#include <libgtcpsocket/gtcp-i18n.h>

#include <string.h>



/**
 * girc_ascii_strcasehash: 
 * @key: the ascii string to hash.
 *
 * Creates a case-insensitive hash value from the given ASCII-encoding string.
 *
 * Returns: a hash value for the given key.
 *
 * Since: 1.0
 **/
guint
girc_ascii_strcasehash (gconstpointer key)
{
	const gchar *ptr = key;
	guint retval;

	if (key == NULL)
		return 0;

	retval = (guint) (guchar) g_ascii_tolower (*ptr);

	for (ptr += 1; *ptr != '\0'; ptr++)
	{
		retval = (retval << 5) - retval + g_ascii_tolower (*ptr);
	}

	return retval;
}


/**
 * girc_ascii_strcaseequal: 
 * @key1: the first ascii-encoding string.
 * @key2: the second ascii-encoded string.
 *
 * Tests two ASCII-encoded strings for equality.
 *
 * Returns: %TRUE if both string are equal, %FALSE if they are not.
 *
 * Since: 1.0
 **/
gboolean
girc_ascii_strcaseequal (gconstpointer key1,
						 gconstpointer key2)
{
	if (key1 != key2)
	{
		if (key1 == NULL || key2 == NULL)
		{
			return FALSE;
		}
		else
		{
			return (g_ascii_strcasecmp ((const gchar *) key1, (const gchar *) key2) == 0);
		}
	}
	else
	{
		return TRUE;
	}
}


/**
 * girc_g_slist_deep_free: 
 * @list: the list to free.
 * @free_func: a function capable of freeing the list data.
 *
 * Frees the given list, and the data in @list using @free_func.
 *
 * Since: 1.0
 **/
void
girc_g_slist_deep_free (GSList * list,
						GFreeFunc free_func)
{
	if (free_func == NULL)
	{
		g_slist_free (list);
		return;
	}

	for (; list != NULL; list = g_slist_remove (list, list->data))
	{
		if (list->data != NULL)
			(*free_func) (list->data);
	}
}


typedef struct
{
	GSList *list;
	GCacheDupFunc copy_func;
}
HashCopyData;


static void
listify_and_copy_hash_table (gpointer key,
							 gpointer value,
							 HashCopyData * data)
{
	if (value != NULL)
		data->list = g_slist_prepend (data->list, ((*data->copy_func) (value)));
}


static void
listify_hash_table (gpointer key,
					gpointer value,
					GSList ** list)
{
	*list = g_slist_prepend (*list, value);
}


/**
 * girc_g_hash_table_copy_to_slist:
 * @table: the hash table to read.
 * @copy_func: a function to copy the values in the hash table.
 * 
 * Copies the values in @table and copies the values into a #GSList using
 * @copy_func. If copy_func is %NULL, then the values will simply be prepended
 * to a list. The returned list should be freed with g_slist_free(), and
 * the data items should be freed using the appropriate function, if
 * @copy_func is not %NULL.
 *
 * Returns: a new #GSList containing the values in @table.
 *
 * Since: 1.0.
 **/
GSList *
girc_g_hash_table_copy_to_slist (GHashTable * table,
								 GCacheDupFunc copy_func)
{
	GSList *list;

	g_return_val_if_fail (table != NULL, NULL);

	if (copy_func != NULL)
	{
		HashCopyData data;

		data.list = NULL;
		data.copy_func = copy_func;

		g_hash_table_foreach (table, (GHFunc) listify_and_copy_hash_table, &data);

		list = data.list;
	}
	else
	{
		list = NULL;

		g_hash_table_foreach (table, (GHFunc) listify_hash_table, &list);
	}

	return list;
}


static gboolean
clear_hash_table (gpointer key,
				  gpointer value,
				  gpointer data)
{
	return TRUE;
}


/**
 * girc_g_hash_table_clear:
 * @table: the table to clear.
 * 
 * Removes all values in a #GHashTable without destroying the table.
 * 
 * Since: 1.0
 **/
void
girc_g_hash_table_clear (GHashTable * table)
{
	g_hash_table_foreach_remove (table, clear_hash_table, NULL);
}


static G_CONST_RETURN gchar *
get_unknown_char (void)
{
	static gchar *unknown_char = NULL;

	if (unknown_char == NULL)
	{
		unknown_char = g_strdup (_("<?>"));
	}

	return unknown_char;
}


/**
 * girc_convert_to_utf8:
 * @text: the legacy-encoded string.
 * @encoding: the expected encoding of str.
 * 
 * Converts a legacy-encoded (non-UTF8) string to UTF8 format using @encoding.
 * If the string cannot be converted using @encoding, the user's locale
 * encoding will be used. If it still cannot be converted, WINDOWS-1252 (Latin-1
 * Extended) encoding will be tried. If WINDOWS-1252 also fails, an error
 * message will be returned. The returned string should be freed when no longer
 * needed.
 *
 * Returns: the UTF-8 encoded version of @text, an error message, or %NULL, if @text is %NULL or an empty string
 * 
 * Since: 1.0
 **/
gchar *
girc_convert_to_utf8 (const gchar * text,
					  const gchar * encoding)
{
	gchar *utf8 = NULL;
	GError *err = NULL;

	/* If empty string, return NULL */
	if (text == NULL || text[0] == '\0')
		return NULL;

	if (encoding != NULL)
	{
		utf8 = g_convert_with_fallback (text, -1, "UTF-8", encoding,
										(gchar *) get_unknown_char (), NULL, NULL, &err);
	}
	else
	{
		utf8 = g_convert_with_fallback (text, -1, "UTF-8", "UTF-8",
										(gchar *) get_unknown_char (), NULL, NULL, &err);
	}

	/* If it didn't work with the requested encoding, try the user's locale. */
	if (utf8 == NULL)
	{
		utf8 = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);

		/* If it didn't work with the locale, then try CP-1252 */
		if (utf8 == NULL)
		{
			utf8 = g_convert_with_fallback (text, -1, "UTF-8", "WINDOWS-1252",
											(gchar *) get_unknown_char (), NULL, NULL,
											NULL);

			/* If it hasn't worked yet, just use the original encoding error message */
			if (utf8 == NULL)
			{
				utf8 = g_strdup_printf (_("[Incorrect Coding: %s]"), err->message);
			}
		}
	}

	if (err != NULL)
	{
		g_error_free (err);
	}

	return utf8;
}


static gint
utf8_strncasecmp (const gchar * str1,
				  const gchar * str2,
				  gssize len)
{
	gchar *nstr1,
	 *nstr2,
	 *string1,
	 *string2;
	gint retval;

	nstr1 = g_utf8_normalize (str1, G_NORMALIZE_DEFAULT, len);
	string1 = g_utf8_casefold (nstr1, len);
	g_free (nstr1);

	nstr2 = g_utf8_normalize (str2, G_NORMALIZE_DEFAULT, len);
	string2 = g_utf8_casefold (nstr2, len);
	g_free (nstr2);

	retval = strncmp (string1, string2, len);

	g_free (string1);
	g_free (string2);

	return retval;
}


/**
 * girc_g_utf8_strncasecmp:
 * @str1: the string to compare with @str2.
 * @str2: the string to compare with @str1.
 * @len: the number of UTF-8 characters to compare.
 * 
 * Compare the first @len characters of two UTF-8 strings, ignoring the case of
 * the UTF-8 characters. If @len is %-1, the strings will be compared completely.
 * 
 * Return value: an integer less than, equal to, or greater than
 *               zero if @str1 is found, respectively, to be less than,
 *               to match, or to be greater than @str2.
 * 
 * Since: 1.0
 **/
gint
girc_g_utf8_strncasecmp (const gchar * str1,
						 const gchar * str2,
						 gssize len)
{
	g_return_val_if_fail (str1 != NULL, FALSE);
	g_return_val_if_fail (str2 != NULL, FALSE);
	g_return_val_if_fail (g_utf8_validate (str1, len, NULL), FALSE);
	g_return_val_if_fail (g_utf8_validate (str2, len, NULL), FALSE);

	return utf8_strncasecmp (str1, str2, len);
}


/**
 * girc_g_utf8_strcasecmp:
 * @str1: string to compare with @str2.
 * @str2: string to compare with @str1.
 * 
 * Compare two UTF-8 strings, ignoring the case of the UTF-8 characters.
 * 
 * Return value: an integer less than, equal to, or greater than
 *               zero if @str1 is found, respectively, to be less than,
 *               to match, or to be greater than @str2.
 * 
 * Since: 1.0
 **/
gint
girc_g_utf8_strcasecmp (const gchar * str1,
						const gchar * str2)
{
	g_return_val_if_fail (str1 != NULL, FALSE);
	g_return_val_if_fail (str2 != NULL, FALSE);
	g_return_val_if_fail (g_utf8_validate (str1, -1, NULL), FALSE);
	g_return_val_if_fail (g_utf8_validate (str2, -1, NULL), FALSE);

	return utf8_strncasecmp (str1, str2, -1);
}


/**
 * girc_g_utf8_strcaseequal:
 * @str1: the string to compare against @str2.
 * @str2: the string to compare against @str1.
 * 
 * Tests the equality of two UTF-8 encoded strings without regard to upper and
 * lower case.
 *
 * Returns: a %TRUE or %FALSE, depending if @str1 and @str2 are equivalent.
 *
 * Since: 1.0
 **/
gboolean
girc_g_utf8_strcaseequal (const gchar * str1,
						  const gchar * str2)
{
	g_return_val_if_fail (str1 != NULL, FALSE);
	g_return_val_if_fail (str2 != NULL, FALSE);
	g_return_val_if_fail (g_utf8_validate (str1, -1, NULL), FALSE);
	g_return_val_if_fail (g_utf8_validate (str2, -1, NULL), FALSE);

	return (utf8_strncasecmp (str1, str2, -1) == 0);
}


/**
 * girc_g_utf8_strcasestr:
 * @haystack: the string to search in.
 * @needle: the string to search for.
 * 
 * Performs a case-insensitive search for the first occurance of @needle in @haystack.
 * 
 * Returns: the position in @haystack where @needle was found, or %NULL, if
 * @needle was not found.
 * 
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
girc_g_utf8_strcasestr (const gchar * haystack,
						const gchar * needle)
{
	const gchar *haystack_ptr = haystack,
	 *haystack_end;
	gunichar haystack_ch,
	  needle_ch;

	g_return_val_if_fail (haystack != NULL, NULL);
	g_return_val_if_fail (needle != NULL, NULL);

	if (g_utf8_get_char (needle) == '\0')
		return haystack;

	if (!g_utf8_validate (needle, -1, NULL))
	{
		g_warning (_("Invalid UTF-8 sequence in needle."));
		return NULL;
	}

	haystack_end = haystack + strlen (haystack) - strlen (needle);

	haystack_ch = g_utf8_get_char (haystack_ptr);

	while (haystack_ptr <= haystack_end && haystack_ch != '\0')
	{
		const gchar *haystack_ptr2 = haystack_ptr,
		 *needle_ptr = needle;

		for (needle_ch = g_utf8_get_char (needle_ptr),
			 haystack_ch = g_utf8_get_char (haystack_ptr2);
			 needle_ch != '\0';
			 needle_ch = g_utf8_get_char (needle_ptr),
			 haystack_ch = g_utf8_get_char (haystack_ptr2))
		{
			
			if (g_unichar_tolower (needle_ch) != g_unichar_tolower (haystack_ch))
			{
				goto next_haystack_ch;
			}
			
			needle_ptr = g_utf8_next_char (needle_ptr);
			haystack_ptr2 = g_utf8_next_char (haystack_ptr2);
		}

		return haystack_ptr;

	  next_haystack_ch:
		haystack_ptr = g_utf8_next_char (haystack_ptr);
		haystack_ch = g_utf8_get_char (haystack_ptr);
	}

	return NULL;
}


typedef void (*SetValueFunc) (GValue * arg1,
							  gpointer arg2);

static inline GValueArray *
get_value_array_from_slist (GSList * list,
							GType type)
{
	GValueArray *array = NULL;
	GValue tmp_value = { 0 };
	gint length;
	SetValueFunc func;

	/* These are the only fundamental types which GLib can legitimately store
	   in a gpointer, the doubles/floats/longs/etc. cannot legally fit into
	   a gpointer, and thus if they do show up, it's a bug. */
	if (g_type_is_a (type, G_TYPE_STRING))
		func = (SetValueFunc) g_value_set_string;
	else if (g_type_is_a (type, G_TYPE_OBJECT))
		func = (SetValueFunc) g_value_set_object;
	else if (g_type_is_a (type, G_TYPE_BOXED))
		func = (SetValueFunc) g_value_set_boxed;
	else if (g_type_is_a (type, G_TYPE_POINTER))
		func = (SetValueFunc) g_value_set_pointer;
	else if (g_type_is_a (type, G_TYPE_CHAR))
		func = (SetValueFunc) g_value_set_int;
	else if (g_type_is_a (type, G_TYPE_UCHAR))
		func = (SetValueFunc) g_value_set_uint;
	else if (g_type_is_a (type, G_TYPE_BOOLEAN))
		func = (SetValueFunc) g_value_set_boolean;
	else if (g_type_is_a (type, G_TYPE_INT))
		func = (SetValueFunc) g_value_set_int;
	else if (g_type_is_a (type, G_TYPE_UINT))
		func = (SetValueFunc) g_value_set_uint;
	else if (g_type_is_a (type, G_TYPE_ENUM))
		func = (SetValueFunc) g_value_set_enum;
	else if (g_type_is_a (type, G_TYPE_FLAGS))
		func = (SetValueFunc) g_value_set_flags;
	else if (g_type_is_a (type, G_TYPE_ENUM))
		func = (SetValueFunc) g_value_set_enum;
	else
		return g_value_array_new (0);

	length = g_slist_length (list);

	array = g_value_array_new (length);

	for (; list != NULL; list = list->next)
	{
		g_value_init (&tmp_value, type);
		func (&tmp_value, list->data);
		g_value_array_append (array, &tmp_value);

		g_value_unset (&tmp_value);
	}

	return array;
}


/**
 * girc_g_value_set_value_array_from_slist:
 * @value: the structure to store the data in.
 * @list: the list of items.
 * @g_type: the registered type of the items in @list.
 * 
 * Creates a #GValueArray of @g_type items from the data in @list, and places
 * it in the boxed member of @value.
 * 
 * Since: 1.0
 **/
void
girc_g_value_set_value_array_from_slist (GValue * value,
										 GSList * list,
										 GType g_type)
{
	g_return_if_fail (G_IS_VALUE (value));

	/* Disallowed types */
	g_return_if_fail (g_type != G_TYPE_INVALID);
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_FLOAT));
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_DOUBLE));
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_LONG));
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_ULONG));
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_INT64));
	g_return_if_fail (g_type_is_a (g_type, G_TYPE_UINT64));

	if (list == NULL)
		g_value_set_boxed_take_ownership (value, g_value_array_new (0));

	g_value_set_boxed_take_ownership (value, get_value_array_from_slist (list, g_type));
}
