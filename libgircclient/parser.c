/*
 * LibGIrcClient: libgircclient/parser.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "parser.h"

#include "girc-client-private.h"
#include "girc-commands.h"

#include "variable-strings.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgtcpsocket/gtcp-i18n.h>


#define IS_USERHOST(raw_text) \
	(sscanf (raw_text, "%*[^!]!%*[^@]@%*s") < 3)
#define PARSE_USERHOST(raw_text,nick_ptr,username_ptr,hostname_ptr) \
	(sscanf (raw_text, "%a[^!]!%a[^@]@%as", nick_ptr, username_ptr, hostname_ptr))

#define LAG_PING_FORMAT "GCP_%ld.%ld"


/* ************ *
 *  DATA TYPES  *
 * ************ */

/* Note that the incoming parser function only handles incoming 
   messages in the format ":prefix data", which descend from
   parse_in_msg_user_msg, other messages are handled differently. */
typedef void (*IncomingParserFunc) (GIrcClient * irc,
									const gchar * prefix,
									const gchar * data);

typedef struct
{
	const gchar *const name;

	IncomingParserFunc parse_incoming;
}
GIrcCommand;


/* ****************** *
 *  GLOBAL VARIABLES  *
 * ****************** */

static GHashTable *commands = NULL;
static GHashTable *server_variables = NULL;


/* ******************* *
 *  UTILITY FUNCTIONS  *
 * ******************* */

static void
update_my_nick (GIrcClient * irc,
				const gchar * nick)
{
	gchar *userhost;

	g_hash_table_remove (irc->_priv->users, nick);

	if (irc->_priv->my_user->nick != NULL)
		g_free (irc->_priv->my_user->nick);

	irc->_priv->my_user->nick = g_strdup (nick);

	userhost = g_strdup_printf ("%s!%s@%s", nick,
								(irc->_priv->my_user->username != NULL ?
								 irc->_priv->my_user->username : "*"),
								(irc->_priv->my_user->hostname != NULL ?
								 irc->_priv->my_user->hostname : "*"));

	g_hash_table_insert (irc->_priv->users, userhost,
						 girc_user_ref (irc->_priv->my_user));

	g_object_notify (G_OBJECT (irc), "nick");
}


static GIrcUser *
find_or_insert_user_from_prefix (GIrcClient * irc,
								 const gchar * prefix)
{
	GIrcUser *user;

	user = g_hash_table_lookup (irc->_priv->users, prefix);

	if (user == NULL)
	{
		user = girc_user_new_from_string (prefix);

		g_hash_table_insert (irc->_priv->users, g_strdup (prefix), girc_user_ref (user));
	}
	else
	{
		gboolean dirty = FALSE;
		gchar *username = NULL,
		 *hostname = NULL;

		sscanf (prefix, "%*[^!]!%a[^@]@%as", &username, &hostname);

		if (user->username == NULL)
		{
			user->username = g_strdup (username);
			dirty = TRUE;
		}
		if (username != NULL)
			g_free (username);

		if (user->hostname == NULL)
		{
			user->hostname = g_strdup (hostname);
			dirty = TRUE;
		}
		if (hostname != NULL)
			g_free (hostname);

		user = girc_user_ref (user);

		if (dirty)
		{
			g_hash_table_remove (irc->_priv->users, prefix);
			g_hash_table_insert (irc->_priv->users, g_strdup (prefix),
								 girc_user_ref (user));
		}
	}

	return user;
}


static guint
parse_channel_modes_with_args (GIrcClient * irc,
							   GIrcChannel * channel,
							   const GValue * changer,
							   gchar ** modesv,
							   gboolean emit_changed_signals)
{
	GIrcChannelModeFlags modes = GIRC_CHANNEL_MODE_NONE;

	GSList *list = NULL;
	gchar *ptr;
	GValue value = { 0 };
	gboolean mode_on = FALSE;
	guint i = 1;

	for (ptr = modesv[0]; ptr != NULL && *ptr != '\0'; ptr++)
	{
		switch (*ptr)
		{
		case '+':
			mode_on = TRUE;
			break;
		case '-':
			mode_on = FALSE;
			break;

		case GIRC_CHANNEL_MODE_CHAR_MODERATED:
			modes |= GIRC_CHANNEL_MODE_MODERATED;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_MODERATED;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_MODERATED;
			break;

		case GIRC_CHANNEL_MODE_CHAR_NOMSG:
			modes |= GIRC_CHANNEL_MODE_NOMSG;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_NOMSG;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_NOMSG;
			break;

		case GIRC_CHANNEL_MODE_CHAR_TOPIC_PROTECT:
			modes = modes | GIRC_CHANNEL_MODE_TOPIC_PROTECT;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_TOPIC_PROTECT;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_TOPIC_PROTECT;
			break;

		case GIRC_CHANNEL_MODE_CHAR_INVITE_ONLY:
			modes = modes | GIRC_CHANNEL_MODE_INVITE_ONLY;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_INVITE_ONLY;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_INVITE_ONLY;
			break;

		case GIRC_CHANNEL_MODE_CHAR_SECRET:
			modes = modes | GIRC_CHANNEL_MODE_SECRET;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_SECRET;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_SECRET;
			break;

		case GIRC_CHANNEL_MODE_CHAR_PRIVATE:
			modes = modes | GIRC_CHANNEL_MODE_PRIVATE;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_PRIVATE;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_PRIVATE;
			break;

		case GIRC_CHANNEL_MODE_CHAR_ANONYMOUS:
			modes = modes | GIRC_CHANNEL_MODE_ANONYMOUS;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_ANONYMOUS;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_ANONYMOUS;
			break;

		case GIRC_CHANNEL_MODE_CHAR_QUIET:
			modes = modes | GIRC_CHANNEL_MODE_QUIET;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_QUIET;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_QUIET;
			break;
		case GIRC_CHANNEL_MODE_CHAR_REOP:
			modes = modes | GIRC_CHANNEL_MODE_REOP;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_REOP;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_REOP;
			break;

		case GIRC_CHANNEL_MODE_CHAR_KEY:
			if (channel->key != NULL)
				g_free (channel->key);

			if (mode_on)
			{
				if (modesv[i] == NULL)
					i--;

				channel->modes |= GIRC_CHANNEL_MODE_KEY;
				channel->key = g_strdup (modesv[i]);
				i++;
			}
			else
			{
				channel->modes &= ~GIRC_CHANNEL_MODE_KEY;
				channel->key = NULL;
			}

			if (emit_changed_signals)
			{
				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, channel->key);

				_girc_client_emit_channel_changed (irc, channel, changer,
												   GIRC_CHANNEL_INFO_KEY, &value,
												   mode_on);

				g_value_unset (&value);
			}
			break;

		case GIRC_CHANNEL_MODE_CHAR_LIMIT:
			if (mode_on)
			{
				if (modesv[i] == NULL)
					i--;

				channel->modes |= GIRC_CHANNEL_MODE_LIMIT;
				sscanf (modesv[i], "%u", &(channel->limit));
				i++;
			}
			else
			{
				channel->modes &= ~GIRC_CHANNEL_MODE_LIMIT;
				channel->limit = 0;
			}

			if (emit_changed_signals)
			{
				g_value_init (&value, G_TYPE_UINT);
				g_value_set_uint (&value, channel->limit);

				_girc_client_emit_channel_changed (irc, channel, changer,
												   GIRC_CHANNEL_INFO_LIMIT, &value,
												   mode_on);

				g_value_unset (&value);
			}
			break;

		case GIRC_CHANNEL_MODE_CHAR_BAN:
			if (modesv[i] == NULL)
				i--;

			if (mode_on)
			{
				channel->bans = g_slist_prepend (channel->bans, g_strdup (modesv[i]));
			}
			else
			{
				list = g_slist_find_custom (channel->bans, modesv[i],
											(GCompareFunc) g_ascii_strcasecmp);
				if (list != NULL)
				{
					channel->bans = g_slist_remove_link (channel->bans, list);
					g_free (list->data);
				}
			}

			if (emit_changed_signals)
			{
				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, modesv[i]);

				_girc_client_emit_channel_changed (irc, channel, changer,
												   GIRC_CHANNEL_INFO_BAN, &value,
												   mode_on);

				g_value_unset (&value);
			}

			i++;
			break;

		case GIRC_CHANNEL_MODE_CHAR_BAN_EXCEPTIONS:
			if (modesv[i] == NULL)
				i--;

			if (mode_on)
			{
				channel->ban_excepts = g_slist_prepend (channel->ban_excepts,
														g_strdup (modesv[i]));
			}
			else
			{
				list = g_slist_find_custom (channel->ban_excepts, modesv[i],
											(GCompareFunc) g_ascii_strcasecmp);
				if (list != NULL)
				{
					channel->ban_excepts = g_slist_remove_link (channel->ban_excepts,
																list);
					g_free (list->data);
				}
			}

			if (emit_changed_signals)
			{
				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, modesv[i]);

				_girc_client_emit_channel_changed (irc, channel, changer,
												   GIRC_CHANNEL_INFO_BAN_EXCEPTION,
												   &value, mode_on);

				g_value_unset (&value);
			}

			i++;
			break;

		case GIRC_CHANNEL_MODE_CHAR_INVITE_EXCEPTIONS:
			if (modesv[i] == NULL)
				i--;

			if (mode_on)
			{
				channel->invite_excepts = g_slist_prepend (channel->invite_excepts,
														   g_strdup (modesv[i]));
			}
			else
			{
				list = g_slist_find_custom (channel->invite_excepts, modesv[i],
											(GCompareFunc) g_ascii_strcasecmp);
				if (list != NULL)
				{
					channel->invite_excepts =
						g_slist_remove_link (channel->invite_excepts, list);
					g_free (list->data);
				}
			}

			if (emit_changed_signals)
			{
				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, modesv[i]);

				_girc_client_emit_channel_changed (irc, channel, changer,
												   GIRC_CHANNEL_INFO_INVITE_EXCEPTION,
												   &value, mode_on);

				g_value_unset (&value);
			}

			i++;
			break;

		case GIRC_CHANNEL_USER_MODE_CHAR_OPS:
			{
				GIrcChannelUser *user;
				gchar *userhost;

				if (modesv[i] == NULL)
					i--;

				userhost = g_strconcat (modesv[i], "!*@*", NULL);
				user = g_hash_table_lookup (channel->users, userhost);
				g_free (userhost);

				if (user != NULL)
				{
					if (mode_on)
						user->modes |= GIRC_CHANNEL_USER_MODE_OPS;
					else
						user->modes &= ~GIRC_CHANNEL_USER_MODE_OPS;

					if (emit_changed_signals)
					{
						g_value_init (&value, GIRC_TYPE_CHANNEL_USER_MODE_FLAGS);
						g_value_set_flags (&value, GIRC_CHANNEL_USER_MODE_OPS);

						_girc_client_emit_channel_user_changed (irc, channel, user,
																GIRC_CHANNEL_USER_CHANGED_MODES,
																changer, &value);

						g_value_unset (&value);
					}
				}

				i++;
			}
			break;

		case GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS:
			{
				GIrcChannelUser *user;
				gchar *userhost;

				if (modesv[i] == NULL)
					i--;

				userhost = g_strconcat (modesv[i], "!*@*", NULL);
				user = g_hash_table_lookup (channel->users, userhost);
				g_free (userhost);

				if (user != NULL)
				{
					if (mode_on)
						user->modes |= GIRC_CHANNEL_USER_MODE_HALFOPS;
					else
						user->modes &= ~GIRC_CHANNEL_USER_MODE_HALFOPS;

					if (emit_changed_signals)
					{
						g_value_init (&value, GIRC_TYPE_CHANNEL_USER_MODE_FLAGS);
						g_value_set_flags (&value, GIRC_CHANNEL_USER_MODE_HALFOPS);

						_girc_client_emit_channel_user_changed (irc, channel, user,
																GIRC_CHANNEL_USER_CHANGED_MODES,
																changer, &value);

						g_value_unset (&value);
					}
				}

				i++;
			}
			break;

		case GIRC_CHANNEL_USER_MODE_CHAR_VOICE:
			{
				GIrcChannelUser *user;
				gchar *userhost;

				if (modesv[i] == NULL)
					i--;

				userhost = g_strconcat (modesv[i], "!*@*", NULL);
				user = g_hash_table_lookup (channel->users, userhost);
				g_free (userhost);

				if (user != NULL)
				{
					if (mode_on)
						user->modes |= GIRC_CHANNEL_USER_MODE_VOICE;
					else
						user->modes &= ~GIRC_CHANNEL_USER_MODE_VOICE;

					if (emit_changed_signals)
					{
						g_value_init (&value, GIRC_TYPE_CHANNEL_USER_MODE_FLAGS);
						g_value_set_flags (&value, GIRC_CHANNEL_USER_MODE_VOICE);

						_girc_client_emit_channel_user_changed (irc, channel, user,
																GIRC_CHANNEL_USER_CHANGED_MODES,
																changer, &value);

						g_value_unset (&value);
					}
				}


				i++;
			}
			break;

		case GIRC_CHANNEL_USER_MODE_CHAR_CREATOR:
			{
				GIrcChannelUser *user;
				gchar *userhost;

				if (modesv[i] == NULL)
					i--;

				userhost = g_strconcat (modesv[i], "!*@*", NULL);
				user = g_hash_table_lookup (channel->users, userhost);
				g_free (userhost);

				if (user != NULL)
				{
					if (mode_on)
						user->modes |= GIRC_CHANNEL_USER_MODE_CREATOR;
					else
						user->modes &= ~GIRC_CHANNEL_USER_MODE_CREATOR;

					if (emit_changed_signals)
					{
						g_value_init (&value, GIRC_TYPE_CHANNEL_USER_MODE_FLAGS);
						g_value_set_flags (&value, GIRC_CHANNEL_USER_MODE_CREATOR);

						_girc_client_emit_channel_user_changed (irc, channel, user,
																GIRC_CHANNEL_USER_CHANGED_MODES,
																changer, &value);

						g_value_unset (&value);
					}
					break;
				}

				i++;
			}
			break;

		default:
			break;
		}
	}

	return modes;
}


static guint
parse_channel_modes (GIrcChannel * channel,
					 const gchar * modes_str)
{
	gchar *ptr;
	guint modes = GIRC_CHANNEL_MODE_NONE;
	gboolean mode_on = FALSE;

	g_return_val_if_fail (*modes_str == '+' || *modes_str == '-', 0);

	for (ptr = (gchar *) modes_str; ptr != NULL && *ptr != '\0'; ptr++)
	{
		switch (*ptr)
		{
		case '+':
			mode_on = TRUE;
			break;
		case '-':
			mode_on = FALSE;
			break;

		case GIRC_CHANNEL_MODE_CHAR_MODERATED:
			modes |= GIRC_CHANNEL_MODE_MODERATED;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_MODERATED;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_MODERATED;
			break;

		case GIRC_CHANNEL_MODE_CHAR_NOMSG:
			modes |= GIRC_CHANNEL_MODE_NOMSG;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_NOMSG;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_NOMSG;
			break;

		case GIRC_CHANNEL_MODE_CHAR_TOPIC_PROTECT:
			modes |= GIRC_CHANNEL_MODE_TOPIC_PROTECT;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_TOPIC_PROTECT;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_TOPIC_PROTECT;
			break;

		case GIRC_CHANNEL_MODE_CHAR_INVITE_ONLY:
			modes |= GIRC_CHANNEL_MODE_INVITE_ONLY;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_INVITE_ONLY;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_INVITE_ONLY;
			break;

		case GIRC_CHANNEL_MODE_CHAR_SECRET:
			modes |= GIRC_CHANNEL_MODE_SECRET;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_SECRET;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_SECRET;
			break;

		case GIRC_CHANNEL_MODE_CHAR_KEY:
			modes |= GIRC_CHANNEL_MODE_KEY;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_KEY;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_KEY;
			break;

		case GIRC_CHANNEL_MODE_CHAR_LIMIT:
			modes |= GIRC_CHANNEL_MODE_LIMIT;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_LIMIT;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_LIMIT;
			break;

		case GIRC_CHANNEL_MODE_CHAR_PRIVATE:
			modes |= GIRC_CHANNEL_MODE_PRIVATE;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_PRIVATE;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_PRIVATE;
			break;

		case GIRC_CHANNEL_MODE_CHAR_ANONYMOUS:
			modes |= GIRC_CHANNEL_MODE_ANONYMOUS;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_ANONYMOUS;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_ANONYMOUS;
			break;

		case GIRC_CHANNEL_MODE_CHAR_QUIET:
			modes |= GIRC_CHANNEL_MODE_QUIET;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_QUIET;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_QUIET;
			break;

		case GIRC_CHANNEL_MODE_CHAR_REOP:
			modes |= GIRC_CHANNEL_MODE_REOP;

			if (mode_on)
				channel->modes |= GIRC_CHANNEL_MODE_REOP;
			else
				channel->modes &= ~GIRC_CHANNEL_MODE_REOP;
			break;

		default:
			break;
		}
	}

	return modes;
}


static GIrcCtcpMessageType
parse_in_ctcp (const gchar * msg,
			   gchar ** result_msg)
{
	GIrcCtcpMessageType type;
	gchar *ctcp_data = NULL;

	sscanf (msg, "\001%a[^\001]\001", &ctcp_data);

	if (ctcp_data != NULL)
	{
		gchar *ctcp_command = NULL,
		 *ctcp_msg = NULL;

		sscanf (ctcp_data, "%as %a[^\001]", &ctcp_command, &ctcp_msg);
		g_free (ctcp_data);

		if (ctcp_command == NULL)
		{
			type = GIRC_CTCP_MESSAGE_NONE;
			*result_msg = g_strdup (msg);
		}
		else if (g_ascii_strcasecmp ("ACTION", ctcp_command) == 0)
		{
			type = GIRC_CTCP_MESSAGE_ACTION;
			*result_msg = g_strdup (ctcp_msg);
		}
		else if (g_ascii_strcasecmp ("VERSION", ctcp_command) == 0)
		{
			type = GIRC_CTCP_MESSAGE_VERSION;
			*result_msg = NULL;
		}
		else if (g_ascii_strcasecmp ("DCC SEND", ctcp_command) == 0)
		{
			type = GIRC_CTCP_MESSAGE_DCC_SEND;
			*result_msg = g_strdup (ctcp_msg);
		}
		else if (g_ascii_strcasecmp ("DCC RECV", ctcp_command) == 0)
		{
			type = GIRC_CTCP_MESSAGE_DCC_RECV;
			*result_msg = g_strdup (ctcp_msg);
		}
		else if (g_ascii_strcasecmp ("DCC CHAT", ctcp_command) == 0)
		{
			type = GIRC_CTCP_MESSAGE_DCC_CHAT;
			*result_msg = g_strdup (ctcp_msg);
		}
		else
		{
			type = GIRC_CTCP_MESSAGE_UNKNOWN;
			*result_msg = g_strdup (ctcp_msg);
		}

		g_free (ctcp_command);
		g_free (ctcp_msg);
	}
	else
	{
		type = GIRC_CTCP_MESSAGE_UNKNOWN;
		*result_msg = NULL;
	}

	return type;
}


/* ************************************ *
 *  INCOMING COMMAND PARSING FUNCTIONS  *
 * ************************************ */

static void
parse_incoming_join (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	gchar *target = NULL;

	sscanf (data, ":%as", &target);

	if (target != NULL)
	{
		GIrcUser *user = find_or_insert_user_from_prefix (irc, prefix);
		GIrcChannel *channel = g_hash_table_lookup (irc->_priv->targets, target);

		if (channel == NULL)
		{
			channel = _girc_client_setup_new_channel (irc, target);
		}
		else if (user != irc->_priv->my_user)
		{
			GIrcChannelUser *ch_user = girc_channel_user_new ();

			ch_user->user = user;
			g_hash_table_insert (channel->users, g_strdup (prefix), ch_user);

			_girc_client_emit_channel_user_changed (irc, channel, ch_user,
													GIRC_CHANNEL_USER_CHANGED_JOIN, NULL,
													NULL);
		}
		g_free (target);
	}
}


static void
parse_incoming_part (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	GIrcChannel *channel = NULL;
	gchar *target = NULL,
	 *reason = NULL;

	sscanf (data, "%as :%a[^\r\n]", &target, &reason);

	if (target != NULL)
	{
		channel = g_hash_table_lookup (irc->_priv->targets, target);
		g_free (target);

		if (channel != NULL)
		{
			GIrcChannelUser *ch_user;

			ch_user = g_hash_table_lookup (channel->users, prefix);

			if (ch_user != NULL)
			{
				if (ch_user->user == irc->_priv->my_user)
				{
					_girc_client_emit_target_closed (irc, GIRC_TARGET (channel), reason);
				}
				else
				{
					GValue value = { 0 };

					g_value_init (&value, G_TYPE_STRING);
					g_value_set_string (&value, reason);

					_girc_client_emit_channel_user_changed (irc, channel, ch_user,
															GIRC_CHANNEL_USER_CHANGED_PART,
															NULL, &value);

					g_value_unset (&value);
				}
			}
		}
	}

	g_free (reason);
}


static void
parse_incoming_notice (GIrcClient * irc,
					   const gchar * prefix,
					   const gchar * data)
{
	gchar *target_str = NULL,
	 *msg = NULL;

	sscanf (data, "%as :%a[^\r\n]", &target_str, &msg);

	if (target_str != NULL)
	{
		GIrcUser *user;
		GIrcTarget *target;

		user = find_or_insert_user_from_prefix (irc, prefix);

		/* Query to our user */
		if (g_ascii_strcasecmp (target_str, irc->_priv->my_user->nick) == 0)
		{
			target = g_hash_table_lookup (irc->_priv->targets, prefix);

			if (target == NULL)
			{
				target = _girc_client_setup_new_query (irc, user);
			}

			_girc_client_emit_target_recv (irc, target, NULL,
										   GIRC_USER_MESSAGE_NOTICE, msg);
		}
		/* Message to a channel */
		else
		{
			GValue sender = { 0 };

			target = g_hash_table_lookup (irc->_priv->targets, target_str);

			if (IS_USERHOST (prefix))
			{
				GIrcChannelUser *ch_user;

				ch_user = g_hash_table_lookup (target->channel.users, prefix);

				if (ch_user != NULL)
				{
					g_value_init (&sender, GIRC_TYPE_CHANNEL_USER);
					g_value_set_boxed (&sender, ch_user);
				}
				else if (user != NULL)
				{
					g_value_init (&sender, GIRC_TYPE_USER);
					g_value_set_boxed (&sender, user);
				}
				else
				{
					g_value_init (&sender, G_TYPE_STRING);
					g_value_set_string (&sender, prefix);
				}
			}
			else
			{
				g_value_init (&sender, G_TYPE_STRING);
				g_value_set_string (&sender, prefix);
			}

			_girc_client_emit_target_recv (irc, target, &sender,
										   GIRC_USER_MESSAGE_NOTICE, msg);

			g_value_unset (&sender);
		}

		if (user != NULL)
			girc_user_unref (user);

		g_free (target_str);
	}

	g_free (msg);
}


static void
parse_incoming_privmsg (GIrcClient * irc,
						const gchar * prefix,
						const gchar * data)
{
	GIrcUser *user;
	gchar *target_str = NULL,
	 *msg = NULL;

	user = find_or_insert_user_from_prefix (irc, prefix);

	sscanf (data, "%as :%a[^\r\n]", &target_str, &msg);

	if (target_str != NULL)
	{
		GIrcTarget *target;

		/* Query to our user */
		if (girc_ascii_strcaseequal (target_str, irc->_priv->my_user->nick))
		{
			target = g_hash_table_lookup (irc->_priv->targets, prefix);

			if (msg[0] == '\001')
			{
				gchar *out_msg = NULL;
				GIrcCtcpMessageType type = parse_in_ctcp (msg, &out_msg);

				if (type == GIRC_CTCP_MESSAGE_ACTION)
				{
					if (target == NULL)
					{
						target = _girc_client_setup_new_query (irc, user);
					}

					_girc_client_emit_target_recv (irc, target, NULL,
												   GIRC_USER_MESSAGE_ACTION, out_msg);
				}
				else
				{
					_girc_client_emit_ctcp_recv (irc, user, type, out_msg);
				}

				if (out_msg != NULL)
					g_free (out_msg);
			}
			else
			{
				if (target == NULL)
				{
					target = _girc_client_setup_new_query (irc, user);
				}

				_girc_client_emit_target_recv (irc, target, NULL,
											   GIRC_USER_MESSAGE_PRIVMSG, msg);
			}
		}
		/* Message to a channel */
		else
		{
			GIrcChannelUser *ch_user;
			GValue sender = { 0 };

			target = g_hash_table_lookup (irc->_priv->targets, target_str);

			if (target != NULL)
			{
				ch_user = g_hash_table_lookup (target->channel.users, prefix);

				if (ch_user != NULL)
				{
					g_value_init (&sender, GIRC_TYPE_CHANNEL_USER);
					g_value_set_boxed (&sender, ch_user);
				}
				else if (user != NULL)
				{
					g_value_init (&sender, GIRC_TYPE_USER);
					g_value_set_boxed (&sender, user);
				}
				else
				{
					g_value_init (&sender, G_TYPE_STRING);
					g_value_set_string (&sender, prefix);
				}

				if (msg[0] == '\001')
				{
					gchar *out_msg = NULL;
					GIrcCtcpMessageType type = parse_in_ctcp (msg, &out_msg);

					if (type == GIRC_CTCP_MESSAGE_ACTION)
					{
						_girc_client_emit_target_recv (irc, target, &sender,
													   GIRC_USER_MESSAGE_ACTION, out_msg);
					}
					else
					{
						_girc_client_emit_ctcp_recv (irc, user, type, out_msg);
					}

					if (out_msg != NULL)
						g_free (out_msg);
				}
				else
				{
					_girc_client_emit_target_recv (irc, target, &sender,
												   GIRC_USER_MESSAGE_PRIVMSG, msg);
				}
			}
			else
			{
				g_warning ("Target channel not found!");
			}

			g_value_unset (&sender);
		}

		g_free (target_str);
	}

	if (user != NULL)
		girc_user_unref (user);

	g_free (msg);
}


// :nick!user@hostname MODE target (+|-)modes <data>
static void
parse_incoming_mode (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	GIrcChannel *channel = NULL;
	gchar *target_str = NULL,
	 *modes_str = NULL;

	sscanf (data, "%as %a[^\r\n]", &target_str, &modes_str);

	if (target_str != NULL)
	{
		channel = GIRC_CHANNEL (g_hash_table_lookup (irc->_priv->targets, target_str));
		g_free (target_str);

		if (channel != NULL && modes_str != NULL)
		{
			guint modes;
			gchar **modesv = NULL;
			GValue changer = { 0 };
			GValue value = { 0 };

			modesv = g_strsplit (modes_str, " ", -1);

			if (IS_USERHOST (prefix))
			{
				GIrcChannelUser *ch_user = g_hash_table_lookup (channel->users, prefix);

				if (ch_user != NULL)
				{
					g_value_init (&changer, GIRC_TYPE_CHANNEL_USER);
					g_value_set_boxed (&changer, ch_user);
				}
				else if (IS_USERHOST (prefix))
				{
					GIrcUser *user = find_or_insert_user_from_prefix (irc, prefix);

					if (user != NULL)
					{
						g_value_init (&changer, GIRC_TYPE_USER);
						g_value_set_boxed (&changer, user);
					}
					else
					{
						g_value_init (&changer, G_TYPE_STRING);
						g_value_set_string (&changer, prefix);
					}
				}
				else
				{
					g_value_init (&changer, G_TYPE_STRING);
					g_value_set_string (&changer, prefix);
				}
			}
			else
			{
				g_value_init (&changer, G_TYPE_STRING);
				g_value_set_string (&changer, prefix);
			}

			if (modesv[1] != NULL && modesv[1][0] != '\0')
			{
				modes = parse_channel_modes_with_args (irc, channel, &changer, modesv,
													   TRUE);

				if (modes > 0)
				{
					g_value_init (&value, GIRC_TYPE_CHANNEL_MODE_FLAGS);
					g_value_set_flags (&value, modes);

					_girc_client_emit_channel_changed (irc, channel, &changer,
													   GIRC_CHANNEL_INFO_MODES,
													   &value, FALSE);

					g_value_unset (&value);
				}
			}
			else
			{
				modes = parse_channel_modes (channel, modesv[0]);

				g_value_init (&value, GIRC_TYPE_CHANNEL_MODE_FLAGS);
				g_value_set_flags (&value, modes);

				_girc_client_emit_channel_changed (irc, channel, &changer,
												   GIRC_CHANNEL_INFO_MODES, &value,
												   FALSE);

				g_value_unset (&value);
			}

			g_value_unset (&changer);

			g_strfreev (modesv);
		}
	}

	g_free (modes_str);
}

// :nick!username@hostname QUIT :Reason
static void
parse_incoming_quit (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	GIrcUser *user = find_or_insert_user_from_prefix (irc, prefix);

	if (user != NULL && data != NULL && data[0] == ':')
	{
		GSList *targets;

		for (targets = _girc_client_get_users_targets (irc, prefix);
			 targets != NULL; targets = g_slist_remove (targets, targets->data))
		{
			if (GIRC_IS_CHANNEL (targets->data))
			{
				GIrcChannel *channel = (GIrcChannel *) (targets->data);
				GIrcChannelUser *ch_user = g_hash_table_lookup (channel->users, prefix);

				if (ch_user != NULL)
				{
					GValue value = { 0 };

					g_value_init (&value, G_TYPE_STRING);
					g_value_set_string (&value, data + 1);

					_girc_client_emit_channel_user_changed (irc, channel, ch_user,
															GIRC_CHANNEL_USER_CHANGED_QUIT,
															NULL, &value);

					g_value_unset (&value);
				}
			}
			else if (GIRC_IS_QUERY (targets->data))
			{
				_girc_client_emit_query_changed (irc, GIRC_QUERY (targets->data),
												 GIRC_QUERY_CHANGED_QUIT, data + 1);
				_girc_client_emit_target_closed (irc, GIRC_TARGET (targets->data),
												 data + 1);
			}
		}

		girc_user_unref (user);
	}
}


static void
parse_incoming_topic (GIrcClient * irc,
					  const gchar * prefix,
					  const gchar * data)
{
	gchar *target_str = NULL,
	 *topic_str = NULL;

	sscanf (data, "%as :%a[^\r\n]", &target_str, &topic_str);

	if (target_str != NULL)
	{
		GIrcChannel *channel = g_hash_table_lookup (irc->_priv->targets, target_str);

		g_free (target_str);

		if (channel != NULL)
		{
			GIrcChannelUser *ch_user = g_hash_table_lookup (channel->users, prefix);

			if (channel->topic != NULL)
				g_free (channel->topic);

			channel->topic = g_strdup (topic_str);

			if (ch_user != NULL)
			{
				GValue changer = { 0 };
				GValue value = { 0 };

				g_value_init (&changer, GIRC_TYPE_CHANNEL_USER);
				g_value_set_boxed (&changer, ch_user);

				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, topic_str);

				_girc_client_emit_channel_changed (irc, channel, &changer,
												   GIRC_CHANNEL_INFO_TOPIC, &value,
												   FALSE);

				g_value_unset (&changer);
				g_value_unset (&value);
			}
			else
			{
				_girc_client_emit_channel_changed (irc, channel, NULL,
												   GIRC_CHANNEL_INFO_TOPIC, NULL, FALSE);
			}
		}
	}

	g_free (topic_str);
}

/* :<nick>!<user>@<host> NICK :<new_nick> */
static void
parse_incoming_nick (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	if (data != NULL && data[0] == ':')
	{
		GIrcUser *user;
		gchar *old_nick = NULL;
		GSList *targets;

		user = find_or_insert_user_from_prefix (irc, prefix);

		g_hash_table_remove (irc->_priv->users, prefix);

		old_nick = user->nick;

		user->nick = g_strdup (data + 1);
		/* Re-insert into the users table w/ new nick */
		g_hash_table_insert (irc->_priv->users, girc_user_to_string (user), user);

		if (user == irc->_priv->my_user)
		{
			_girc_client_emit_nick_changed (irc, user->nick);
		}

		for (targets = _girc_client_get_users_targets (irc, prefix);
			 targets != NULL; targets = g_slist_remove_link (targets, targets))
		{
			if (GIRC_IS_CHANNEL (targets->data))
			{
				GIrcChannel *channel = GIRC_CHANNEL (targets->data);
				GIrcChannelUser *ch_user = g_hash_table_lookup (channel->users,
																prefix);

				if (ch_user != NULL)
				{
					GValue value = { 0 };

					girc_channel_user_ref (ch_user);
					g_hash_table_remove (channel->users, prefix);
					g_hash_table_insert (channel->users, girc_user_to_string (user),
										 ch_user);

					g_value_init (&value, G_TYPE_STRING);
					g_value_set_string (&value, old_nick);

					_girc_client_emit_channel_user_changed (irc, channel, ch_user,
															GIRC_CHANNEL_USER_CHANGED_NICK,
															NULL, &value);

					g_value_unset (&value);
				}
			}
			else if (GIRC_IS_QUERY (targets->data))
			{
				GIrcQuery *query = GIRC_QUERY (targets->data);

				_girc_client_emit_query_changed (irc, query, GIRC_QUERY_CHANGED_NICK,
												 NULL);

				if (g_hash_table_steal (irc->_priv->targets, query->userhost))
				{
					g_free (query->userhost);

					query->userhost = girc_user_to_string (user);

					g_hash_table_insert (irc->_priv->targets, query->userhost, query);
				}
			}
			else
			{
				g_assert_not_reached ();
			}
		}

		g_free (old_nick);
	}
}


static void
parse_incoming_kick (GIrcClient * irc,
					 const gchar * prefix,
					 const gchar * data)
{
	GIrcUser *user;
	gchar *channel_str = NULL,
	 *target_str = NULL,
	 *reason = NULL;

	user = find_or_insert_user_from_prefix (irc, prefix);

	sscanf (data, "%as %as :%a[^\r\n]", &channel_str, &target_str, &reason);

	if (channel_str != NULL)
	{
		GIrcChannel *channel = g_hash_table_lookup (irc->_priv->targets, channel_str);

		g_free (channel_str);

		if (channel != NULL)
		{
			GIrcChannelUser *ch_user,
			 *prefix_user;
			gchar *key = g_strdup_printf ("%s!*@*", target_str);

			ch_user = g_hash_table_lookup (channel->users, key);
			g_free (key);

			prefix_user = g_hash_table_lookup (channel->users, prefix);

			if (ch_user != NULL)
			{
				GValue changer = { 0 };
				GValue value = { 0 };

				g_value_init (&changer, GIRC_TYPE_CHANNEL_USER);
				g_value_set_boxed (&changer, prefix_user);

				g_value_init (&value, G_TYPE_STRING);
				g_value_set_string (&value, reason);

				_girc_client_emit_channel_user_changed (irc, channel, ch_user,
														GIRC_CHANNEL_USER_CHANGED_KICKED,
														&changer, &value);

				g_value_unset (&changer);
				g_value_unset (&value);

				if (ch_user == channel->my_user)
				{
					_girc_client_emit_target_closed (irc, GIRC_TARGET (channel), reason);
				}

				g_hash_table_remove (channel->users, target_str);
			}
		}
	}

	g_free (target_str);
	g_free (reason);
}


/* ****************************** *
 *  BASIC INCOMING PARSING STACK  *
 * ****************************** */

static void
parse_in_msg_user_msg (GIrcClient * irc,
					   const gchar * prefix,
					   const gchar * data)
{
	gchar *cmd_str = NULL,
	 *rest_of_data = NULL;
	IncomingParserFunc command = NULL;

	sscanf (data, "%as %a[^\r\n]", &cmd_str, &rest_of_data);

	command = g_hash_table_lookup (commands, cmd_str);

	if (command != NULL)
	{
		(*command) (irc, prefix, rest_of_data);
	}
	else
	{
		g_warning ("Unknown incoming command recieved: \"%s\"", cmd_str);
	}

	g_free (cmd_str);
	g_free (rest_of_data);
}


/* The infamous 005 Numeric */
static void
parse_in_server_myinfo (GIrcClient * irc,
						const gchar * data)
{
	gchar **vars,
	**lang_items;
	gchar *valid_data,
	 *key,
	 *value,
	 *chars = NULL,
	 *prefixes = NULL;
	gint i,
	  scanf_retval,
	  i2;
	guint key_id;

	g_object_freeze_notify (G_OBJECT (irc));

	sscanf (data, "%a[^:]:%*s", &valid_data);
	vars = g_strsplit (valid_data, " ", -1);
	g_free (valid_data);

	for (i = 0; vars[i] != NULL && vars[i][0] != '\0'; i++)
	{
		scanf_retval = sscanf (vars[i], "%a[^= ]=%as", &key, &value);

		if (scanf_retval < 0)
			break;

		key_id = GPOINTER_TO_UINT (g_hash_table_lookup (server_variables, key));

		switch (key_id)
		{
			/* Strings */
		case GIRC_VARIABLE_NETWORK_ITEM:
			irc->_priv->vars->network_name = g_strdup (value);
			g_object_notify (G_OBJECT (irc), "network-name");
			break;
		case GIRC_VARIABLE_CHARSET_ITEM:
			if (g_ascii_strcasecmp (value, "ascii") == 0)
			{
				irc->_priv->vars->char_mapping = GIRC_CLIENT_CHARACTER_MAPPING_ASCII;
				g_object_notify (G_OBJECT (irc), "character-mapping");
			}
			else if (g_ascii_strcasecmp (value, "rfc1459") == 0)
			{
				irc->_priv->vars->char_mapping = GIRC_CLIENT_CHARACTER_MAPPING_RFC1459;
				g_object_notify (G_OBJECT (irc), "character-mapping");
			}
			else
			{
				irc->_priv->vars->charset = g_strdup (value);
				g_object_notify (G_OBJECT (irc), "server-character-set");
			}
			break;
		case GIRC_VARIABLE_CHARMAPPING_ITEM:
			if (g_ascii_strcasecmp (value, "ascii") == 0)
				irc->_priv->vars->char_mapping = GIRC_CLIENT_CHARACTER_MAPPING_ASCII;
			else if (g_ascii_strcasecmp (value, "rfc1459") == 0)
				irc->_priv->vars->char_mapping = GIRC_CLIENT_CHARACTER_MAPPING_RFC1459;
			else
				irc->_priv->vars->char_mapping = GIRC_CLIENT_CHARACTER_MAPPING_UNKNOWN;
			g_object_notify (G_OBJECT (irc), "character-mapping");
			break;
			/* Limits */
		case GIRC_VARIABLE_MAX_CHANNELS_ITEM:
			irc->_priv->vars->max_channels = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "max-channels");
			break;
		case GIRC_VARIABLE_MAX_BANS_ITEM:
			irc->_priv->vars->max_num_of_bans = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "max-bans");
			break;
		case GIRC_VARIABLE_MAX_PER_MODE_CMD_ITEM:
			irc->_priv->vars->max_per_mode_cmds = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "mode-command-size");
			break;
		case GIRC_VARIABLE_MAX_WATCHES_ITEM:
			irc->_priv->vars->max_watches = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "max-watches");
			break;
		case GIRC_VARIABLE_MAX_TARGETS_ITEM:
			irc->_priv->vars->max_targets = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "max-targets");
			break;
		case GIRC_VARIABLE_NICK_LENGTH_ITEM:
			irc->_priv->vars->max_nick_length = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "nick-length");
			break;
		case GIRC_VARIABLE_TOPIC_LENGTH_ITEM:
			irc->_priv->vars->max_topic_length = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "topic-length");
			break;
		case GIRC_VARIABLE_KICK_LENGTH_ITEM:
			irc->_priv->vars->max_kickmsg_length = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "kick-message-length");
			break;
		case GIRC_VARIABLE_CHANNEL_LENGTH_ITEM:
			irc->_priv->vars->max_channel_length = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "channel-name-length");
			break;
		case GIRC_VARIABLE_CHANID_LENGTH_ITEM:
			irc->_priv->vars->chanid_length = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "chanid-length");
			break;
		case GIRC_VARIABLE_SILENCE_CMD_ITEM:
			irc->_priv->vars->silence_list_size = CLAMP (atoi (value), 0, 255);
			g_object_notify (G_OBJECT (irc), "silence-list-size");
			break;

			/* Environment */
		case GIRC_VARIABLE_BAN_EXCEPTIONS_ITEM:
			irc->_priv->vars->ban_exceptions = TRUE;
			g_object_notify (G_OBJECT (irc), "ban-exceptions");
			break;
		case GIRC_VARIABLE_INVITE_EXCEPTIONS_ITEM:
			irc->_priv->vars->invite_exceptions = TRUE;
			g_object_notify (G_OBJECT (irc), "invite-exceptions");
			break;
		case GIRC_VARIABLE_PENALTY_ITEM:
			irc->_priv->vars->penalty = TRUE;
			g_object_notify (G_OBJECT (irc), "penalty");
			break;
		case GIRC_VARIABLE_FORCED_NICKCHANGE_ITEM:
			irc->_priv->vars->forced_nickchange = TRUE;
			g_object_notify (G_OBJECT (irc), "force-nickchange");
			break;
		case GIRC_VARIABLE_SAFE_LIST_ITEM:
			irc->_priv->vars->safe_list = TRUE;
			g_object_notify (G_OBJECT (irc), "safe-list");
			break;
		case GIRC_VARIABLE_WHO_IS_WHOX_ITEM:
			irc->_priv->vars->who_is_whox = TRUE;
			g_object_notify (G_OBJECT (irc), "who-command-is-whox");
			break;
		case GIRC_VARIABLE_CALLERID_ITEM:
		case GIRC_VARIABLE_ACCEPT_ITEM:
			irc->_priv->vars->server_side_ignore = TRUE;
			g_object_notify (G_OBJECT (irc), "server-side-ignore");
			break;

			/* Supported commands */
		case GIRC_VARIABLE_USERIP_CMD_ITEM:
			irc->_priv->vars->userip_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "userip-command");
			break;
		case GIRC_VARIABLE_KNOCK_CMD_ITEM:
			irc->_priv->vars->knock_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "knock-command");
			break;
		case GIRC_VARIABLE_CPRIVMSG_CMD_ITEM:
			irc->_priv->vars->cprivmsg_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "cprivmsg-command");
			break;
		case GIRC_VARIABLE_CNOTICE_CMD_ITEM:
			irc->_priv->vars->cnotice_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "cnotice-command");
			break;
		case GIRC_VARIABLE_CJOIN_CMD_ITEM:
			irc->_priv->vars->cjoin_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "cjoin-command");
			break;
		case GIRC_VARIABLE_WALLCHOPS_CMD_ITEM:
			irc->_priv->vars->wallchops_cmd = TRUE;
			g_object_notify (G_OBJECT (irc), "wallchops-command");
			break;

			/* Lists */
			/* User Modes */
		case GIRC_VARIABLE_PREFIX_ITEM:
			/* This junk is here because Freenode sucks ass. */
			if (value[0] == '(')
			{
				sscanf (value, "(%a[^)])%as", &chars, &prefixes);

				if (chars == NULL)
				{
					g_free (prefixes);
					prefixes = NULL;
					break;
				}
			}
			else if (value[0] != ' ')
			{
				sscanf (value, "%a[^ ]", &prefixes);

				if (prefixes == NULL)
					break;
			}
			else
			{
				break;
			}

			if (chars == NULL)
			{
				chars = g_new (gchar, 3);
				chars[0] = GIRC_CHANNEL_USER_MODE_CHAR_OPS;
				chars[1] = GIRC_CHANNEL_USER_MODE_CHAR_VOICE;
				chars[3] = '\0';
			}

			for (i2 = 0; chars[i2] != '\0' && prefixes[i2] != '\0'; i2++)
			{
				irc->_priv->vars->user_modes =
					g_slist_append (irc->_priv->vars->user_modes,
									girc_channel_user_mode_new (chars[i2], prefixes[i2]));
				g_object_notify (G_OBJECT (irc), "channel-user-modes");
			}

			g_free (chars);
			g_free (prefixes);
			chars = NULL;
			prefixes = NULL;
			break;
			/* Channel Modes */
		case GIRC_VARIABLE_CHANNEL_MODES_ITEM:
			for (i2 = 0; value[i2] != '\0'; i2++)
			{
				if (value[i2] != ',')
				{
					irc->_priv->vars->channel_modes =
						g_slist_append (irc->_priv->vars->channel_modes,
										(gpointer)
										girc_channel_mode_new_from_char (value[i2]));
					g_object_notify (G_OBJECT (irc), "channel-modes");
				}
			}
			break;
			/* Channel Prefixes */
		case GIRC_VARIABLE_CHANTYPES_ITEM:
			for (i2 = 0; value[i2] != '\0'; i2++)
			{
				irc->_priv->vars->channel_prefixes =
					g_slist_append (irc->_priv->vars->channel_prefixes,
									(gpointer)
									girc_channel_prefix_get_prefix (value[i2]));
				g_object_notify (G_OBJECT (irc), "channel-types");
			}
			break;
			/* Languages */
		case GIRC_VARIABLE_LANGUAGE_CMD_ITEM:
			irc->_priv->vars->language_cmd = TRUE;
			lang_items = g_strsplit (value, ",", -1);

			for (i2 = 0; lang_items[i2] != NULL && lang_items[i2] != '\0'; i2++)
			{
				irc->_priv->vars->languages =
					g_slist_append (irc->_priv->vars->languages, lang_items[i2]);
			}

			/* Keep the strings themselves around for the list, so no g_strfreev calls */
			g_free (lang_items);
			break;

			/* Leave this here, so I can get notified about missing/unknown variables */
		default:
			g_warning (_("Unknown server variable: '%s'. Please report to "
						 "<jcape@ignore-your.tv>."), key);
			break;
		}
	}

	g_strfreev (vars);
	g_object_thaw_notify (G_OBJECT (irc));
}


static void
parse_whois_user (GIrcClient * irc,
				  const gchar * data)
{
	GIrcUser *user;
	gchar *userhost,
	 *nick = NULL,
	 *username = NULL,
	 *ip_address = NULL,
	 *hostname = NULL,
	 *realname = NULL;
	gboolean dirty = FALSE;

	sscanf (data, "%as %as %as %as :%a[^\r\n]",
			&nick, &username, &ip_address, &hostname, &realname);

	userhost = g_strdup_printf ("%s!%s@%s", nick, username, hostname);
	g_free (nick);

	user = g_hash_table_lookup (irc->_priv->users, userhost);

	if (user == NULL)
	{
		user = girc_user_new_from_string (userhost);
		dirty = TRUE;
	}
	else
	{
		girc_user_ref (user);
	}

	if (irc->_priv->waiting_whois == NULL)
		irc->_priv->waiting_whois = girc_user_ref (user);

	/* Username */
	if (user->username == NULL)
	{
		user->username = g_strdup (username);
		dirty = TRUE;
	}
	g_free (username);

	/* Hostname */
	if (user->hostname == NULL)
	{
		user->hostname = g_strdup (hostname);
		dirty = TRUE;
	}
	g_free (hostname);

	/* Realname */
	if (user->realname == NULL)
	{
		user->realname = g_strdup (realname);
	}
	g_free (realname);

	/* Re-insert into hash table */
	if (dirty)
	{
		g_hash_table_remove (irc->_priv->users, userhost);
		g_hash_table_insert (irc->_priv->users, userhost, girc_user_ref (user));
	}
	else
	{
		g_free (userhost);
	}


	irc->_priv->waiting_whois->ip_address = g_strdup (ip_address);
	g_free (ip_address);

	girc_user_unref (user);
}


static void
parse_channel_on (GIrcClient * irc,
				  const gchar * data,
				  gchar ** name,
				  GIrcChannelUserModeFlags * modes)
{
	GSList *user_modes;
	gchar *mode_char;
	guint n_modes,
	  misses;

	g_return_if_fail (name != NULL && *name == NULL);
	g_return_if_fail (modes != NULL);

	/* Fill in some defaults if the server is stupid and doesn't send a 005 line...
	   And thus we've no idea what channel user modes it supports.
	   AHEM, F R E E N O D E ! */
	if (irc->_priv->vars->user_modes == NULL)
	{
		g_object_freeze_notify (G_OBJECT (irc));
		irc->_priv->vars->user_modes =
			g_slist_prepend (irc->_priv->vars->user_modes,
							 girc_channel_user_mode_new ('h', '%'));
		g_object_notify (G_OBJECT (irc), "channel-user-modes");
		irc->_priv->vars->user_modes =
			g_slist_prepend (irc->_priv->vars->user_modes,
							 girc_channel_user_mode_new ('v', '+'));
		g_object_notify (G_OBJECT (irc), "channel-user-modes");
		irc->_priv->vars->user_modes =
			g_slist_prepend (irc->_priv->vars->user_modes,
							 girc_channel_user_mode_new ('o', '@'));
		g_object_notify (G_OBJECT (irc), "channel-user-modes");
		g_object_thaw_notify (G_OBJECT (irc));

	}

	n_modes = g_slist_length (irc->_priv->vars->user_modes);

	for (mode_char = (gchar *) data; mode_char != '\0'; ++mode_char)
	{
		misses = 0;

		for (user_modes = irc->_priv->vars->user_modes;
			 user_modes != NULL; user_modes = user_modes->next)
		{
			if (GIRC_IS_CHANNEL_USER_MODE (user_modes->data))
			{
				GIrcChannelUserMode *mode = (GIrcChannelUserMode *) (user_modes->data);

				if (mode != NULL && *mode_char == mode->prefix_char)
				{
					*modes = (*modes | mode->mode);
				}
				else
				{
					misses++;
				}
			}
		}

		if (misses >= n_modes)
			break;
	}

	*name = g_strdup (mode_char);
}


static void
parse_whois_channels_on (GIrcClient * irc,
						 const gchar * data)
{
	gchar *chans = NULL;
	gchar **channels = NULL;
	gint i;

	g_return_if_fail (GIRC_IS_USER (irc->_priv->waiting_whois));

	sscanf (data, "%*s :%a[^\r\n]", &chans);
	channels = g_strsplit (chans, " ", -1);
	g_free (chans);

	for (i = 0; channels[i] != NULL && channels[i][0] != '\0'; i++)
	{
		GIrcUserChannelOn *chan_on = NULL;
		gchar *name = NULL;
		GIrcChannelUserModeFlags modes = GIRC_CHANNEL_USER_MODE_NONE;

		parse_channel_on (irc, channels[i], &name, &modes);

		chan_on = g_hash_table_lookup (irc->_priv->waiting_whois->channels, name);

		if (chan_on == NULL)
		{
			chan_on = girc_user_channel_on_new ();
			chan_on->name = name;
		}
		else
		{
			g_hash_table_steal (irc->_priv->waiting_whois->channels, chan_on->name);
			g_free (name);
		}

		chan_on->modes = modes;

		g_hash_table_insert (irc->_priv->waiting_whois->channels, chan_on->name, chan_on);
	}

	g_strfreev (channels);
}


static void
parse_whois_server (GIrcClient * irc,
					const gchar * data)
{
	gchar *server,
	 *server_comment;

	g_return_if_fail (GIRC_IS_USER (irc->_priv->waiting_whois));

	sscanf (data, "%*s %as :%as", &server, &server_comment);

	irc->_priv->waiting_whois->server = g_strdup (server);
	g_free (server);

	irc->_priv->waiting_whois->server_comment = g_strdup (server_comment);
	g_free (server_comment);
}


static void
parse_whois_idle (GIrcClient * irc,
				  const gchar * data)
{
	g_return_if_fail (GIRC_IS_USER (irc->_priv->waiting_whois));

	sscanf (data, "%*s %ld :%ld", &(irc->_priv->waiting_whois->idle_sec),
			&(irc->_priv->waiting_whois->signon));
}


static void
parse_chan_modes (GIrcClient * irc,
				  const gchar * data)
{
	GIrcChannel *channel;
	gchar *target = NULL,
	 *modes_str = NULL;

	sscanf (data, "%as %a[^\r\n]", &target, &modes_str);

	channel = g_hash_table_lookup (irc->_priv->targets, target);
	g_free (target);

	if (modes_str != NULL)
	{
		gchar **modesv = NULL;
		guint modes = 0;

		modesv = g_strsplit (modes_str, " ", -1);

		if (modesv[1] == NULL || modesv[1][0] == '\0')
			modes = parse_channel_modes (channel, modes_str);
		else
			modes = parse_channel_modes_with_args (irc, channel, NULL, modesv, FALSE);

		g_free (modes_str);

		_girc_client_emit_channel_changed (irc, channel, NULL, GIRC_CHANNEL_INFO_MODES,
										   NULL, FALSE);

		g_strfreev (modesv);
	}
}


static void
parse_chan_info (GIrcClient * irc,
				 const gchar * data)
{
	gchar *target_str = NULL;
	gulong creation_time = 0;

	sscanf (data, "%as %lu", &target_str, &creation_time);

	if (target_str != NULL && creation_time != 0)
	{
		GIrcChannel *channel;

		channel = g_hash_table_lookup (irc->_priv->targets, target_str);

		if (channel != NULL)
		{
			channel->ctime = (time_t) creation_time;
		}
	}

	g_free (target_str);
}


static void
parse_topic (GIrcClient * irc,
			 const gchar * data)
{
	gchar *target_str = NULL,
	 *topic = NULL;

	sscanf (data, "%as :%a[^\r\n]", &target_str, &topic);

	if (target_str != NULL)
	{
		GIrcChannel *channel = g_hash_table_lookup (irc->_priv->targets, target_str);

		g_free (target_str);

		if (channel != NULL)
		{
			g_free (channel->topic);
			channel->topic = g_strdup (topic);
		}

		_girc_client_emit_channel_changed (irc, channel, NULL, GIRC_CHANNEL_INFO_TOPIC,
										   NULL, FALSE);
	}

	g_free (topic);
}


static void
parse_chan_list_body (GIrcClient * irc,
					  const gchar * data)
{
	GIrcListItem *item;
	gchar *channel = NULL,
	 *topic = NULL;

	item = girc_list_item_new ();

	sscanf (data, "%as %u :%as", &channel, &(item->users), &topic);

	item->channel = g_strdup (channel);
	g_free (channel);

	item->topic = g_strdup (topic);
	g_free (topic);

	irc->_priv->chan_list = g_slist_append (irc->_priv->chan_list, item);
}


static void
parse_names_item (GIrcClient * irc,
				  const gchar * data)
{
	gchar *channel = NULL,
	 *users_group = NULL;
	gint i;

	sscanf (data, "%*c %as :%a[^\r\n]", &channel, &users_group);

	if (channel != NULL)
	{
		GIrcNamesItem *item = NULL;
		gchar **users;

		item = g_hash_table_lookup (irc->_priv->names_reply, channel);

		if (item == NULL)
		{
			item = girc_names_item_new ();

			item->channel = g_strdup (channel);
			g_hash_table_insert (irc->_priv->names_reply, item->channel, item);
		}

		users = g_strsplit (users_group, " ", -1);

		for (i = 0; users[i] != NULL; i++)
		{
			item->users = g_slist_prepend (item->users, users[i]);
		}

		g_free (users);
	}

	g_free (channel);
	g_free (users_group);
}


static void
parse_in_banlist (GIrcClient * irc,
				  const gchar * data)
{
	GIrcChannelMask *banmask = NULL;
	gchar *mask = NULL,
	 *server = NULL;
	time_t set_on_time = 0;

	sscanf (data, "%*s %as %as %lu", &mask, &server, &set_on_time);

	if (mask == NULL)
		return;

	banmask = girc_channel_mask_new ();

	banmask->mask = g_strdup (mask);
	g_free (mask);

	banmask->server = g_strdup (server);
	g_free (server);

	banmask->set_on_time = set_on_time;

	irc->_priv->waiting_banlist = g_slist_append (irc->_priv->waiting_banlist, banmask);
}


static void
parse_in_banlist_end (GIrcClient * irc,
					  const gchar * data)
{
	GIrcChannel *channel;
	gchar *name = NULL;

	sscanf (data, "%as :%*s", &name);

	if (name == NULL)
		return;

	channel = g_hash_table_lookup (irc->_priv->targets, name);
	g_free (name);

	if (channel != NULL)
	{
		girc_g_slist_deep_free (channel->bans, (GFreeFunc) girc_channel_mask_free);

		channel->bans = irc->_priv->waiting_banlist;

		_girc_client_emit_channel_changed (irc, channel, NULL, GIRC_CHANNEL_INFO_BAN,
										   NULL, FALSE);
	}
	else
	{
		girc_g_slist_deep_free (irc->_priv->waiting_banlist,
								(GFreeFunc) girc_channel_mask_free);
	}

	irc->_priv->waiting_banlist = NULL;
}


static void
parse_in_who (GIrcClient * irc,
			  const gchar * data)
{
	GIrcUser *user;
	gchar *username = NULL,
	 *hostname = NULL,
	 *server = NULL,
	 *nick = NULL,
	 *realname = NULL,
	 *userhost;
	gushort hopcount;

	sscanf (data, "%*s %as %as %as %as %*s :%hu %a[^\n\r]", &username, &hostname, &server,
			&nick, &hopcount, &realname);

	userhost = g_strdup_printf ("%s!%s@%s", nick, username, hostname);

	g_free (nick);
	g_free (username);
	g_free (hostname);

	user = find_or_insert_user_from_prefix (irc, userhost);

	if (user->server == NULL)
		user->server = g_strdup (server);

	g_free (server);

	user->hopcount = hopcount;

	if (user->realname == NULL)
		user->realname = g_strdup (realname);

	g_free (realname);
}


static void
parse_in_msg_serv_msg (GIrcClient * irc,
					   const gchar * prefix,
					   const gchar * data)
{
	/* Numeric reply */
	if (data[0] >= '0' && data[0] <= '9')
	{
		guint num_reply = 0;
		gchar *rest_of_data = NULL;

		/* Get the numeric reply & separate the actual data (skip over our
		   nick) */
		sscanf (data, "%d %*s %a[^\r\n]", &num_reply, &rest_of_data);

		switch (num_reply)
		{
			/* Server Info Lines */
		case GIRC_REPLY_MYINFO:
			parse_in_server_myinfo (irc, rest_of_data);
			break;

			/* MOTD */
		case GIRC_REPLY_MOTD_START:
			irc->_priv->waiting_motd = girc_motd_new ();
			irc->_priv->waiting_motd->server = g_strdup (prefix);
			break;
		case GIRC_REPLY_MOTD_BODY:
			{
				gchar *tmp;

				if (irc->_priv->waiting_motd == NULL)
				{
					irc->_priv->waiting_motd = girc_motd_new ();
					irc->_priv->waiting_motd->server = g_strdup (prefix);
				}

				if (irc->_priv->waiting_motd->motd != NULL)
				{
					tmp = g_strconcat (irc->_priv->waiting_motd->motd, "\n",
									   rest_of_data, NULL);

					g_free (irc->_priv->waiting_motd->motd);

					irc->_priv->waiting_motd->motd = tmp;
				}
				else
				{
					irc->_priv->waiting_motd->motd = g_strdup (rest_of_data);
				}

				if (g_ascii_strcasecmp (prefix, irc->_priv->vars->servername) == 0)
				{
					if (irc->_priv->vars->motd != NULL)
						g_free (irc->_priv->vars->motd);

					irc->_priv->vars->motd = g_strdup (irc->_priv->waiting_motd->motd);
				}
			}
			break;
		case GIRC_REPLY_MOTD_END:
			_girc_client_emit_motd_recv (irc);
			girc_motd_free (irc->_priv->waiting_motd);
			irc->_priv->waiting_motd = NULL;
			break;

			/* WHOIS command output */
		case GIRC_REPLY_WHOIS_USER:
			parse_whois_user (irc, rest_of_data);
			break;
		case GIRC_REPLY_WHOIS_CHANNELS_ON:
			parse_whois_channels_on (irc, rest_of_data);
			break;
		case GIRC_REPLY_WHOIS_SERVER:
			parse_whois_server (irc, rest_of_data);
			break;
		case GIRC_REPLY_WHOIS_IS_IDLE:
			parse_whois_idle (irc, rest_of_data);
			break;
		case GIRC_REPLY_WHOIS_IS_OPER:
			g_return_if_fail (GIRC_IS_USER (irc->_priv->waiting_whois));
			irc->_priv->waiting_whois->is_ircop = TRUE;
			break;
		case GIRC_REPLY_WHOIS_END:
			_girc_client_emit_whois_recv (irc);
			break;

			/* MODE command output */
		case GIRC_REPLY_CHAN_MODES:
			parse_chan_modes (irc, rest_of_data);
			break;
		case GIRC_REPLY_CHAN_INFO:
			parse_chan_info (irc, rest_of_data);
			break;
		case GIRC_REPLY_CHAN_INFO2:
			break;

			/* Channel Topics */
		case GIRC_REPLY_TOPIC:
			parse_topic (irc, rest_of_data);
			break;
		case GIRC_REPLY_NO_TOPIC_IS_SET:
			/* Do nothing :-) */
			break;

			/* LIST command output */
		case GIRC_REPLY_LIST_START:
			break;
		case GIRC_REPLY_LIST_BODY:
			parse_chan_list_body (irc, rest_of_data);
			break;
		case GIRC_REPLY_LIST_END:
			_girc_client_emit_list_recv (irc);
			break;

			/* NAMES command output */
		case GIRC_REPLY_NAME:
			parse_names_item (irc, rest_of_data);
			break;
		case GIRC_REPLY_NAMES_END:
			_girc_client_emit_names_recv (irc);
			break;

		case GIRC_REPLY_BANLIST:
			parse_in_banlist (irc, rest_of_data);
			break;
		case GIRC_REPLY_BANLIST_END:
			parse_in_banlist_end (irc, rest_of_data);
			break;

		case GIRC_REPLY_WHO:
			parse_in_who (irc, rest_of_data);
			break;
		case GIRC_REPLY_WHO_END:
			break;

		default:
			if (num_reply > 400 && rest_of_data != NULL)
			{
				_girc_client_emit_error_recv (irc, num_reply, rest_of_data);
			}
			else
			{
				_girc_client_emit_msg_recv (irc, num_reply, rest_of_data);
			}
			break;
		}

		g_free (rest_of_data);
	}
	/* The server replying to one of our pings */
	else if (g_ascii_strncasecmp (GIRC_COMMAND_PONG, data, 4) == 0)
	{
		GTimeVal sent,
		  current;

		/* If it's a reply to an auto-lag-detect ping, update things
		   appropriately. That's the only PONG from a server we worry about. */
		if (sscanf (data, "%*s :" LAG_PING_FORMAT, &(sent.tv_sec), &(sent.tv_usec)) == 2)
		{
			g_get_current_time (&current);
			irc->_priv->lag.tv_sec = current.tv_sec - sent.tv_sec;
			irc->_priv->lag.tv_usec = current.tv_usec - sent.tv_usec;
		}
	}
}


static void
parse_in_msg (GIrcClient * irc,
			  const gchar * data)
{
	gchar *prefix = NULL,
	 *msg = NULL;

	sscanf (data, "%as %a[^\n]", &prefix, &msg);

	if (prefix == NULL)
		return;

	/* If the prefix != a userhost */
	if (sscanf (prefix, "%*[^!]!%*[^@]@%*s") != 0)
	{
		parse_in_msg_serv_msg (irc, prefix, msg);
	}
	/* Otherwise, it's a user who's sending this msg. */
	else
	{
		parse_in_msg_user_msg (irc, prefix, msg);
	}

	g_free (prefix);
	g_free (msg);
}


static void
parse_in_startup (GIrcClient * irc,
				  const gchar * data)
{
	gint response_num;
	gsize len;
	gchar *name = NULL,
	 *text,
	 *nick = NULL,
	 *msg2 = NULL;
	const gchar *cmd,
	 *msg;

	sscanf (data, ":%as %n", &name, &len);
	cmd = data + len;

	sscanf (cmd, "%d %n", &response_num, &len);
	msg = cmd + len;

	if (irc->_priv->vars->servername == NULL)
		irc->_priv->vars->servername = g_strdup (name);

	g_free (name);

	/* This response will be to our "NICK" cmd that we send after setting the nick. */
	if (response_num == GIRC_SERVER_ERROR_NO_NICKNAME_GIVEN)
	{
		/* The nick wasn't accepted. msg[0] would be the first char of the nick
		   if it was. So, what we do is this:
		   - If the nick is < (max nick len), append '_'s until we find one that
		   works,
		   - Otherwise, start at the end, and chug through it backwards,
		   replacing letters with '_' until we find one that works. */
		if (msg[0] == ':' || msg[0] == ' ')
		{
			gchar *old_nick;

			if (irc->_priv->my_user->nick == NULL)
				irc->_priv->my_user->nick = g_strdup (irc->_priv->preferred_nick);

			old_nick = irc->_priv->my_user->nick;
			irc->_priv->my_user->nick = g_strconcat (old_nick, "_", NULL);
			g_free (old_nick);

			girc_client_set_nick (irc, irc->_priv->my_user->nick);
		}
		/* The nickname was accepted */
		else
		{
			text = g_strdup_printf ("%s %s %d %s :%s", GIRC_COMMAND_USER,
									irc->_priv->my_user->username,
									irc->_priv->modes,
									(irc->_priv->my_user->hostname != NULL ?
									 irc->_priv->my_user->hostname : "*"),
									irc->_priv->my_user->realname);
			girc_client_send_raw (irc, text);
			g_free (text);
		}
	}
	/* These errors would happen only if the USER cmd failed, which is sick. */
	else if (response_num == GIRC_SERVER_ERROR_NOT_ENOUGH_PARAMS
			 || response_num == GIRC_SERVER_ERROR_COULD_NOT_REGISTER)
	{
		irc->_priv->status = GIRC_CLIENT_ERROR_INVALID_USER;

		_girc_client_emit_connect_complete (irc, GIRC_CLIENT_ERROR_INVALID_USER);
	}
	/* Success! */
	else if (response_num == GIRC_REPLY_WELCOME)
	{
		sscanf (msg, "%as :%a[^\r\n]", &nick, &msg2);

		update_my_nick (irc, nick);

		g_free (nick);

		_girc_client_emit_connect_complete (irc, GIRC_CLIENT_CONNECTED);
		_girc_client_emit_msg_recv (irc, GIRC_REPLY_WELCOME, msg2);

		g_free (msg2);

		irc->_priv->status = GIRC_CLIENT_CONNECTED;
	}
	/* We don't care about anything else at this point. */
}


static void
parse_in_notice (GIrcClient * irc,
				 const gchar * data)
{
	gchar *notice_data = NULL,
	 *msg = NULL;

	if (data[0] == ':')
	{
		msg = g_strdup (data + 1);
	}
	else
	{
		gchar *tmp = NULL;

		sscanf (data, "%as %a[^\r\n]", &notice_data, &tmp);

		if (tmp != NULL && tmp[0] == ':')
		{
			msg = g_strdup (tmp + 1);
		}
		else
		{
			msg = g_strdup (tmp);
		}

		g_free (tmp);
	}

	_girc_client_emit_notice_recv (irc, NULL, msg, notice_data);

	g_free (notice_data);
	g_free (msg);
}


/* ************************ *
 *  HOUSEKEEPING FUNCTIONS  *
 * ************************ */


static void
parser_init (void)
{
	guint i;

	if (commands == NULL)
	{
		/* Commands */
		const GIrcCommand command_array[] = {
			/* Startup Commands */
			{"PASS", NULL},
			{"USER", NULL},
			{"SERVICE", NULL},

			/* Channel commands */
			{"TOPIC", parse_incoming_topic},
			{"JOIN", parse_incoming_join},
			{"PART", parse_incoming_part},
			{"KICK", parse_incoming_kick},
			{"MODE", parse_incoming_mode},
			{"PRIVMSG", parse_incoming_privmsg},
			{"NOTICE", parse_incoming_notice},

			/* IRCop commands */
			{"OPER", NULL},
			{"SQUIT", NULL},
			{"CONNECT", NULL},

			/* Server Commands */
			{"NICK", parse_incoming_nick},
			{"VERSION", NULL},
			{"STATS", NULL},
			{"LINKS", NULL},
			{"QUIT", parse_incoming_quit},

			/* Misc Commands */
			{"LIST", NULL},
			{"NAMES", NULL},
			{"LUSERS", NULL},
			{"TIME", NULL},
			{"TRACE", NULL},
			{"WHO", NULL},
			{"WHOX", NULL},
			{"WHOIS", NULL}
		};

		commands = g_hash_table_new (girc_ascii_strcasehash, girc_ascii_strcaseequal);

		for (i = 0; i < G_N_ELEMENTS (command_array); i++)
		{
			g_hash_table_insert (commands, (gpointer) command_array[i].name,
								 (gpointer) command_array[i].parse_incoming);
		}
	}

	if (server_variables == NULL)
	{
		server_variables = g_hash_table_new (g_str_hash, g_str_equal);

		for (i = 0; i < GIRC_VARIABLE_LAST; i++)
		{
			g_hash_table_insert (server_variables, (gpointer) variable_strings[i],
								 GUINT_TO_POINTER (i));
		}
	}
}


/* **************** *
 *  LIB-PUBLIC API  *
 * **************** */

void
_girc_parser_parse_incoming (GIrcClient * irc,
							 gchar * data)
{
	gchar **lines = NULL;
	gchar *buffer,
	 *text;
	gsize line_len = 0;
	gint i;

	parser_init ();

	/* Split up the data into lines */
	lines = g_strsplit (data, "\n", -1);

	/* Free the forcibly NUL-terminated data, we don't need it anymore */
	g_free (data);

	for (i = 0; lines[i] != NULL && lines[i][0] != '\0'; i++)
	{
		line_len = strlen (lines[i]);
		/*
		 * We have a partial line in the buffer and this is the first line
		 * in the incoming data
		 */
		if (irc->_priv->buffer != NULL && i == 0)
		{
			buffer = g_strconcat (irc->_priv->buffer, lines[0], NULL);
			/* Free the buffer, we don't need it anymore (right now) */
			g_free (irc->_priv->buffer);
			irc->_priv->buffer = NULL;

			if (buffer[0] == ':')
			{
				if (irc->_priv->status == GIRC_CLIENT_LOGGING_IN)
				{
					parse_in_startup (irc, buffer);
				}
				else
				{
					parse_in_msg (irc, buffer + 1);
				}
			}
			else if (g_ascii_strncasecmp (buffer, GIRC_COMMAND_PING, 4) == 0)
			{
				text = g_strconcat (GIRC_COMMAND_PONG, buffer + 4, NULL);
				girc_client_send_raw (irc, text);
				g_free (text);
			}
			else if (g_ascii_strncasecmp (buffer, GIRC_COMMAND_ERROR, 5) == 0)
			{
				_girc_client_emit_error_recv (irc, 0, buffer + 6);
			}
			else if (g_ascii_strncasecmp (buffer, GIRC_COMMAND_NOTICE, 6) == 0)
			{
				parse_in_notice (irc, buffer + 7);
			}

			g_free (buffer);
		}
		else
		{
			/* If this line is a full line */
			if (lines[i][line_len - 1] == '\r' || lines[i][line_len - 1] == '\n')
			{
				buffer = lines[i];

				if (buffer[0] == ':')
				{
					if (irc->_priv->status == GIRC_CLIENT_LOGGING_IN)
					{
						parse_in_startup (irc, buffer);
					}
					else
					{
						parse_in_msg (irc, buffer + 1);
					}
				}
				else if (g_ascii_strncasecmp (buffer, GIRC_COMMAND_PING, 4) == 0)
				{
					text = g_strconcat (GIRC_COMMAND_PONG, buffer + 4, NULL);
					girc_client_send_raw (irc, text);
					g_free (text);
				}
				else if (g_ascii_strncasecmp (buffer, GIRC_COMMAND_NOTICE, 6) == 0)
				{
					parse_in_notice (irc, buffer + 6);
				}
			}
			else
			{
				irc->_priv->buffer = g_strdup (lines[i]);
			}
		}
	}

	g_strfreev (lines);
}


GIrcChannelUser *
_girc_parser_parse_channel_user (GIrcClient * irc,
								 const gchar * data)
{
	GIrcChannelUser *chan_user = NULL;
	GIrcChannelUserModeFlags modes = GIRC_CHANNEL_USER_MODE_NONE;
	GIrcUser *user = NULL;
	gchar *nick = NULL,
	 *userhost;

	if (data == NULL || data[0] == '\0')
		return NULL;

	parse_channel_on (irc, data, &nick, &modes);

	if (nick == NULL)
		return NULL;

	chan_user = girc_channel_user_new ();

	chan_user->modes = modes;

	userhost = g_strdup_printf ("%s!*@*", nick);

	user = g_hash_table_lookup (irc->_priv->users, userhost);

	if (user == NULL)
	{
		user = girc_user_new ();
		user->nick = nick;

		g_hash_table_insert (irc->_priv->users, userhost, user);
	}
	else
	{
		g_free (nick);
	}

	chan_user->user = girc_user_ref (user);

	return chan_user;
}
