/*
 * LibGIrcClient: libgircclient/girc-type-utils.h
 *
 * Copyright (c) 2002 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIRC_TYPE_UTILS_H__
#define __GIRC_TYPE_UTILS_H__


#include <glib-object.h>

/**
 * GIRC_TYPE_CHECK_BOXED_TYPE:
 * @boxed: the pointer to check.
 * @type: the expected #GType.
 *
 * A GType-style type-checking macro for boxed types with GType first members.
 **/
#define GIRC_TYPE_CHECK_BOXED_TYPE(boxed,type)			(_GIRC_TYPE_CBT ((boxed), (type)))

/**
 * GIRC_TYPE_CHECK_BOXED_CAST:
 * @boxed: the pointer to check.
 * @type: the expected #GType.
 * @c_type: the desired typecast.
 *
 * A GType-style type-casting macro for boxed types with GType first members.
 **/
#define GIRC_TYPE_CHECK_BOXED_CAST(boxed,type,c_type)	(_GIRC_TYPE_CBC ((boxed), (type), c_type))


/**
 * GIRC_TYPE_CHECK_BOXED_2UNION_TYPE:
 * @boxed: the pointer to check.
 * @type1: the first expected #GType.
 * @type2: the second expected #GType.
 *
 * A GType-style type-checking macro for unions of two registered boxed types with GType first members.
 **/
#define GIRC_TYPE_CHECK_BOXED_2UNION_TYPE(boxed,type1,type2)	(_GIRC_TYPE_CB2UT ((boxed), (type1), (type2)))

/**
 * GIRC_TYPE_CHECK_BOXED_2UNION_CAST
 * @boxed: the pointer to check.
 * @union_type: the #GType of the union structure.
 * @type1: the first possible #GType.
 * @type2: the second possible #GType.
 * @c_type: the desired typecast.
 *
 * A GType-style type-casting macro for unions of two registered boxed types with GType first members.
 **/
#define GIRC_TYPE_CHECK_BOXED_2UNION_CAST(boxed,union_type,type1,type2,c_type) (_GIRC_TYPE_CB2UC ((boxed), (union_type), (type1), (type2), c_type))


#define _GIRC_TYPE_CBT(bp,gt)			(bp != NULL && ((GTypeClass *)(bp))->g_type == (gt))
#define _GIRC_TYPE_CB2UT(bp,gt1,gt2)	(bp != NULL && (((GTypeClass *)(bp))->g_type == (gt1) || ((GTypeClass *)(bp))->g_type == (gt2)))

#ifdef  __GNUC__
#	define _GIRC_TYPE_CBC(bp,gt,ct)	(G_GNUC_EXTENSION ({ \
	GTypeClass *__klass = (GTypeClass*) bp; \
	if (!__klass) \
		g_log ("GIrcClient", G_LOG_LEVEL_CRITICAL, "invalid cast from `(null)' to `%s'", \
			   g_type_name ((gt))); \
	else if (__klass->g_type != (gt)) \
		g_log ("GIrcClient", G_LOG_LEVEL_CRITICAL, "invalid cast from `%s' to `%s'", \
			   g_type_name (__klass->g_type), g_type_name ((gt))); \
	((ct*) bp); \
}))
#	define _GIRC_TYPE_CB2UC(bp,ugt,gt1,gt2,ct)	(G_GNUC_EXTENSION ({ \
	GTypeClass *__klass = (GTypeClass*) bp; \
	if (!__klass) \
		g_log ("GIrcClient", G_LOG_LEVEL_CRITICAL, "invalid cast from `(null)' to `%s'", \
			   g_type_name ((ugt))); \
	else if (!(__klass->g_type == (gt1) || __klass->g_type == (gt2))) \
		g_log ("GIrcClient", G_LOG_LEVEL_CRITICAL, "invalid cast from `%s' to `%s'", \
			   g_type_name (__klass->g_type), g_type_name ((ugt))); \
	((ct*) (bp)); \
}))
#else /* ! __GNUC__ */
#	define _GIRC_TYPE_CBC(bp,gt,ct)				((ct*) (bp))
#	define _GIRC_TYPE_CB2UC(bp,ugt,gt1,gt2,ct)	((ct*) (bp))
#endif /* __GNUC__ */


#endif /* __GIRC_TYPE_UTILS_H__ */
