/*
 * LibGIrcClient: libgircclient/girc-client.h
 *
 * Copyright (c) 2002 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIRC_CLIENT_H__
#define __GIRC_CLIENT_H__


#include <glib.h>
#include <glib-object.h>
#include <libgtcpsocket/gtcp-connection.h>

#include "girc-types.h"
#include "girc-user.h"
#include "girc-utils.h"
#include "girc-type-builtins.h"


#define GIRC_TYPE_CLIENT			(girc_client_get_type ())
#define GIRC_CLIENT(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GIRC_TYPE_CLIENT, GIrcClient))
#define GIRC_CLIENT_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GIRC_TYPE_CLIENT, GIrcClientClass))
#define GIRC_IS_CLIENT(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIRC_TYPE_CLIENT))
#define GIRC_IS_CLIENT_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GIRC_TYPE_CLIENT))
#define GIRC_CLIENT_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GIRC_TYPE_CLIENT, GIrcClientClass))


#define GIRC_MAX_NICK_LEN 9


typedef struct _GIrcClient GIrcClient;
typedef struct _GIrcClientClass GIrcClientClass;
typedef struct _GIrcClientPrivate GIrcClientPrivate;


struct _GIrcClient
{
	/*< private >*/
	GTcpConnection parent;

	GIrcClientPrivate *_priv;
};

struct _GIrcClientClass
{
	/*< private >*/
	GTcpConnectionClass parent_class;

	/* New connection-level signals
	void (*irc_status_changed) (GIrcClient * irc,
								GIrcClientStatus status);
	void (*irc_quit) (GIrcClient * irc,
					  gboolean requested); */

	/*< public >*/
	/* Signals */
	/* Connection-level -- use these in favor of 
	   GTcpConnection's "connect-done & "closed"
	   signals. */
	void (*connect_complete) (GIrcClient * irc,
							  GIrcClientStatus status,
							  const gchar * server,
							  const gchar * nick);
	void (*quit) (GIrcClient * irc,
				  gboolean requested);

	void (*nick_changed) (GIrcClient * irc,
						  const gchar * new_nick);

	void (*ctcp_recv) (GIrcClient * irc,
					   GIrcUser * user,
					   GIrcCtcpMessageType type,
					   const gchar * data);

	/* Queries & Channels */
	void (*target_opened) (GIrcClient * irc,
						   GIrcTarget * target);
	void (*target_closed) (GIrcClient * irc,
						   GIrcTarget * target,
						   const gchar * reason);
	void (*target_recv) (GIrcClient * irc,
						 GIrcTarget * target,
						 const GValue * sender,
						 GIrcUserMessageType type,
						 const gchar * msg);

	/* Queries */
	void (*query_changed) (GIrcClient * irc,
						   GIrcQuery * query,
						   GIrcQueryChangedType changed,
						   const gchar * msg);

	/* Channels */
	void (*channel_changed) (GIrcClient * irc,
							 GIrcChannel * channel,
							 const GValue * changer,
							 GIrcChannelInfoType what_changed,
							 const GValue * value,
							 gboolean is_on);
	void (*channel_user_changed) (GIrcClient * irc,
								  GIrcChannel * channel,
								  GIrcChannelUser * user,
								  GIrcChannelUserChangedType changed,
								  const GValue * changer,
								  const GValue * value);

	/* In response to user requests */
	void (*whois_recv) (GIrcClient * irc,
						GIrcUser * user);
	void (*list_recv) (GIrcClient * irc,
					   GSList * list);
	void (*names_recv) (GIrcClient * irc,
						GSList * names);
	void (*notify_recv) (GIrcClient * irc,
						 GIrcUser * user);
	void (*motd_recv) (GIrcClient * irc,
					   GIrcMotd * motd);
	void (*mode_recv) (GIrcClient * irc,
					   guint modes);
	void (*notice_recv) (GIrcClient * irc,
						 const gchar * sender,
						 const gchar * msg,
						 const gchar * data);

	/* Generic server messages */
	void (*msg_recv) (GIrcClient * irc,
					  GIrcReplyType type,
					  const gchar * msg);
	void (*error_recv) (GIrcClient * irc,
						GIrcServerErrorType error,
						const gchar * msg);

	/*< private >*/
	/* For ABI-compatible expansion */
	void (*__girc_reserved_1) (void);
	void (*__girc_reserved_2) (void);
	void (*__girc_reserved_3) (void);
	void (*__girc_reserved_4) (void);
 };


GType girc_client_get_type (void);

GObject *girc_client_new (const gchar * nick,
						  const gchar * realname,
						  const gchar * username,
						  const gchar * server,
						  guint port,
						  const gchar * pass,
						  const gchar * outgoing_encoding);

void girc_client_open (GIrcClient * irc);
void girc_client_quit (GIrcClient * irc,
					   const gchar * reason);

void girc_client_send_raw (GIrcClient * irc,
						   const gchar * str);

gboolean girc_client_set_nick (GIrcClient * irc,
							   const gchar * nick);
G_CONST_RETURN gchar * girc_client_get_nick (GIrcClient * irc);

void girc_client_set_encoding (GIrcClient * irc,
							   const gchar * encoding);
G_CONST_RETURN gchar * girc_client_get_encoding (GIrcClient * irc);

void girc_client_set_away (GIrcClient * irc,
						   gboolean away);
gboolean girc_client_get_away (GIrcClient * irc);

G_CONST_RETURN gchar * girc_client_get_motd (GIrcClient * irc);

G_CONST_RETURN GIrcTarget *girc_client_open_query (GIrcClient * irc,
												   const gchar * nick);

G_CONST_RETURN GIrcTarget *girc_client_get_target (GIrcClient * irc,
												   const gchar * handle);
void girc_client_close_target (GIrcClient * irc,
							   GIrcTarget * target);

gboolean girc_client_is_open (GIrcClient * irc);

gboolean girc_client_is_valid_nick (GIrcClient * irc,
									const gchar * nick);

void girc_client_add_user_notify (GIrcClient * irc,
								  const gchar * nick);
void girc_client_remove_user_notify (GIrcClient * irc,
									 const gchar * nick);

GIrcUser *girc_client_get_user_by_nick (GIrcClient * irc,
										const gchar * nick);

gboolean girc_client_has_channel_mode (GIrcClient * irc,
									   gchar mode_char);


#endif /* __GIRC_CLIENT_H__ */
