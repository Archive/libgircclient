/*
 * LibGIrcClient: libgircclient/girc-channel.h
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIRC_USER_H__
#define __GIRC_USER_H__


#include <glib-object.h>
#include "girc-type-utils.h"


#define GIRC_TYPE_USER		(girc_user_get_type ())
#define GIRC_USER(boxed)	(GIRC_TYPE_CHECK_BOXED_CAST (boxed, GIRC_TYPE_QUERY, GIrcUser))
#define GIRC_IS_USER(boxed)	(GIRC_TYPE_CHECK_BOXED_TYPE (boxed, GIRC_TYPE_USER))


typedef struct _GIrcUser GIrcUser;

struct _GIrcUser
{
	/* < private > */
	GType type;
	guint ref;

	/* < public > */
	gchar *nick;
	gchar *username;
	gchar *hostname;
	gchar *realname;

	gchar *ip_address;
	gchar *server;
	gchar *server_comment;

	GHashTable *channels;

	time_t idle_sec;
	time_t signon;

	guint8 hopcount;

	gboolean is_ircop:1;

	guint modes:6;
};

GType girc_user_get_type (void);

GIrcUser *girc_user_new (void);
GIrcUser *girc_user_new_from_string (const gchar * userhost);
GIrcUser *girc_user_ref (GIrcUser * user);
void girc_user_unref (GIrcUser * user);

gchar *girc_user_to_string (const GIrcUser * user);
gchar *girc_user_to_userhost (const GIrcUser * user);

guint girc_userhost_hash (gconstpointer userhost);
gboolean girc_userhost_equal (gconstpointer key1,
							  gconstpointer key2);

#endif /* __GIRC_USER_H__ */
