/*
 * LibGIrcClient: libgircclient/girc-user.c
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/* GBoxed type handling stuff for GIrcUser */

#include "girc-channel.h"

#include <stdio.h>


GType
girc_user_get_type (void)
{
	static GType boxed_type = 0;

	if (boxed_type == 0)
	{
		boxed_type = g_boxed_type_register_static ("GIrcUser",
												   (GBoxedCopyFunc) girc_user_ref,
												   (GBoxedFreeFunc) girc_user_unref);
	}

	return boxed_type;
}


/**
 * girc_user_new: 
 *
 * Creates a new #GIrcUser-struct with a reference count of one, initialized
 * to %NULL.
 *
 * Returns: a new #GIrcUser-struct.
 *
 * Since 1.0
 **/
GIrcUser *
girc_user_new (void)
{
	GIrcUser *user = NULL;

	user = g_new0 (GIrcUser, 1);

	user->type = GIRC_TYPE_USER;
	user->ref = 1;
	user->channels = g_hash_table_new_full (girc_userhost_hash, girc_userhost_equal, NULL,
											(GDestroyNotify) girc_user_channel_on_free);

	user->nick = NULL;
	user->username = NULL;
	user->hostname = NULL;
	user->realname = NULL;
	user->ip_address = NULL;
	user->server = NULL;
	user->server_comment = NULL;

	return user;
}


/**
 * girc_user_new_from_string: 
 * @userhost: a string in nick!username@@host format.
 *
 * Creates a new #GIrcUser-struct with a reference count of one, initialized
 * to the data in @userhost. If @userhost is invalid, %NULL will be returned.
 *
 * Returns: a new #GIrcUser-struct or %NULL.
 *
 * Since 1.0
 **/
GIrcUser *
girc_user_new_from_string (const gchar * userhost)
{
	GIrcUser *user;
	gchar *nickname = NULL,
	 *username = NULL,
	 *hostname = NULL;

	if (userhost == NULL || userhost[0] == '\0')
		return NULL;

	if (sscanf (userhost, "%a[^!]!%a[^@]@%as", &nickname, &username, &hostname) < 3)
		return NULL;

	user = girc_user_new ();

	user->nick = g_strdup (nickname);
	user->username = g_strdup (username);
	user->hostname = g_strdup (hostname);

	g_free (nickname);
	g_free (username);
	g_free (hostname);

	return user;
}


/**
 * girc_user_ref: 
 * @user: the user to reference.
 *
 * Increments the reference count of this #GIrcUser-struct by one. When the
 * reference count reaches zero, the #GIrcUser-struct will be freed. If @user
 * is not a valid #GIrcUser-struct, this function will return %NULL.
 *
 * Returns: a reference to @user, or %NULL.
 * 
 * Since 1.0
 **/
GIrcUser *
girc_user_ref (GIrcUser * user)
{
	g_return_val_if_fail (GIRC_IS_USER (user), NULL);

	user->ref++;

	return user;
}


/**
 * girc_user_unref: 
 * @user: the user to unref.
 *
 * Decrements the reference count of this #GIrcUser-struct by one. When the
 * reference count reaches zero, the #GIrcUser-struct will be freed.
 * 
 * Since 1.0
 **/
void
girc_user_unref (GIrcUser * user)
{
	g_return_if_fail (GIRC_IS_USER (user));

	user->ref--;

	if (user->ref == 0)
	{
		g_free (user->nick);
		g_free (user->username);
		g_free (user->realname);
		g_free (user->hostname);

		g_free (user->server);
		g_free (user->server_comment);
		g_free (user->ip_address);

		g_hash_table_destroy (user->channels);

		g_free (user);
	}
}


/**
 * girc_user_to_string: 
 * @user: The user.
 *
 * Creates a string in nick!user@@host format based on the contents of @user.
 * The returned string should be freed when no longer needed.
 *
 * Returns: the user in nick!user@@host format.
 * 
 * Since 1.0
 **/
gchar *
girc_user_to_string (const GIrcUser * user)
{
	const gchar *nickname,
	 *username,
	 *hostname;

	g_return_val_if_fail (GIRC_IS_USER (user), NULL);

	if (user->nick != NULL)
		nickname = user->nick;
	else
		nickname = "*";

	if (user->username != NULL)
		username = user->username;
	else
		username = "*";

	if (user->hostname != NULL)
		hostname = user->hostname;
	else
		hostname = "*";

	return g_strdup_printf ("%s!%s@%s", nickname, username, hostname);
}


/**
 * girc_user_to_userhost: 
 * @user: the user.
 *
 * Creates a string in user@@host format based on the contents of @user.
 * The returned string should be freed when no longer needed.
 *
 * Returns: the user in user@@host format.
 * 
 * Since 1.0
 **/
gchar *
girc_user_to_userhost (const GIrcUser * user)
{
	const gchar *username,
	 *hostname;

	g_return_val_if_fail (GIRC_IS_USER (user), NULL);

	if (user->username != NULL)
		username = user->username;
	else
		username = "*";

	if (user->hostname != NULL)
		hostname = user->hostname;
	else
		hostname = "*";

	return g_strdup_printf ("%s@%s", username, hostname);
}


/**
 * girc_userhost_hash: 
 * @userhost: the userhost string.
 *
 * Creates a hash value for the given string @userhost in "nick!user@@host" format.
 *
 * Returns: the hash value for @userhost.
 * 
 * Since 1.0
 **/
guint
girc_userhost_hash (gconstpointer userhost)
{
	const gchar *ptr = userhost;
	guint retval;

	if (userhost == NULL || *ptr == '*')
		return 0;

	retval = (guint) (guchar) g_ascii_tolower (*ptr);

	for (ptr += 1; *ptr != '!' && *ptr != '\0'; ptr++)
	{
		retval = (retval << 5) - retval + g_ascii_tolower (*ptr);
	}

	return retval;
}


/**
 * girc_userhost_equal:
 * @key1: the first userhost.
 * @key2: the second userhost.
 *
 * Tests the equality of two strings in "nick!user@@host" format.
 *
 * Returns: %TRUE if the strings are equivalent, %FALSE if they are not.
 *
 * Since 1.0
 **/
gboolean
girc_userhost_equal (gconstpointer key1,
					 gconstpointer key2)
{
	gchar *nick1 = NULL,
	 *user1 = NULL,
	 *host1 = NULL,
	 *nick2 = NULL,
	 *user2 = NULL,
	 *host2 = NULL;
	gboolean nick_equal = FALSE,
	  user_equal = FALSE,
	  host_equal = FALSE,
	  retval = FALSE;

	if (sscanf (key1, "%a[^!]!%a[^@]@%as", &nick1, &user1, &host1) !=
		sscanf (key2, "%a[^!]!%a[^@]@%as", &nick2, &user2, &host2))
	{
		retval = FALSE;
	}
	else
	{

		if (nick1 != NULL)
		{
			if (nick1[0] == '*' || (nick2 != NULL && nick2[0] == '*'))
				nick_equal = TRUE;
			else
				nick_equal = (g_ascii_strcasecmp (nick1, nick2) == 0);
		}
		else
		{
			nick_equal = (nick2 == NULL || nick2[0] == '*');
		}

		if (nick_equal)
		{
			if (user1 != NULL)
			{
				if (user1[0] == '*' || (user2 != NULL && user2[0] == '*'))
					user_equal = TRUE;
				else
					user_equal = (g_ascii_strcasecmp (user1, user2) == 0);
			}
			else
			{
				user_equal = (user2 == NULL || user2[0] == '*');
			}


			if (user_equal)
			{
				if (host1 != NULL)
				{
					if (host1[0] == '*' || (host2 != NULL && host2[0] == '*'))
						host_equal = TRUE;
					else
						host_equal = (g_ascii_strcasecmp (host1, host2) == 0);
				}
				else
				{
					host_equal = (host2 == NULL || host2[0] == '*');
				}

				retval = host_equal;
			}
			else
			{
				retval = FALSE;
			}
		}
		else
		{
			retval = FALSE;
		}
	}

	g_free (nick1);
	g_free (user1);
	g_free (host1);

	g_free (nick2);
	g_free (user2);
	g_free (host2);

	return retval;
}
