/*
 * LibGIrcClient: libgircclient/girc-query.h
 *
 * Copyright (c) 2002, 2003 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIRC_QUERY_H__
#define __GIRC_QUERY_H__


#include <glib.h>
#include <glib-object.h>

#include <time.h>

#include "girc-type-utils.h"
#include "girc-user.h"


#define GIRC_TYPE_QUERY			(girc_query_get_type ())
#define GIRC_QUERY(boxed)		(GIRC_TYPE_CHECK_BOXED_CAST (boxed, GIRC_TYPE_QUERY, GIrcQuery))
#define GIRC_IS_QUERY(boxed)	(GIRC_TYPE_CHECK_BOXED_TYPE (boxed, GIRC_TYPE_QUERY))


typedef enum
{
	GIRC_QUERY_CHANGED_NONE,
	GIRC_QUERY_CHANGED_NICK,
	GIRC_QUERY_CHANGED_QUIT
}
GIrcQueryChangedType;


typedef struct _GIrcQuery GIrcQuery;


struct _GIrcQuery
{
	/* < private > */
	GType type;
	guint ref;

	/* < public > */
	gchar *userhost;
	GIrcUser *user;
	time_t ctime;
};


GType girc_query_get_type (void);

GIrcQuery *girc_query_new (void);
GIrcQuery *girc_query_ref (GIrcQuery * query);
void girc_query_unref (GIrcQuery *query);


#endif /* __GIRC_QUERY_H__ */
