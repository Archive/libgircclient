<!-- ##### SECTION Title ##### -->
Channels

<!-- ##### SECTION Short_Description ##### -->
Chat channels

<!-- ##### SECTION Long_Description ##### -->
<para>
The data structures listed here pertain to IRC channels, their users, and
information about channel types &amp; modes.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### STRUCT GIrcChannel ##### -->
<para>
Data representing a channel the user is on.
</para>

@name: the name of the channel.
@creator: the user who created the channel.
@ctime: the time (in unix time() format) when the channel was created.
@topic: the channel's topic.
@my_user: the current users' #GIrcChannelUser-struct.
@limit: the channel limit (+l).
@key: the channel key (+k).
@bans: a list of strings for the channel's ban masks (+b).
@ban_excepts: a list of strings for the channel's ban exception masks (+e).
@invites: a list of strings for the channel's channel invite masks (+i).
@invite_excepts: a list of strings for the channel's  channel invite exceptions masks (+I).
@users: a list of #GIrcChannelUser-struct structures for each user on the channel.
@modes: the channel's mode bitmask.

<!-- ##### MACRO GIRC_CHANNEL ##### -->
<para>
A standard #GObject style typecasting macro for #GIrcChannel-struct structures.
</para>

@boxed: the pointer to be cast.


<!-- ##### MACRO GIRC_IS_CHANNEL ##### -->
<para>
A standard #GObject type-checking macro for #GIrcChannel-struct structures.
</para>

@boxed: the pointer to check.


<!-- ##### MACRO GIRC_TYPE_CHANNEL ##### -->
<para>
A standard #GBoxed type for #GIrcChannel-struct data.
</para>



<!-- ##### FUNCTION girc_channel_new ##### -->
<para>

</para>

@Returns: 


<!-- ##### FUNCTION girc_channel_ref ##### -->
<para>

</para>

@channel: 
@Returns: 


<!-- ##### FUNCTION girc_channel_unref ##### -->
<para>

</para>

@channel: 


<!-- ##### FUNCTION girc_channel_get_users ##### -->
<para>

</para>

@channel: 
@Returns: 


<!-- ##### STRUCT GIrcChannelUser ##### -->
<para>
The data structure representing a user on a channel.
</para>

@user: the user's basic information.
@modes: the channel modes set on this user.

<!-- ##### MACRO GIRC_CHANNEL_USER ##### -->
<para>
A standard #GObject style typecasting macro for #GIrcChannelUser-struct structures.
</para>

@boxed: the pointer to be cast.


<!-- ##### MACRO GIRC_IS_CHANNEL_USER ##### -->
<para>
A standard #GObject type-checking macro for #GIrcChannelUser-struct structures.
</para>

@boxed: the pointer to be checked.


<!-- ##### MACRO GIRC_TYPE_CHANNEL_USER ##### -->
<para>
A standard #GBoxed type macro for the #GIrcChannelUser boxed type.
</para>



<!-- ##### FUNCTION girc_channel_user_new ##### -->
<para>

</para>

@Returns: 


<!-- ##### FUNCTION girc_channel_user_ref ##### -->
<para>

</para>

@user: 
@Returns: 


<!-- ##### FUNCTION girc_channel_user_unref ##### -->
<para>

</para>

@user: 


<!-- ##### FUNCTION girc_channel_user_collate ##### -->
<para>

</para>

@user1: 
@user2: 
@encoding: 
@Returns: 


<!-- ##### STRUCT GIrcChannelMask ##### -->
<para>
A data structure representing the return data from a "MODE {channel} +b" command.
</para>

@mask: the usermask being banned.
@server: the server enforcing this ban.
@set_on_time: the time the ban was set.

<!-- ##### MACRO GIRC_CHANNEL_MASK ##### -->
<para>
A standard #GObject style typecasting macro for #GIrcChannelMask-struct structures.
</para>

@boxed: the pointer to be cast.


<!-- ##### MACRO GIRC_IS_CHANNEL_MASK ##### -->
<para>
A standard #GObject type-checking macro for #GIrcChannelMask-struct structures.
</para>

@boxed: the pointer to be checked.


<!-- ##### MACRO GIRC_TYPE_CHANNEL_MASK ##### -->
<para>
A standard #GBoxed type macro for the #GIrcChannelMask-struct boxed type.
</para>



<!-- ##### FUNCTION girc_channel_mask_new ##### -->
<para>

</para>

@Returns: 


<!-- ##### FUNCTION girc_channel_mask_dup ##### -->
<para>

</para>

@src: 
@Returns: 


<!-- ##### FUNCTION girc_channel_mask_free ##### -->
<para>

</para>

@mask: 


<!-- ##### STRUCT GIrcChannelUserMode ##### -->
<para>
A structure describing a channel user's mode.
</para>

@mode: the mode flag.
@mode_char: the character used in the MODE command.
@prefix_char: the prefix character used in channel lists.
@description: a description of the mode.

<!-- ##### MACRO GIRC_CHANNEL_USER_MODE ##### -->
<para>
A standard #GObject style typecasting macro for #GircChannelUserMode-struct
structures.
</para>

@boxed: the pointer to cast.


<!-- ##### MACRO GIRC_IS_CHANNEL_USER_MODE ##### -->
<para>
A standard #GObject type-checking macro for #GIrcChannelUserMode-struct structures.
</para>

@boxed: the pointer to check.


<!-- ##### MACRO GIRC_TYPE_CHANNEL_USER_MODE ##### -->
<para>
A standard GBoxed type macro for #GIrcChannelUserMode structures.
</para>



<!-- ##### ENUM GIrcChannelUserModeFlags ##### -->
<para>
An enumerated type representing a bitmask of a channel user's mode.
</para>

@GIRC_CHANNEL_USER_MODE_NONE: the user has no special permissions.
@GIRC_CHANNEL_USER_MODE_VOICE: the user can speak freely when the channel is moderated (+m).
@GIRC_CHANNEL_USER_MODE_HALFOPS: the user has operator permissions, but cannot override a full operator.
@GIRC_CHANNEL_USER_MODE_OPS: the user has operator permissions.
@GIRC_CHANNEL_USER_MODE_CREATOR: the user created the channel.

<!-- ##### ENUM GIrcChannelUserModeChar ##### -->
<para>
An enumerated type of characters used to represent channel user modes in the MODE command.
</para>

@GIRC_CHANNEL_USER_MODE_CHAR_NONE: 
@GIRC_CHANNEL_USER_MODE_CHAR_OPS: 
@GIRC_CHANNEL_USER_MODE_CHAR_HALFOPS: 
@GIRC_CHANNEL_USER_MODE_CHAR_VOICE: 
@GIRC_CHANNEL_USER_MODE_CHAR_CREATOR: 

<!-- ##### FUNCTION girc_channel_user_mode_new ##### -->
<para>

</para>

@mode_char: 
@prefix_char: 
@Returns: 


<!-- ##### FUNCTION girc_channel_user_mode_get_description ##### -->
<para>

</para>

@mode_char: 
@Returns: 


<!-- ##### FUNCTION girc_channel_user_mode_dup ##### -->
<para>

</para>

@src: 
@Returns: 


<!-- ##### FUNCTION girc_channel_user_mode_free ##### -->
<para>

</para>

@mode: 


<!-- ##### FUNCTION girc_channel_user_mode_flags_collate ##### -->
<para>

</para>

@modes1: 
@modes2: 
@Returns: 


<!-- ##### STRUCT GIrcChannelMode ##### -->
<para>
A structure of information about a particular channel mode.
</para>

@mode_char: The character used for this mode in the %MODE command
@mode: The flag corresponding to this mode
@arg_type: The set of arguments this mode takes
@description: A description of this mode

<!-- ##### MACRO GIRC_CHANNEL_MODE ##### -->
<para>
A standard #GObject style typecasting macro for #GIrcChannelMode-struct
structures.
</para>

@boxed: the pointer to be cast.


<!-- ##### MACRO GIRC_TYPE_CHANNEL_MODE ##### -->
<para>
The #GBoxed type of #GIrcChannelMode-struct.
</para>



<!-- ##### ENUM GIrcChannelModeFlags ##### -->
<para>
A bitmask of possible channel modes.
</para>

@GIRC_CHANNEL_MODE_NONE: the channel has no modes set.
@GIRC_CHANNEL_MODE_MODERATED: the channel is moderated.
@GIRC_CHANNEL_MODE_INVITE_ONLY: the channel is set invite-only.
@GIRC_CHANNEL_MODE_NOMSG: the channel does not allow messages from users outside.
@GIRC_CHANNEL_MODE_TOPIC_PROTECT: the channel only allows operators to change the topic.
@GIRC_CHANNEL_MODE_LIMIT: the channel has set a limit on joining users.
@GIRC_CHANNEL_MODE_KEY: the channel has a password.
@GIRC_CHANNEL_MODE_PRIVATE: the channel keeps it's users out of lists.
@GIRC_CHANNEL_MODE_SECRET: the channel is totally secret.
@GIRC_CHANNEL_MODE_QUIET: the channel does not print join/quit messages.
@GIRC_CHANNEL_MODE_ANONYMOUS: the channel hides all it's users' identities.
@GIRC_CHANNEL_MODE_REOP: the server should automatically re-op the last operator if ops is lost.

<!-- ##### ENUM GIrcChannelModeChar ##### -->
<para>
The characters used to signify particular channel modes.
</para>

@GIRC_CHANNEL_MODE_CHAR_MODERATED: 
@GIRC_CHANNEL_MODE_CHAR_INVITE_ONLY: 
@GIRC_CHANNEL_MODE_CHAR_NOMSG: 
@GIRC_CHANNEL_MODE_CHAR_TOPIC_PROTECT: 
@GIRC_CHANNEL_MODE_CHAR_LIMIT: 
@GIRC_CHANNEL_MODE_CHAR_KEY: 
@GIRC_CHANNEL_MODE_CHAR_PRIVATE: 
@GIRC_CHANNEL_MODE_CHAR_SECRET: 
@GIRC_CHANNEL_MODE_CHAR_QUIET: 
@GIRC_CHANNEL_MODE_CHAR_ANONYMOUS: 
@GIRC_CHANNEL_MODE_CHAR_REOP: 
@GIRC_CHANNEL_MODE_CHAR_BAN: 
@GIRC_CHANNEL_MODE_CHAR_BAN_EXCEPTIONS: 
@GIRC_CHANNEL_MODE_CHAR_INVITE: 
@GIRC_CHANNEL_MODE_CHAR_INVITE_EXCEPTIONS: 

<!-- ##### ENUM GIrcChannelModeArgumentType ##### -->
<para>
An enumerated type corresponding to the argument style a channel mode uses.
</para>

@GIRC_CHANNEL_MODE_ARGS_UNKNOWN: 
@GIRC_CHANNEL_MODE_ARGS_LIST_CHANGE_ONE_PARAM: 
@GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_PARAM: 
@GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_ONE_OR_NO_PARAM: 
@GIRC_CHANNEL_MODE_ARGS_SETTINGS_CHANGE_NO_PARAM: 

<!-- ##### FUNCTION girc_channel_mode_new_from_char ##### -->
<para>

</para>

@mode_char: 
@Returns: 


<!-- ##### FUNCTION girc_channel_mode_dup ##### -->
<para>

</para>

@src: 
@Returns: 


<!-- ##### FUNCTION girc_channel_mode_free ##### -->
<para>

</para>

@mode: 


<!-- ##### STRUCT GIrcChannelPrefix ##### -->
<para>
A structure representing information about a particular channel type (prefix).
</para>

@prefix: the character for this prefix.
@description: a description of this channel type.

<!-- ##### MACRO GIRC_CHANNEL_PREFIX ##### -->
<para>
A standard #GObject style typecasting macro for #GIrcChannelPrefix-struct
structures.
</para>

@boxed: The pointer to be cast.


<!-- ##### MACRO GIRC_TYPE_CHANNEL_PREFIX ##### -->
<para>
The #GBoxed type for #GIrcChannelPrefix-struct structures.
</para>



<!-- ##### ENUM GIrcChannelPrefixChar ##### -->
<para>
An enumerated type corresponding to the standard channel prefix characters.
</para>

@GIRC_CHANNEL_PREFIX_NORMAL: standard channels.
@GIRC_CHANNEL_PREFIX_LOCAL: local-user-only channels.
@GIRC_CHANNEL_PREFIX_SAFE: "safe" channels.
@GIRC_CHANNEL_PREFIX_NO_MODES: channels who's mode flags cannot be set.

<!-- ##### FUNCTION girc_channel_prefix_get_prefix ##### -->
<para>

</para>

@prefix_char: 
@Returns: 


<!-- ##### FUNCTION girc_channel_prefix_dup ##### -->
<para>

</para>

@src: 
@Returns: 


<!-- ##### FUNCTION girc_channel_prefix_free ##### -->
<para>

</para>

@prefix: 


