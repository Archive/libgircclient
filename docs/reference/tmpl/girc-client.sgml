<!-- ##### SECTION Title ##### -->
GIrcClient

<!-- ##### SECTION Short_Description ##### -->
The basic client object

<!-- ##### SECTION Long_Description ##### -->
<para>

</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### MACRO GIRC_MAX_NICK_LEN ##### -->
<para>
The default maximum nick length defined by RFC 2812.
</para>



<!-- ##### STRUCT GIrcClient ##### -->
<para>
The #GIrcClient-struct structure contains no public data.
</para>


<!-- ##### STRUCT GIrcClientClass ##### -->
<para>
The class structure for #GIrcClient-struct objects.
</para>

@connect_complete: the object callback slot for the "connect-complete" signal.
@quit: the object callback slot for the "quit" signal"
@nick_changed: the object callback slot for the "nick-changed" signal.
@ctcp_recv: the object callback slot for the "ctcp-recv" signal.
@target_opened: the object callback slot for the "target-opened" signal.
@target_closed: the object callback slot for the "target-closed" signal.
@target_recv: the object callback slot for the "target-recv" signal.
@query_changed: the object callback slot for the "query-changed" signal.
@channel_changed: the object callback slot for the "channel-changed" signal.
@channel_user_changed: the object callback slot for the "channel-user-changed" signal.
@whois_recv: the object callback slot for the "whois-recv" signal.
@list_recv: the object callback slot for the "list-recv" signal.
@names_recv: the object callback slot for the "names-recv" signal.
@notify_recv: the object callback slot for the "notify-recv" signal.
@motd_recv: the object callback slot for the "motd-recv" signal.
@mode_recv: the object callback slot for the "mode-recv" signal.
@notice_recv: the object callback slot for the "notice-recv" signal.
@msg_recv: the object callback slot for the "msg-recv" signal.
@error_recv: the object callback slot for the "error-recv" signal.
@__girc_reserved_1: 
@__girc_reserved_2: 
@__girc_reserved_3: 
@__girc_reserved_4: 

<!-- ##### FUNCTION girc_client_new ##### -->
<para>

</para>

@nick: 
@realname: 
@username: 
@server: 
@port: 
@pass: 
@outgoing_encoding: 
@Returns: 


<!-- ##### FUNCTION girc_client_is_valid_nick ##### -->
<para>

</para>

@irc: 
@nick: 
@Returns: 


<!-- ##### FUNCTION girc_client_open ##### -->
<para>

</para>

@irc: 


<!-- ##### FUNCTION girc_client_quit ##### -->
<para>

</para>

@irc: 
@reason: 


<!-- ##### FUNCTION girc_client_is_open ##### -->
<para>

</para>

@irc: 
@Returns: 


<!-- ##### FUNCTION girc_client_send_raw ##### -->
<para>

</para>

@irc: 
@str: 


<!-- ##### FUNCTION girc_client_set_nick ##### -->
<para>

</para>

@irc: 
@nick: 
@Returns: 


<!-- ##### FUNCTION girc_client_get_nick ##### -->
<para>

</para>

@irc: 
@Returns: 


<!-- ##### FUNCTION girc_client_set_encoding ##### -->
<para>

</para>

@irc: 
@encoding: 


<!-- ##### FUNCTION girc_client_get_encoding ##### -->
<para>

</para>

@irc: 
@Returns: 


<!-- ##### FUNCTION girc_client_set_away ##### -->
<para>

</para>

@irc: 
@away: 


<!-- ##### FUNCTION girc_client_get_away ##### -->
<para>

</para>

@irc: 
@Returns: 


<!-- ##### FUNCTION girc_client_get_motd ##### -->
<para>

</para>

@irc: 
@Returns: 


<!-- ##### FUNCTION girc_client_open_query ##### -->
<para>

</para>

@irc: 
@nick: 
@Returns: 


<!-- ##### FUNCTION girc_client_get_target ##### -->
<para>

</para>

@irc: 
@handle: 
@Returns: 


<!-- ##### FUNCTION girc_client_close_target ##### -->
<para>

</para>

@irc: 
@target: 


<!-- ##### FUNCTION girc_client_add_user_notify ##### -->
<para>

</para>

@irc: 
@nick: 


<!-- ##### FUNCTION girc_client_remove_user_notify ##### -->
<para>

</para>

@irc: 
@nick: 


<!-- ##### FUNCTION girc_client_get_user_by_nick ##### -->
<para>

</para>

@irc: 
@nick: 
@Returns: 


<!-- ##### FUNCTION girc_client_has_channel_mode ##### -->
<para>

</para>

@irc: 
@mode_char: 
@Returns: 


<!-- ##### SIGNAL GIrcClient::channel-changed ##### -->
<para>
This signal is emitted when a channel the user is on has changed.
</para>

@gircclient: the object which received the signal.
@arg1: the channel.
@arg2: the channel data that has been changed.
@arg3: the actor (can be a string, #GIrcUser-struct, or #GIrcChannelUser-struct) who made the changes.
@arg4: the change message.
@:
@:
@:
@:
@: 

<!-- ##### SIGNAL GIrcClient::channel-user-changed ##### -->
<para>
This signal is emitted when a user on a channel has changed.
</para>

@gircclient: the object which received the signal.
@arg1: the channel.
@arg2: the user being changed.
@arg3: the user data that has been changed.
@arg4: the (can be a string, #GIrcUser-struct, or #GIrcChannelUser-struct) who made the changes.
@arg5: the change message.

<!-- ##### SIGNAL GIrcClient::connect-complete ##### -->
<para>
This signal is emitted when the connection process is complete.
</para>

@gircclient: the object which received the signal.
@arg1: the connection status.
@arg2: the cannonical server name.
@arg3: the user's current nickname.

<!-- ##### SIGNAL GIrcClient::ctcp-recv ##### -->
<para>
This signal is emitted when a user has sent us a CTCP message.
</para>

@gircclient: the object which received the signal.
@arg1: the type of CTCP message.
@arg2: the message data.
@:
@:
@:
@:
@: 

<!-- ##### SIGNAL GIrcClient::error-recv ##### -->
<para>
This signal is emitted when the server sends and error message.
</para>

@gircclient: the object which received the signal.
@arg1: the error type
@arg2: the error message

<!-- ##### SIGNAL GIrcClient::list-recv ##### -->
<para>
This signal is emitted when a LIST command has returned.
</para>

@gircclient: the object which received the signal.
@arg1: a list of #GIrcListItem-struct structures.

<!-- ##### SIGNAL GIrcClient::mode-recv ##### -->
<para>
This signal is emitted when a channel or user's mode has been requested.
</para>

@gircclient: the object which received the signal.
@arg1: the user's server mode flags.

<!-- ##### SIGNAL GIrcClient::motd-recv ##### -->
<para>
This signal is emitted when the MOTD command has returned.
</para>

@gircclient: the object which received the signal.
@arg1: the message of the day.

<!-- ##### SIGNAL GIrcClient::msg-recv ##### -->
<para>
This signal is emitted when a message from the server has been recieved that
is not covered by another signal.
</para>

@gircclient: the object which received the signal.
@arg1: the message type.
@arg2: the message itself.

<!-- ##### SIGNAL GIrcClient::names-recv ##### -->
<para>
This signal is emitted when a NAMES command has returned.
</para>

@gircclient: the object which received the signal.
@arg1: a list of #GIrcNamesItem-struct structures.

<!-- ##### SIGNAL GIrcClient::nick-changed ##### -->
<para>
This signal is emitted when the user's nickname has changed.
</para>

@gircclient: the object which received the signal.
@arg1: the new nickname.

<!-- ##### SIGNAL GIrcClient::notice-recv ##### -->
<para>
This signal is emitted when a server notice has been recieved.
</para>

@gircclient: the object which received the signal.
@arg1: the server which sent the notice.
@arg2: the notice message.
@arg3: any extra data about the notice.

<!-- ##### SIGNAL GIrcClient::query-changed ##### -->
<para>
This signal is emitted when a query's target user has changed.
</para>

@gircclient: the object which received the signal.
@arg1: the query.
@arg2: the type of change.
@arg3: the change message.

<!-- ##### SIGNAL GIrcClient::quit ##### -->
<para>
This signal is emitted when the client has closed the connection with the
server.
</para>

@gircclient: the object which received the signal.
@arg1: whether or not the user requested the connection be closed.

<!-- ##### SIGNAL GIrcClient::target-closed ##### -->
<para>
This signal is emitted when a query or channel has been closed.
</para>

@gircclient: the object which received the signal.
@arg1: the query or channel.
@arg2: the close message (reason).

<!-- ##### SIGNAL GIrcClient::target-opened ##### -->
<para>
This signal is emitted when a new target has been opened.
</para>

@gircclient: the object which received the signal.
@arg1: the new query or channel.

<!-- ##### SIGNAL GIrcClient::target-recv ##### -->
<para>
This signal is emitted when a message for a target has been received.
</para>

@gircclient: the object which received the signal.
@arg1: the query or channel.
@arg2: the actor who sent the message (can be a string, #GIrcUser-struct, or #GIrcChannelUser-struct).
@arg3: the message type.
@arg4: the message itself.

<!-- ##### SIGNAL GIrcClient::whois-recv ##### -->
<para>
This signal is emitted when a WHOIS command has returned.
</para>

@gircclient: the object which received the signal.
@arg1: the #GIrcUser-struct for the user requested.

<!-- ##### ARG GIrcClient:away ##### -->
<para>
Whether the user is away or not.
</para>

<!-- ##### ARG GIrcClient:ban-exceptions ##### -->
<para>
Whether the server supports ban exceptions (+e).
</para>

<!-- ##### ARG GIrcClient:channel-id-length ##### -->
<para>
How long a Channel ID for "!" channels can be.
</para>

<!-- ##### ARG GIrcClient:channel-modes ##### -->
<para>
A #GValueArray of #GIrcChannelMode-struct structures.
</para>

<!-- ##### ARG GIrcClient:channel-name-length ##### -->
<para>
The maximum length a channel name can be.
</para>

<!-- ##### ARG GIrcClient:channel-types ##### -->
<para>
A #GValueArray-struct of #GIrcChannelPrefix-struct structures.
</para>

<!-- ##### ARG GIrcClient:channel-user-modes ##### -->
<para>
A #GValueArray-struct of #GIrcChannelUserMode-struct structures.
</para>

<!-- ##### ARG GIrcClient:channels ##### -->
<para>
A #GValueArray-struct of #GIrcChannel-struct structures.
</para>

<!-- ##### ARG GIrcClient:character-mapping ##### -->
<para>
The server's character mapping style (unused).
</para>

<!-- ##### ARG GIrcClient:cjoin-command ##### -->
<para>
Whether the CJOIN command is supported.
</para>

<!-- ##### ARG GIrcClient:cnotice-command ##### -->
<para>
Whether the CNOTICE command is supported.
</para>

<!-- ##### ARG GIrcClient:cprivmsg-command ##### -->
<para>
Whether the CPRIVMSG command is supported.
</para>

<!-- ##### ARG GIrcClient:encoding ##### -->
<para>
The outgoing encoding.
</para>

<!-- ##### ARG GIrcClient:forced-nickchange ##### -->
<para>
Whether the server can change the user's nickname without him/her requesting it.
</para>

<!-- ##### ARG GIrcClient:hostname ##### -->
<para>
The user's hostname.
</para>

<!-- ##### ARG GIrcClient:invite-exceptions ##### -->
<para>
Whether the server supports invite exceptions (+I).
</para>

<!-- ##### ARG GIrcClient:kick-message-length ##### -->
<para>
The maximum length of KICK command messages.
</para>

<!-- ##### ARG GIrcClient:knock-command ##### -->
<para>
Whether the KNOCK command is supported.
</para>

<!-- ##### ARG GIrcClient:language-command ##### -->
<para>
Whether the LANGUAGE command is supported.
</para>

<!-- ##### ARG GIrcClient:languages ##### -->
<para>
A #GValueArray-struct of #gchararray for supported languages.
</para>

<!-- ##### ARG GIrcClient:max-bans ##### -->
<para>
The maximum size of a channel's banmask list.
</para>

<!-- ##### ARG GIrcClient:max-channels ##### -->
<para>
The maximum number of channels a user can be on concurrently.
</para>

<!-- ##### ARG GIrcClient:max-targets ##### -->
<para>
The maximum number of targets for the CJOIN, CNOTICE, and WALLCHOPS commands.
</para>

<!-- ##### ARG GIrcClient:max-watches ##### -->
<para>
The maximum number of watches which can be set.
</para>

<!-- ##### ARG GIrcClient:mode-command-size ##### -->
<para>
The maximum number of users a MODE command can affect.
</para>

<!-- ##### ARG GIrcClient:motd ##### -->
<para>
The last-known message of the day.
</para>

<!-- ##### ARG GIrcClient:network-name ##### -->
<para>
The server's network name.
</para>

<!-- ##### ARG GIrcClient:nick ##### -->
<para>
The user's current nickname.
</para>

<!-- ##### ARG GIrcClient:nick-length ##### -->
<para>
The true maximum length of the user's nickname.
</para>

<!-- ##### ARG GIrcClient:pass ##### -->
<para>
The user's current PASS command value (password).
</para>

<!-- ##### ARG GIrcClient:penalty ##### -->
<para>
Whether the server has an time penalty for using certain commands.
</para>

<!-- ##### ARG GIrcClient:preferred-nick ##### -->
<para>
The user's preferred nickname.
</para>

<!-- ##### ARG GIrcClient:queries ##### -->
<para>
A #GValueArray-struct of #GIrcQuery-struct structures.
</para>

<!-- ##### ARG GIrcClient:realname ##### -->
<para>
The user's real name.
</para>

<!-- ##### ARG GIrcClient:safe-list ##### -->
<para>
Whether the server sends replies to the LIST command "safely" (in multiple
commands), or all at once.
</para>

<!-- ##### ARG GIrcClient:server-character-set ##### -->
<para>
The server's expected character set (typically unused).
</para>

<!-- ##### ARG GIrcClient:server-name ##### -->
<para>
The server's name.
</para>

<!-- ##### ARG GIrcClient:server-side-ignore ##### -->
<para>
Whether the server supports the caller-id mode (+g) and the ACCEPT command, to
completely ignore certain users.
</para>

<!-- ##### ARG GIrcClient:silence-list-size ##### -->
<para>
The maximum number of users which can be SILENCEd at any one time.
</para>

<!-- ##### ARG GIrcClient:topic-length ##### -->
<para>
The maximum length (in bytes) for a channel's topic.
</para>

<!-- ##### ARG GIrcClient:userip-command ##### -->
<para>
Whether the USERIP command is supported.
</para>

<!-- ##### ARG GIrcClient:username ##### -->
<para>
The user's current username.
</para>

<!-- ##### ARG GIrcClient:wallchops-command ##### -->
<para>
Whether the WALLCHOPS command is supported.
</para>

<!-- ##### ARG GIrcClient:who-command-is-whox ##### -->
<para>
Whether the WHO command is an alias for the WHOX command.
</para>

